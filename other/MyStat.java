package org.math.factoring;

import java.math.BigInteger;
import java.security.SecureRandom;

class MyStat {
    private final static BigInteger ZERO = new BigInteger("0");
    private final static BigInteger ONE = new BigInteger("1");
    private final static BigInteger TWO = new BigInteger("2");
    private final static SecureRandom random = new SecureRandom();

    private final static BigInteger TEN = new BigInteger("10");
    private final static BigInteger BI_12000 = new BigInteger("12000");

    private static final int[][] STATS = new int[28][28];
    private static final int[] COUNT = new int[28];
    private static final int[] FACTOR_COUNT = new int[12001];

    public static BigInteger rho(BigInteger N) {
        BigInteger divisor;
        BigInteger c = new BigInteger(N.bitLength(), random);
        BigInteger x = new BigInteger(N.bitLength(), random);
        BigInteger xx = x;

        // check divisibility by 2
        if (N.mod(TWO).compareTo(ZERO) == 0) return TWO;

        do {
            int step = 0;
            do {
                step++;
                x = x.multiply(x).mod(N).add(c).mod(N);
                xx = xx.multiply(xx).mod(N).add(c).mod(N);
                xx = xx.multiply(xx).mod(N).add(c).mod(N);
                divisor = x.subtract(xx).gcd(N);
            } while ((divisor.compareTo(ONE)) == 0 && step < 1000);
            c = new BigInteger(N.bitLength(), random);
            x = new BigInteger(N.bitLength(), random);
            xx = x;
        } while ((divisor.compareTo(ONE)) == 0);

        return divisor;
    }

    public static void init() {
        FACTOR_COUNT[0] = 0;
        FACTOR_COUNT[1] = 0;
        FACTOR_COUNT[2] = 1;
        FACTOR_COUNT[3] = 1;
        FACTOR_COUNT[4] = 2;
        FACTOR_COUNT[5] = 1;
        FACTOR_COUNT[6] = 2;
        FACTOR_COUNT[7] = 1;
        FACTOR_COUNT[8] = 3;
        FACTOR_COUNT[9] = 2;
    }

    public static int factorCount(BigInteger N) {
        if (N.compareTo(ONE) == 0) return 0;
        if (N.isProbablePrime(20)) {
            // System.out.println(N);
            return 1;
        }
        if (N.subtract(BI_12000).signum() == -1 && FACTOR_COUNT[N.intValue()] != 0) {
            return FACTOR_COUNT[N.intValue()];
        }

        BigInteger divisor = PollardRho.rho(N);
        //factor(divisor);
        //factor(N.divide(divisor));
        return factorCount(divisor) + factorCount(N.divide(divisor));
    }

    private static void stat() {
        BigInteger max = TWO.pow(27);
        // int count = 3;
        COUNT[1] = 4;
        COUNT[2] = 3;
        COUNT[3] = 1;
        int index = 4;
        for (BigInteger i = TEN; i.subtract(max.add(ONE)).signum() == -1; i = i.add(ONE)) {
            System.out.println(i);

            int fcount = factorCount(i);

            if (i.subtract(BI_12000).signum() == -1) {
                FACTOR_COUNT[i.intValue()] = fcount;
            }

//            if (fcount == c) {
//                count++;
//            }

            COUNT[fcount]++;

            if (i.equals(TWO.pow(index))) {
                System.arraycopy(COUNT, 1, STATS[index], 1, 27);
                index++;
            }
        }
    }

    private static void printStat() {
//        for (int i = 0; i < 27; i++) {
//            System.out.print(i);
//            System.out.print(" : ");
//            System.out.print(TWO.pow(i));
//            System.out.print(" : ");
//            System.out.println(STATS[i]);
//        }

        for (int i = 1; i <= 27; i++) {
            System.out.println("=========================");
            for (int j = 1; j <= 27; j++) {
                System.out.print(TWO.pow(j));
                System.out.print(",");
                System.out.println(STATS[j][i]);
            }
        }
    }

    public static void main(String[] args) {
        // System.out.println(factorCount(new BigInteger("200")));

        stat();
        System.out.println("-----------------");
        printStat();
    }
}