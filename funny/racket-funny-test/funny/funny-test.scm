#lang racket

(require rackunit)
(require rackunit/text-ui)

(require "../prepare/text-to-parse.scm")
(require "../../racket-funny/common/messages.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Trie.scm")
(require "../../racket-funny/ds/LcrsTree.scm")
(require "../../racket-funny/funny/global.scm")
(require "../../racket-funny/funny/engine.scm")
(require "../../racket-funny/funny/funny.scm")

;(find-files (lambda (p) (string-suffix? (path->string p) ".fn"))  "../../racket-funny/config/lang/")

(define funny-test
  (test-suite
   "Tests for funny.scm"
   (test-case
    "init-system-macro"
    (begin
      (init-system-macro (open-input-string +test-system-macros+))
      (check-equal? (null? (trie-find *mapping-trie* "def")) #f "Wrong init-system-macro.")))
   
   ))

(run-tests funny-test)

(build-components '("E:/gitee/funnyos/study/funny") "org/funny/circle-test/funny-component-circle-0.1.0")


;(define (assoc-value alist key)
;  (cdr (assoc key alist)))
;(define aa (parse-project-info "E:/gitee/funnyos/study/funny/racket-funny/funny-example/project.info"))
;(assoc-value aa "[dependencies]")



;(define aa (parse-project-info "E:/gitee/funnyos/study/funny/racket-funny/funny-example/component.project"))
;(assoc-value (assoc-value aa "[project-settings]") "dependencies")

#|

"""
[component-name]
funny-example::1.0.0

[global-variables]
functions

[bindings]
source-location=src
build-location=build
meta-file=component.meta

[function-bindings]
get=default
clean=default
parse-meta=default
build=default

[build-actions]
(begin
	(:clean build-location)
	(let ((r (:parse-meta meta-file)))
		(:build r (:get functions "match"))))

[profile]
"""

(extract-config "E:\\gitee\\funnyos\\study\\funny\\racket-funny\\funny-example\\component.project")
=>
'(("[component-name]" "funny-example::1.0.0")
  ("[global-variables]" "functions")
  ("[bindings]" "source-location=src" "build-location=build" "meta-file=component.meta")
  ("[function-bindings]"
   "get=default"
   "clean=default"
   "parse-meta=default"
   "build=default")
  ("[build-actions]"
   "(begin"
   "(:clean build-location)"
   "(let ((r (:parse-meta meta-file)))"
   "(:build r (:get functions \"match\"))))")
  ("[profile]"))


(assoc-value (extract-config "E:\\gitee\\funnyos\\study\\funny\\racket-funny\\funny-example\\component.project") "[function-bindings]")
=>
'("get=default" "clean=default" "parse-meta=default" "build=default")

|#

