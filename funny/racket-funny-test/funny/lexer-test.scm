#lang racket

(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/messages.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Queue.scm")
(require "../../racket-funny/ds/HashTable.scm")
(require "../../racket-funny/funny/global.scm")
(require "../../racket-funny/funny/lexer.scm")

; enums
(define +E-direct-rule+      1)
(define +E-plain-rule+       2)
(define +E-group-rule+       3)
(define +E-terminal-rule+    4)

(define (construct-rule)
  (clear-rules)
  (register-rule "atom" (list (list +E-direct-rule+ "<boolean>")
                              (list +E-direct-rule+ "<character>")
                              (list +E-direct-rule+ "<string>")
                              (list +E-direct-rule+ "<number>")
                              (list +E-direct-rule+ "<symbol>")))
  (register-rule "boolean" (list (list +E-plain-rule+ "<bool>" "(lex \"Boolean\" \"%1%\")")))
  (register-rule "character" (list (list +E-plain-rule+ "<char>" "(lex \"Character\" \"%1%\")")))
  (register-rule "string" (list (list +E-plain-rule+ "<str>" "(lex \"String\" \"%1%\")")))
  (register-rule "symbol" (list (list +E-group-rule+ "('<initial><subsequent>*)" "(lex \"Symbol\" \"%1%\")")
                                (list +E-plain-rule+ "<peculiar>" "(lex \"Symbol\" \"%1%\")")))
  (register-rule "number" (list (list +E-direct-rule+ "<complex>")))
  (register-rule "complex" (list (list +E-plain-rule+ "<real>" "(make-complex %1% 0)")
                                 (list +E-plain-rule+ "<real>\\+<ureal>i" "(make-complex %1% %2%)")
                                 (list +E-plain-rule+ "<real>-<ureal>i" "(make-complex %1% (- %2%))")
                                 (list +E-plain-rule+ "<real>\\+i" "(make-complex %1% 1)")
                                 (list +E-plain-rule+ "<real>-i" "(make-complex %1% -1)")
                                 (list +E-plain-rule+ "\\+<ureal>i" "(make-complex 0 %1%)")
                                 (list +E-plain-rule+ "-<ureal>i" "(make-complex 0 (- %1%))")
                                 (list +E-plain-rule+ "\\+i" "(make-complex 0 1)")
                                 (list +E-plain-rule+ "-i" "(make-complex 0 -1)")))
  (register-rule "real" (list (list +E-direct-rule+ "<integer>")
                              (list +E-direct-rule+ "<fraction>")
                              (list +E-direct-rule+ "<decimal>")))
  (register-rule "ureal" (list (list +E-direct-rule+ "<uinteger>")
                               (list +E-direct-rule+ "<ufraction>")
                               (list +E-direct-rule+ "<udecimal>")))
  (register-rule "fraction" (list (list +E-plain-rule+ "<integer>\\/<uinteger>" "(make-fraction %1% %2%)")))
  (register-rule "ufraction" (list (list +E-plain-rule+ "<uinteger>\\/<uinteger>" "(make-fraction %1% %2%)")))
  (register-rule "integer" (list (list +E-group-rule+ "(<sign><digit>+)" "(lex \"Integer\" \"%1%\")")))
  (register-rule "uinteger" (list (list +E-group-rule+ "(<digit>+)" "(lex \"Integer\" \"%1%\")")))
  (register-rule "decimal" (list (list +E-group-rule+ "(<sign><digit>+)" "(lex \"Decimal\" \"%1%\")")
                                 (list +E-group-rule+ "(<sign>\\.<digit>+)" "(lex \"Decimal\" \"%1%\")")
                                 (list +E-group-rule+ "(<sign><digit>+\\.<digit>*)" "(lex \"Decimal\" \"%1%\")")
                                 (list +E-group-rule+ "(<sign><digit>+\\.)" "(lex \"Decimal\" \"%1%\")")
                                 (list +E-group-rule+ "(<sign><digit>+<suffix>)" "(lex \"Decimal\" \"%1%\")")
                                 (list +E-group-rule+ "(<sign>\\.<digit>+<suffix>)" "(lex \"Decimal\" \"%1%\")")
                                 (list +E-group-rule+ "(<sign><digit>+\\.<digit>*<suffix>)" "(lex \"Decimal\" \"%1%\")")
                                 (list +E-group-rule+ "(<sign><digit>+\\.<suffix>)" "(lex \"Decimal\" \"%1%\")")))
  (register-rule "udecimal" (list (list +E-group-rule+ "(<digit>+)" "(lex \"Decimal\" \"%1%\")")
                                  (list +E-group-rule+ "(\\.<digit>+)" "(lex \"Decimal\" \"%1%\")")
                                  (list +E-group-rule+ "(<digit>+\\.<digit>*)" "(lex \"Decimal\" \"%1%\")")
                                  (list +E-group-rule+ "(<digit>+\\.)" "(lex \"Decimal\" \"%1%\")")
                                  (list +E-group-rule+ "(<digit>+<suffix>)" "(lex \"Decimal\" \"%1%\")")
                                  (list +E-group-rule+ "(\\.<digit>+<suffix>)" "(lex \"Decimal\" \"%1%\")")
                                  (list +E-group-rule+ "(<digit>+\\.<digit>*<suffix>)" "(lex \"Decimal\" \"%1%\")")
                                  (list +E-group-rule+ "(<digit>+\\.<suffix>)" "(lex \"Decimal\" \"%1%\")")))

  (register-terminal "bool" "#[tf]")
  (register-terminal "char" "#\\\\[[:graph:]]|#\\\\space|#\\\\newline")
  (register-terminal "str" "\"([^\"\\\\]*(\\\\.[^\"\\\\]*)*)\"")
  (register-terminal "suffix" "e[+-]?[[:digit:]]+")
  (register-terminal "sign" "[+-]?")
  (register-terminal "digit" "[[:digit:]]")
  (register-terminal "initial" "[a-zA-Z!$%&*/:<=>?~_^]")
  (register-terminal "subsequent" "[a-zA-Z0-9!$%&*/:<=>?~_^.+-]")
  (register-terminal "peculiar" "\\+|-|...")
  (register-terminal "letter" "[a-zA-Z]")
  
  )










(enqueue *lex-orders* "complex")
(construct-rule)
(cache-patterns)

;(rule-walker (hashmap-get *lex-rules* "complex"))

;(displayln "------------------------------")
(lex-to-function "complex" "-4/7+3i")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; transformations
; "boolean"
;  -(init-pattern)->    '("boolean" "<1>" "%1%" #hashmap("1" ((2 "<bool>" "bool-target"))))
;  -(pattern-node-list)->
;                     #hashmap("1" ((2 "<bool>" "bool-target")))  "boolean"
;                     -(expand-rule-map)-> '((("1" "boolean" (2 "<bool>" "bool-target"))))
;
; "complex"
;  -(init-pattern)->    '("complex" "<1>" "%1%" #hashmap("1" ((2 "<real>\\+<ureal>i" "complex-target"))))
;  -(pattern-node-list)->
;                     #hashmap("1" ((2 "<real>\\+<ureal>i" "complex-target")))  "complex"
;                     -(expand-rule-map)-> '((("1" "complex" (2 "<real>\\+<ureal>i" "complex-target"))))
;
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



#|


(define (construct-rule-map keys rule-names)
  (construct-rule)
  (let ((hm (CREATE-HashMap)))
    (map
     (lambda (key rule-name)
       (hashmap-put! hm key (rule-value rule-name)))
     keys
     rule-names)
    hm))

(define (transform-hashtable-in-list lst pos)
  (list-update lst pos hashmap-elements))

(define lexer-test
  (test-suite
   "Tests for lexer.scm"
   (test-case
    "terminal-rule-type?"
    (begin
      (check-equal? (terminal-rule-type? +E-group-rule+) #t "Wrong terminal-rule-type.")
      (check-equal? (terminal-rule-type? +E-token+) #t "Wrong terminal-rule-type.")
      (check-equal? (terminal-rule-type? +E-direct-rule+) #f "Wrong terminal-rule-type.")
      (check-equal? (terminal-rule-type? +E-plain-rule+) #f "Wrong terminal-rule-type.")))
   (test-case
    "non-terminal-rule-type?"
    (begin
      (check-equal? (non-terminal-rule-type? +E-group-rule+) #f "Wrong non-terminal-rule-type.")
      (check-equal? (non-terminal-rule-type? +E-token+) #f "Wrong non-terminal-rule-type.")
      (check-equal? (non-terminal-rule-type? +E-direct-rule+) #t "Wrong non-terminal-rule-type.")
      (check-equal? (non-terminal-rule-type? +E-plain-rule+) #t "Wrong non-terminal-rule-type.")))


  
   (test-case
    "expand-direct-rule"
    (begin
      (construct-rule)
      (let ((actual-result   (expand-direct-rule "111" '("atom" 1 "<boolean>")))
            (expected-result (list 1 "111" "<1111>" "%1111%" (construct-rule-map (list "1111") (list "boolean")))))
        (check-equal? (transform-hashtable-in-list actual-result 4)
                      (transform-hashtable-in-list expected-result 4)
                      "Wrong expand-direct-rule."))))
   (test-case
    "expand-plain-rule"
    (begin
      (construct-rule)
      (let ((actual-result   (expand-plain-rule "111" '("fraction" 2 "<integer>\\/<uinteger>" "(make-fraction %1% %2%)")))
            (expected-result (list 2 "111" "<1111>\\/<1112>" "(make-fraction %1111% %1112%)" (construct-rule-map (list "1111" "1112") (list "integer" "uinteger")))))
        (check-equal? (transform-hashtable-in-list actual-result 4)
                      (transform-hashtable-in-list expected-result 4)
                      "Wrong expand-plain-rule."))))
   (test-case
    "expand-rule"
    (begin
      (construct-rule)
      (let ((actual-result   (expand-rule '("111" ("fraction" 2 "<integer>\\/<uinteger>" "(make-fraction %1% %2%)"))))
            (expected-result (list 2 "111" "<1111>\\/<1112>" "(make-fraction %1111% %1112%)" (construct-rule-map (list "1111" "1112") (list "integer" "uinteger")))))
        (check-equal? (transform-hashtable-in-list actual-result 4)
                      (transform-hashtable-in-list expected-result 4)
                      "Wrong expand-rule."))))
   (test-case
    "expand-rule-map"
    (begin
      (construct-rule)
      (let ((actual-result   (expand-rule-map (construct-rule-map (list "11" "12") (list "real" "ureal")) "complex"))
            (expected-result '((("11" "real" (1 "<integer>") (1 "<fraction>") (1 "<decimal>")))
                               (("12" "ureal" (1 "<uinteger>") (1 "<ufraction>") (1 "<udecimal>"))))))
        (check-equal? actual-result
                      expected-result
                      "Wrong expand-rule-map."))))
   (test-case
    "cartesian-product-rule"
    (begin
      (construct-rule)
      (let ((actual-result   (cartesian-product-rule '((("11" "real" (1 "<integer>") (1 "<fraction>") (1 "<decimal>")))
                                                       (("12" "ureal" (1 "<uinteger>") (1 "<ufraction>") (1 "<udecimal>"))))))
            (expected-result '((("11" "real" 1 "<integer>") ("12" "ureal" 1 "<uinteger>"))
                               (("11" "real" 1 "<integer>") ("12" "ureal" 1 "<ufraction>"))
                               (("11" "real" 1 "<integer>") ("12" "ureal" 1 "<udecimal>"))
                               (("11" "real" 1 "<fraction>") ("12" "ureal" 1 "<uinteger>"))
                               (("11" "real" 1 "<fraction>") ("12" "ureal" 1 "<ufraction>"))
                               (("11" "real" 1 "<fraction>") ("12" "ureal" 1 "<udecimal>"))
                               (("11" "real" 1 "<decimal>") ("12" "ureal" 1 "<uinteger>"))
                               (("11" "real" 1 "<decimal>") ("12" "ureal" 1 "<ufraction>"))
                               (("11" "real" 1 "<decimal>") ("12" "ureal" 1 "<udecimal>")))))
        (check-equal? actual-result
                      expected-result
                      "Wrong cartesian-product-rule."))))

   
  
   (test-case
    "merged-hashmap"
    (begin
      (let ((hm1 (CREATE-HashMap))
            (hm2 (CREATE-HashMap))
            (hm3 (CREATE-HashMap)))
        (hashmap-put! hm1 "1" "element1")
        (hashmap-put! hm2 "2" "element2")
        (hashmap-put! hm3 "1" "element1")
        (hashmap-put! hm3 "2" "element2")
        (check-equal? (transform-hashtable-in-list (list hm1) 0)
                      (transform-hashtable-in-list (list (merged-hashmap (list hm1))) 0)
                      "Wrong merged-hashmap.")
        (check-equal? (transform-hashtable-in-list (list hm3) 0)
                      (transform-hashtable-in-list (list (merged-hashmap (list hm1 hm2))) 0)
                      "Wrong merged-hashmap."))))

;   (test-case
;    "expand-group-rule"
;    (begin
;      (construct-rule)
;      (let ((actual-result   (expand-group-rule "111" '("symbol" 3 "('<initial><subsequent>*)" "(lex \"Symbol\" \"%1%\")")))
;            (expected-result (list 3 "111" "<1111>" "%1111%" (construct-rule-map (list "1111") (list "symbol")))))
;        (check-equal? (transform-hashtable-in-list actual-result 4)
;                      (transform-hashtable-in-list expected-result 4)
;                      "Wrong expand-group-rule."))))


   (test-case
    "get-rule"
    (begin
      (construct-rule)
      (let ((actual-result   (get-rule "fraction"))
            (expected-result (list (list +E-plain-rule+ "<integer>\\/<uinteger>" "(make-fraction %1% %2%)"))))
        (check-equal? actual-result
                      expected-result
                      "Wrong get-rule."))))

   (test-case
    "generate-rule-children"
    (begin
      (construct-rule)
      (let ((actual-result   (generate-rule-children "fraction"))
            (expected-result (list (list "fraction" '((1 . "integer") (2 . "uinteger")) +E-plain-rule+ "<1>\\/<2>" "(make-fraction %1% %2%)"))))
        (check-equal? actual-result
                      expected-result
                      "Wrong generate-rule-children."))))

;   (test-case
;    "expand-rule"
;    (begin
;      (construct-rule)
;      (let ((actual-result   (expand-rule (list "fraction" '((1 . "integer") (2 . "uinteger")) +E-plain-rule+ "<1>\\/<2>" "(make-fraction %1% %2%)")))
;            (expected-result '()))
;        (check-equal? actual-result
;                      expected-result
;                      "Wrong expand-rule."))))

   (test-case
    "merge-rule"
    (begin
      (construct-rule)
      (let ((actual-result   (merge-rule (list "fraction" '((1 . "integer") (2 . "uinteger")) +E-plain-rule+ "<1>\\/<2>" "(make-fraction %1% %2%)")
                                         (list '((3 "(<sign><digit>+)" "(lex \"Integer\" \"%1%\")")
                                                 (3 "(<digit>+)" "(lex \"Integer\" \"%1%\")")))))
            (expected-result '()))
        (check-equal? actual-result
                      expected-result
                      "Wrong merge-rule."))))

   ;'(((fraction . fnode1) (ureal . unode1)) ((fraction . fnode1) (ureal . unode2)))
   ))

(run-tests lexer-test)

|#




