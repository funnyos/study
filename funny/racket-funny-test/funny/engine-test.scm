#lang racket

(require rackunit)
(require rackunit/text-ui)

(require "../prepare/text-to-parse.scm")
(require "../../racket-funny/common/messages.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Trie.scm")
(require "../../racket-funny/ds/LcrsTree.scm")
(require "../../racket-funny/funny/global.scm")
(require "../../racket-funny/funny/engine.scm")

;(find-files (lambda (p) (string-suffix? (path->string p) ".fn"))  "../../racket-funny/config/lang/")

(define engine-test
  (test-suite
   "Tests for engine.scm"
   (test-case
    "init-system-macro"
    (begin
      (init-system-macro (open-input-string +test-system-macros+))
      (check-equal? (null? (trie-find *mapping-trie* "def")) #f "Wrong init-system-macro.")))
   (test-case
    "cache-compute-domains"
    (begin
      (cache-compute-domains "../../racket-funny/config/lang/")
      (tree-travel *compute-domains* (lambda (t) (displayln (tree-node-key t)) (displayln (tree-node-value t)))) ;to verify
      ))
   ))

(run-tests engine-test)


