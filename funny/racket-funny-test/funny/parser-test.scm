#lang racket

(require rackunit)
(require rackunit/text-ui)

(require "../prepare/text-to-parse.scm")
(require "../../racket-funny/common/messages.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/funny/parser.scm")

(define parser-test
  (test-suite
   "Tests for parser.scm"
   (test-case
    "parse-statement"
    (begin
      (parse-statements +test-example1+)
      (parse-statements +test-system-macros+)))
   (test-case
    "eval-statements"
    (begin
      (eval-statements
       (lambda (st)
         (if (null? st) null
             (displayln (ask st +M-Object-to-string+))))
       +test-example1+)
      (eval-statements
       (lambda (st)
         (if (null? st) null
             (displayln (ask st +M-Object-to-string+))))
       +test-system-macros+)))
   ))

(run-tests parser-test)
