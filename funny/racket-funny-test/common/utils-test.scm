#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/utils.scm")

(define utils-test
  (test-suite
   "Tests for utils-test.scm"
   (test-case
    "make-counter"
    (let ((counter (make-counter)))
      (check-equal? (counter) 1 "Wrong counter.")
      (check-equal? (counter) 2 "Wrong counter.")))
   (test-case
    "make-empties"
    (begin
      (check-equal? (make-empties 0) '() "Wrong make-empties.")
      (check-equal? (make-empties 1) '(()) "Wrong make-empties.")
      (check-equal? (make-empties 2) '(() ()) "Wrong make-empties.")))
   (test-case
    "make-enumeration"
    (begin
      (check-equal? (make-enumeration 1 1) '(1) "Wrong make-enumeration.")
      (check-equal? (make-enumeration 1 3) '(1 2 3) "Wrong make-enumeration.")
      (check-equal? (make-enumeration 1 0) '() "Wrong make-enumeration.")))
   (test-case
    "make-alist"
    (begin
      (check-equal? (make-alist '(1 2 3) '(a v x)) '((1 . a) (2 . v) (3 . x)) "Wrong make-alist.")
      (check-equal? (make-alist '(1 2) '(a v x)) '((1 . a) (2 . v)) "Wrong make-alist.")
      (check-equal? (make-alist '(1 2 3) '(a v)) '((1 . a) (2 . v)) "Wrong make-alist.")
      (check-equal? (make-alist '() '()) '() "Wrong make-alist.")))
   (test-case
    "make-enumeration-list"
    (begin
      (check-equal? (make-enumeration-list '(a v x)) '((1 . a) (2 . v) (3 . x)) "Wrong make-enumeration-list.")
      (check-equal? (make-enumeration-list '()) '() "Wrong make-enumeration-list.")))
   (test-case
    "split-by"
    (begin
      (check-equal?
       (split-by '(a b c x d x e x f) 'x)
       '((a b c) (d) (e) (f))
       "Wrong split-by.")
      (check-equal?
       (split-by '(x) 'x)
       '(() ())
       "Wrong split-by.")))
   (test-case
    "interleave"
    (begin
      (check-equal?
       (interleave '(1 2 3 4 5) '(a b c))
       '(1 a 2 b 3 c 4 5)
       "Wrong interleave.")))
   (test-case
    "insert-between"
    (begin
      (check-equal?
       (insert-between '(a b c) 'x)
       '(a x b x c)
       "Wrong insert-between.")))
   (test-case
    "read-lines"
    (begin
      (check >
             (length (read-lines (open-input-file "utils-test.scm")))
             5
             "Wrong read-lines."))
    (begin
      (check-equal?
       (read-lines (open-input-string "a\nbb\ncd\ne"))
       '("a" "bb" "cd" "e")
       "Wrong read-lines.")))
   (test-case
    "every?"
    (begin
      (check-equal? (every? char? (list #\a #\b #\c)) #t "Wrong every?")
      (check-equal? (every? char? (list #\a 12 #\c)) #f "Wrong every?")))
   (test-case
    "some?"
    (begin
      (check-equal? (some? char? (list #\a #\b #\c)) #f "Wrong some?")
      (check-equal? (some? char? (list "a" #\b #\c)) #t "Wrong some?")))
   (test-case
    "any?"
    (begin
      (check-equal? (any? char? (list "a" #\b #\c)) #t "Wrong any?")
      (check-equal? (any? char? (list "a" "b" "c")) #f "Wrong any?")))
   (test-case
    "none?"
    (begin
      (check-equal? (none? char? (list "a" #\b #\c)) #f "Wrong none?")
      (check-equal? (none? char? (list "a" "b" "c")) #t "Wrong none?")))
   (test-case
    "concat"
    (begin
      (check-equal?
       (concat '(1 2 3) '(4 5 6) '(7 8 9))
       '(1 2 3 4 5 6 7 8 9)
       "Wrong concat.")))))

(run-tests utils-test)
