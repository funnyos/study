#lang racket

(require "../../racket-funny/common/utils.scm")
(require "../../racket-funny/ds/LcrsTree.scm")
(require "../../racket-funny/plugin/default-meta-parser.scm")

(define txt "
::Object
--::A1
    methods: ma1/0, ma2/0, ma3/0
----::B1
      methods: mb1/1, mb2/1, mb3/2
------::C1
        properties: pc1, pc2
        methods: mc1/1, mc2/1
------::C2
        properties: pc3, pc4
        methods: mc3/1, mc4/1
------::C3
        properties: pc5, pc6
        methods: mc5/1, mc6/1
----::B2
      methods: mb4/1, mb5/1, mb6/2

::Others
--::D1
    methods: md1/0, md2/0, md3/0
----::E1
      methods: me1/1, me2/1, me3/2
------::F1
        properties: pf1, pf2
        methods: mf1/1, mf2/1
")

(parse-classes txt)

(map (lambda (t) (tree-travel t (lambda (n) (display (tree-node-key n)) (display #\newline)))) (parse-classes txt))

;(filter non-blank-string? (string-split txt "\n"))

;(partition (lambda (t) (string-prefix? t "--"))
;           (filter non-blank-string? (string-split txt "\n")))


;(grouping-classes (filter non-blank-string? (string-split txt "\n")))

;(grouping-blocks (filter non-blank-string? (string-split txt "\n"))
;                 (lambda (t) (string-prefix? t "::")))


#|

'(("::Object"
   ("--::A1" "methods: ma1/0, ma2/0, ma3/0")
   ("----::B1" "methods: mb1/1, mb2/1, mb3/2")
   ("------::C1" "properties: pc1, pc2" "methods: mc1/1, mc2/1")
   ("------::C2" "properties: pc3, pc4" "methods: mc3/1, mc4/1")
   ("------::C3" "properties: pc5, pc6" "methods: mc5/1, mc6/1")
   ("----::B2" "methods: mb4/1, mb5/1, mb6/2"))
  ("::Others"
   ("--::D1" "methods: md1/0, md2/0, md3/0")
   ("----::E1" "methods: me1/1, me2/1, me3/2")
   ("------::F1" "properties: pf1, pf2" "methods: mf1/1, mf2/1")))

|#



