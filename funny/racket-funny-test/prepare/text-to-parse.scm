#lang racket

(require racket/file)

(provide (all-defined-out))

(define +test-example1+ "  ;aa  a\n {\"abc\" \"de\\\"f\" aaa {comment \"123\" \"456\"}  ...} ;sdfd\n   ;aa  a\n {\"abc\" \"de\\\"f\" aaa {\"123\" \"456\"} aa} ;sdfd\n ")

(define +test-system-macros+ (file->string "../../racket-funny/config/system-macro.fn"))

;(define +test-project-config+ (file->string "../../racket-funny/funny-example/component.project"))

;(define +test-component-meta+ (file->string "../../racket-funny/funny-example/component.meta"))

;(define +test-build-script+ (file->string "../../racket-funny/funny-example/component.project.build"))

(define +test-example-project-info+ (file->string "../../org/funny/funny-example-0.1.0/project.info"))

(define +test-example-component-info+ (file->string "../../org/funny/funny-example-0.1.0/component.info"))
