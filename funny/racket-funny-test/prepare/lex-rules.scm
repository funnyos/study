#lang racket
(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/HashTable.scm")
(require "../../racket-funny/ds/LexRule.scm")

(provide (all-defined-out))

(define (get-fraction-number-rule rule-name)
  (define (init-rules)
    (define lex-rules (CREATE-HashMap))
    (hashmap-put! lex-rules "fraction-number" (list "fraction-number" +E-direct-rule+ "<1>" "%1%" '((1 . "fraction"))))
    (hashmap-put! lex-rules "fraction" (list "fraction" +E-plain-rule+  "<1>\\/<2>" "(make-fraction %1% %2%)" '((1 . "integer") (2 . "uinteger"))))
    (hashmap-put! lex-rules "integer" (list "integer" +E-group-rule+  "<1>" "%1%" '((1 . "an-integer"))))
    (hashmap-put! lex-rules "uinteger" (list "uinteger" +E-group-rule+  "<1>" "%1%" '((1 . "an-uinteger"))))
    (hashmap-put! lex-rules "an-integer" (list "an-integer" +E-group-rule+  "<1>" "(lex \"Integer\" \"%1%\")" '((1 . "(<sign><digit>+)"))))
    (hashmap-put! lex-rules "an-uinteger" (list "an-uinteger" +E-group-rule+  "<1>" "(lex \"Integer\" \"%1%\")" '((1 . "(<digit>+)"))))
    (hashmap-put! lex-rules "sign" (list "sign" +E-terminal+ "[+-]?"))
    (hashmap-put! lex-rules "digit" (list "digit" +E-terminal+ "[[:digit:]]"))
    lex-rules)
  (define *fraction-number-rules* (init-rules))
  (hashmap-get *fraction-number-rules* rule-name))

(define fraction-number-rule-fetcher (lambda (rule-name) (get-fraction-number-rule rule-name)))

(define *lex-rule-fraction-number* (CREATE-LexRule-By-Fetcher "fraction-number" fraction-number-rule-fetcher))
(define *lex-rule-fraction* (CREATE-LexRule-By-Fetcher "fraction" fraction-number-rule-fetcher))
(define *lex-rule-integer* (CREATE-LexRule-By-Fetcher "integer" fraction-number-rule-fetcher))
(define *lex-rule-uinteger* (CREATE-LexRule-By-Fetcher "uinteger" fraction-number-rule-fetcher))


