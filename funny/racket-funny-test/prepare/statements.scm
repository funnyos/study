#lang racket

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Statement.scm")

(provide (all-defined-out))

(define (CREATE-OneArgument symbol)
  (CREATE-OneStatement (list (CREATE-SymbolStatement symbol))))

; statement: {a b {c} {d} e {f}}
(define test-statement1
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-SymbolStatement "b")
         (CREATE-OneArgument "c")
         (CREATE-OneArgument "d")
         (CREATE-SymbolStatement "e")
         (CREATE-OneArgument "f"))))

; statement: {a b {{1} + {2}} {3} e {4}}
(define test-statement2
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-SymbolStatement "b")
         (CREATE-OneStatement
          (list (CREATE-OneArgument "1")
                (CREATE-SymbolStatement "+")
                (CREATE-OneArgument "2")))
         (CREATE-OneArgument "3")
         (CREATE-SymbolStatement "e")
         (CREATE-OneArgument "4"))))

; statement: {a {b {c} ..} d}
(define test-statement3
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-OneStatement
          (list (CREATE-SymbolStatement "b")
                (CREATE-OneArgument "c")
                (CREATE-SymbolStatement "..")))
         (CREATE-SymbolStatement "d"))))

; statement: {a {b {c} ...} d}
(define test-statement4
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-OneStatement
          (list (CREATE-SymbolStatement "b")
                (CREATE-OneArgument "c")
                (CREATE-SymbolStatement "...")))
         (CREATE-SymbolStatement "d"))))

; {run-script {"(cache-lex {orders})"} when match statement {cache lex {orders}}}
(define test-script-statement
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "run-script")
         (CREATE-OneArgument "\"(cache-lex {orders})\"")
         (CREATE-SymbolStatement "when")
         (CREATE-SymbolStatement "match")
         (CREATE-SymbolStatement "statement")
         (CREATE-OneStatement
          (list (CREATE-SymbolStatement "cache")
                (CREATE-SymbolStatement "lex")
                (CREATE-OneArgument "orders"))))))






