#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Stack.scm")

(define Stack-test
  (test-suite
   "Tests for Stack-test.scm"
   (test-case
    "stack-is-empty"
    (let ((stack (CREATE-Stack)))
      (check-equal? (stack-is-empty? stack) #t "Wrong stack-is-empty.")
      (stack-push stack 1)
      (check-equal? (stack-is-empty? stack) #f "Wrong stack-is-empty.")
      (stack-pop stack)
      (check-equal? (stack-is-empty? stack) #t "Wrong stack-is-empty.")
      (stack-pop stack)
      (check-equal? (stack-is-empty? stack) #t "Wrong stack-is-empty.")))
   (test-case
    "push and pop"
    (let ((stack (CREATE-Stack)))
      (stack-push stack 1)
      (check-equal? (stack-peek stack) 1 "Wrong push and pop.")
      (stack-push stack 2)
      (check-equal? (stack-peek stack) 2 "Wrong push and pop.")
      (stack-push stack 3)
      (check-equal? (stack-peek stack) 3 "Wrong push and pop.")
      (check-equal? (stack-pop stack) 3 "Wrong push and pop.")
      (check-equal? (stack-peek stack) 2 "Wrong push and pop.")
      (stack-push stack 4)
      (stack-push stack 5)
      (check-equal? (stack-pop stack) 5 "Wrong push and pop.")
      (check-equal? (stack-pop stack) 4 "Wrong push and pop.")
      (check-equal? (stack-pop stack) 2 "Wrong push and pop.")
      (check-equal? (stack-pop stack) 1 "Wrong push and pop.")
      (check-equal? (stack-pop stack) +nil+ "Wrong push and pop.")))
   (test-case
    "stack-push-all"
    (let ((stack (CREATE-Stack)))
      (stack-push-all stack '(1 2 3))
      (check-equal? (stack-pop stack) 3 "Wrong stack-push-all.")
      (stack-push-all stack '(4 5))
      (check-equal? (stack-pop stack) 5 "Wrong stack-push-all.")
      (check-equal? (stack-pop stack) 4 "Wrong stack-push-all.")
      (check-equal? (stack-pop stack) 2 "Wrong stack-push-all.")
      (check-equal? (stack-pop stack) 1 "Wrong stack-push-all.")))))

(run-tests Stack-test)

