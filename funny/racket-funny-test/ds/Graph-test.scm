#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Graph.scm")

(define Graph-test
  (test-suite
   "Tests for Graph-test.scm"
   (test-case
    "graph is dag"
    (let ((g (CREATE-DAG))
          (a (cons "a" 123))
          (b (cons "b" 456))
          (c (cons "c" 789)))
      (graph-add-vertex! g a)
      (graph-add-vertex! g b)
      (graph-add-vertex! g c)
      (check-equal? (graph-dag? g) #t "Wrong graph-dag.")
      (check-equal? (graph-find-vertex g "a") '("a" . 123) "Wrong graph-find-vertex.")
      (check-equal? (graph-find-vertex g "b") '("b" . 456) "Wrong graph-find-vertex.")
      (check-equal? (graph-find-vertex g "c") '("c" . 789) "Wrong graph-find-vertex.")
      (check-equal? (not (member a (graph-vertices g))) #f "Wrong graph-vertices.")
      (check-equal? (not (member b (graph-vertices g))) #f "Wrong graph-vertices.")
      (check-equal? (not (member c (graph-vertices g))) #f "Wrong graph-vertices.")
      (check-equal? (member (cons "d" 111) (graph-vertices g)) #f "Wrong graph-vertices.")
      (graph-add-edge! g "a" "b")
      (graph-add-edge! g "b" "c")
      (graph-add-edge! g "a" "c")
      (check-equal? (graph-dag? g) #t "Wrong graph-dag.")
      (check-equal? (graph-topological-sort g) '(("a" . 123) ("b" . 456) ("c" . 789)) "Wrong graph-topological-sort.")))
   (test-case
    "graph is not dag"
    (let ((g (CREATE-DAG))
          (a (cons "a" 123))
          (b (cons "b" 456))
          (c (cons "c" 789)))
      (graph-add-vertex! g a)
      (graph-add-vertex! g b)
      (graph-add-vertex! g c)
      (graph-add-edge! g "a" "b")
      (graph-add-edge! g "b" "c")
      (graph-add-edge! g "c" "a")
      (check-equal? (graph-dag? g) #f "Wrong graph-dag.")))
   (test-case
    "graph update vertex!"
    (let ((g (CREATE-DAG))
          (a (cons "a" 123))
          (b (cons "b" 456))
          (c (cons "c" 789)))
      (graph-add-vertex! g a)
      (graph-add-vertex! g b)
      (graph-add-vertex! g c)
      (graph-add-edge! g "a" "b")
      (graph-add-edge! g "b" "c")
      (graph-add-edge! g "a" "c")
      (graph-update-vertex! g "a" "vertex updated!")
      (check-equal? (graph-find-vertex g "a") '("a" . "vertex updated!") "Wrong graph-update-vertex!")))))

(run-tests Graph-test)

