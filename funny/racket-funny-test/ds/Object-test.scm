#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/common/messages.scm")
(require "../../racket-funny/ds/Object.scm")

(define Object-test
  (test-suite
   "Tests for Object-test.scm"
   (test-case
    "equals"
	(let ((object1 (CREATE-Object))
              (object2 (CREATE-Object)))
          (check-equal? (ask object1 +M-Object-equals+ object2) #t "Wrong equals.")))
   (test-case
    "hash-code"
	(let ((object (CREATE-Object)))
	  (check-equal? (ask object +M-Object-hash-code+) -1 "Wrong hash code.")))
   (test-case
    "to-string"
	(let ((object (CREATE-Object)))
	  (check-equal? (ask object +M-Object-to-string+) +nil+ "Wrong to string.")))
   (test-case
    "type"
	(let ((object (CREATE-Object)))
	  (check-equal? (ask object +M-Object-type+) 'Object "Wrong type.")))))

(run-tests Object-test)

