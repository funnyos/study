#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Trie.scm")

(define Trie-test
  (test-suite
   "Tests for Trie-test.scm"
   (test-case
    "prefix-out-of-range"
    (begin
      (check-equal? (prefix-out-of-range "abc") #f "Wrong prefix-out-of-range.")
      (check-equal? (prefix-out-of-range "azde") #f "Wrong prefix-out-of-range.")
      (check-equal? (prefix-out-of-range "AdE") #t "Wrong prefix-out-of-range.")
      (check-equal? (prefix-out-of-range "s`ad`f") #f "Wrong prefix-out-of-range.")
      (check-equal? (prefix-out-of-range "$%^") #t "Wrong prefix-out-of-range.")))
   (test-case
    "trie-add and trie-find"
    (let ((trie (CREATE-Trie)))
      (trie-add trie "abc" 1)
      (trie-add trie "azde" 2)
      (trie-add trie "AdE" 3)
      (trie-add trie "s`ad`f" 4)
      (trie-add trie "$%^" 5)
      (trie-add trie "ab" 6)
      (trie-add trie "a" 7)
      (trie-add trie "abc" 8)
      (check-equal? (trie-find trie "abc") '(8 1) "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "azde") '(2) "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "AdE") '(3) "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "s`ad`f") '(4) "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "$%^") '(5) "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "ab") '(6) "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "a") '(7) "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "123") +nil+ "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "c") +nil+ "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "de") +nil+ "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "%&%") +nil+ "Wrong trie-add and trie-find.")
      (check-equal? (trie-find trie "ff") +nil+ "Wrong trie-add and trie-find.")))))

(run-tests Trie-test)

