#lang racket
(require rackunit)
(require rackunit/text-ui)

; (require "../prepare/lex-rules.scm")

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/HashTable.scm")
(require "../../racket-funny/ds/LexRule.scm")


(require "../../racket-funny/common/utils.scm")


;(define (get-fraction-number-rule rule-name)
;  (define (init-rules)
;    (define lex-rules (CREATE-HashMap))
;    (hashmap-put! lex-rules "fraction-number" (list "fraction-number" +E-direct-rule+ "<1>" "%1%" '((1 . "fraction"))))
;    (hashmap-put! lex-rules "fraction" (list "fraction" +E-plain-rule+  "<1>\\/<2>" "(make-fraction %1% %2%)" '((1 . "integer") (2 . "uinteger"))))
;    (hashmap-put! lex-rules "integer" (list "integer" +E-group-rule+  "<1>" "%1%" '((1 . "an-integer"))))
;    (hashmap-put! lex-rules "uinteger" (list "uinteger" +E-group-rule+  "<1>" "%1%" '((1 . "an-uinteger"))))
;    (hashmap-put! lex-rules "an-integer" (list "an-integer" +E-group-rule+  "<1>" "(lex \"Integer\" \"%1%\")" '((1 . "(<sign><digit>+)"))))
;    (hashmap-put! lex-rules "an-uinteger" (list "an-uinteger" +E-group-rule+  "<1>" "(lex \"Integer\" \"%1%\")" '((1 . "(<digit>+)"))))
;    (hashmap-put! lex-rules "sign" (list "sign" +E-terminal+ "[+-]?"))
;    (hashmap-put! lex-rules "digit" (list "digit" +E-terminal+ "[[:digit:]]"))
;    lex-rules)
;  (define *fraction-number-rules* (init-rules))
;  (hashmap-get *fraction-number-rules* rule-name))
;
;(define +fraction-number-rule-fetcher+ (lambda (rule-name) (get-fraction-number-rule rule-name)))


(define (get-rule-for-upgrade-test rule-name)
  (define lex-rules (CREATE-HashMap))
  (hashmap-put! lex-rules "empty-to-upgrade" (CREATE-LexRule "empty-to-upgrade" +E-rule+ "" "" '()))
  (hashmap-put! lex-rules "none-to-upgrade" (CREATE-LexRule "none-upgrade" +E-rule+ "aa" "bb" '()))
  (hashmap-put! lex-rules "normal-upgrade-1" (CREATE-LexRule "normal-upgrade-1" +E-rule+ "<1>" "%1%" '((1 . "child-rule1"))))
  (hashmap-put! lex-rules "normal-upgrade-2" (CREATE-LexRule "normal-upgrade-2" +E-rule+ "<1>..<2>" "%1%, %2%" '((1 . "child-rule1") (2 . "child-rule2"))))
  (hashmap-put! lex-rules "normal-upgrade-3" (CREATE-LexRule "normal-upgrade-3" +E-rule+ "<1>.<2>.<3>" "%1%, %2%, %3%" '((1 . "child-rule1") (2 . "child-rule2") (3 . "child-rule3"))))
  (hashmap-put! lex-rules "multi-upgrade-1" (CREATE-LexRule "multi-upgrade-1" +E-rule+ "<1>..<1>" "%1%, %1%" '((1 . "child-rule1"))))
  (hashmap-put! lex-rules "multi-upgrade-2" (CREATE-LexRule "multi-upgrade-2" +E-rule+ "<1>.<2>.<1>" "%1%, %2%, %1%" '((1 . "child-rule1") (2 . "child-rule2"))))
  (hashmap-get lex-rules rule-name))

(define (check-upgrade-result? rule-upgraded regex target child-rule-names)
  (check-equal? (lex-rule-regex rule-upgraded) regex "Wrong regex.")
  (check-equal? (lex-rule-target rule-upgraded) target "Wrong target.")
  (check-equal? (lex-rule-child-rule-names rule-upgraded) child-rule-names "Wrong child-rule-names."))

(define (get-rule-for-merge-one-test rule-name)
  (define lex-rules (CREATE-HashMap))
  (hashmap-put! lex-rules "normal-parent" (CREATE-LexRule "normal-parent" +E-rule+ "<1>..<2>" "%1%, %2%" '((1 . "normal-child1") (2 . "normal-child2"))))
  (hashmap-put! lex-rules "normal-child1" (CREATE-LexRule "normal-child1" +E-rule+ "<1>" "%1%" '((1 . "terminal-rule1"))))
  (hashmap-put! lex-rules "normal-child2" (CREATE-LexRule "normal-child2" +E-rule+ "<1>" "%1%" '((1 . "terminal-rule2"))))
  (hashmap-put! lex-rules "complex-parent" (CREATE-LexRule "complex-parent" +E-rule+ "<1>.<2>.<1>" "%1%, %2%, %1%" '((1 . "complex-child1") (2 . "complex-child2"))))
  (hashmap-put! lex-rules "complex-child1" (CREATE-LexRule "complex-child1" +E-rule+ "<1>*<2>" "%1% mult %2%" '((1 . "terminal-rule1") (2 . "terminal-rule2"))))
  (hashmap-put! lex-rules "complex-child2" (CREATE-LexRule "complex-child2" +E-rule+ "<1>#<2>" "%1% sharp %2%" '((1 . "terminal-rule1") (2 . "terminal-rule2"))))
  (hashmap-get lex-rules rule-name))

(define check-merge-one-result? check-upgrade-result?)

(define (get-rule-for-merge-test rule-name)
  (define lex-rules (CREATE-HashMap))
  (hashmap-put! lex-rules "parent" (CREATE-LexRule "parent" +E-rule+ "<1>..<2>" "%1%, %2%" '((1 . "child1") (2 . "child2"))))
  (hashmap-put! lex-rules "child1" (CREATE-LexRule "child1" +E-rule+ "<1>" "%1%" '((1 . "terminal-rule1"))))
  (hashmap-put! lex-rules "child2" (CREATE-LexRule "child2" +E-rule+ "<1>" "%1%" '((1 . "terminal-rule2"))))
  (hashmap-put! lex-rules "child3" (CREATE-LexRule "child1" +E-rule+ "<1>*<2>" "%1% mult %2%" '((1 . "terminal-rule1") (2 . "terminal-rule2"))))
  (hashmap-put! lex-rules "child4" (CREATE-LexRule "child2" +E-rule+ "<1>#<2>" "%1% sharp %2%" '((1 . "terminal-rule1") (2 . "terminal-rule2"))))
  (hashmap-get lex-rules rule-name))

(define (get-rule-for-terminate-test rule-name)
  (define lex-rules (CREATE-HashMap))
  (hashmap-put! lex-rules "term1" (list (CREATE-LexRule-Normalized "term1" +E-terminal-rule+ "[+-]?" "")))
  (hashmap-put! lex-rules "term2" (list (CREATE-LexRule-Normalized "term2" +E-terminal-rule+ "[[:digit:]]" "")))
  (hashmap-get lex-rules rule-name))

(define LexRule-test
  (test-suite
   "Tests for LexRule.scm"
   (test-case
    "lex-rule-terminate"
    (let* ((rule (CREATE-LexRule "rule" +E-rule+ "<1>..<2>" "%1%, %2%" '((1 "term1" . #f) (2 "term2" . #f))))
           (group-rule (CREATE-LexRule "group-rule" +E-group+ "<1>" "..%1%.." '((1 "(<term1><term2>+)" . #f))))
           (terminals '(("term1" "[+-]?" . #t) ("term2" "[[:digit:]]" . #t)))
           (rule-terminate (lex-rule-terminate rule terminals))
           (group-terminate (lex-rule-terminate group-rule terminals)))
      (displayln (object-to-string rule-terminate))
      (displayln (object-to-string group-terminate))))
   
;   (test-case
;    "lex-rule-upgrade"
;    (let* ((parent-id 5)
;           (rule-upgrade (lambda (rule-name) (lex-rule-upgrade (get-rule-for-upgrade-test rule-name) parent-id))))
;      (check-upgrade-result? (rule-upgrade "empty-to-upgrade")
;                             "" "" '())
;      (check-upgrade-result? (rule-upgrade "none-to-upgrade")
;                             "aa" "bb" '())
;      (check-upgrade-result? (rule-upgrade "normal-upgrade-1")
;                             "<51>"
;                             "%51%"
;                             '((51 . "child-rule1")))
;      (check-upgrade-result? (rule-upgrade "normal-upgrade-2")
;                             "<51>..<52>"
;                             "%51%, %52%"
;                             '((51 . "child-rule1") (52 . "child-rule2")))
;      (check-upgrade-result? (rule-upgrade "normal-upgrade-3")
;                             "<51>.<52>.<53>"
;                             "%51%, %52%, %53%"
;                             '((51 . "child-rule1") (52 . "child-rule2") (53 . "child-rule3")))
;      (check-upgrade-result? (rule-upgrade "multi-upgrade-1")
;                             "<51>..<51>"
;                             "%51%, %51%"
;                             '((51 . "child-rule1")))
;      (check-upgrade-result? (rule-upgrade "multi-upgrade-2")
;                             "<51>.<52>.<51>"
;                             "%51%, %52%, %51%"
;                             '((51 . "child-rule1") (52 . "child-rule2")))))
   
   (test-case
    "lex-rule-merge-one-normal"
    (let* ((normal-parent (CREATE-LexRule "normal-parent" +E-rule+ "<1>..<2>" "%1%, %2%" '((1 "normal-child1" . #f) (2 "normal-child2" . #f))))
           (normal-child1 (CREATE-LexRule "normal-child1" +E-rule+ "<1>" "%1%" '((1 "terminal-rule1" . #f))))
           (normal-child2 (CREATE-LexRule "normal-child2" +E-rule+ "<1>" "%1%" '((1 "terminal-rule2" . #f))))
           (rule-merged (lex-rule-merge-one normal-parent (list normal-child1 normal-child2)
                                            (list '("terminal-rule1" "[term-one]" . #t) '("terminal-rule2" "[term-two]" . #t)))))
      (displayln (object-to-string rule-merged))))
   (test-case
    "lex-rule-merge-one-complex"
    (let* ((complex-parent (CREATE-LexRule "complex-parent" +E-rule+ "<1>.<2>.<1>" "%1%, %2%, %1%" '((1 "complex-child1" . #f) (2 "complex-child2" . #f))))
           (complex-child1 (CREATE-LexRule "complex-child1" +E-rule+ "<1>*<2>" "%1% mult %2%" '((1 "terminal-rule1" . #f) (2 "terminal-rule2" . #f))))
           (complex-child2 (CREATE-LexRule "complex-child2" +E-rule+ "<1>#<2>" "%1% sharp %2%" '((1 "terminal-rule1" . #f) (2 "terminal-rule2" . #f))))
           (rule-merged (lex-rule-merge-one complex-parent (list complex-child1 complex-child2)
                                            (list '("terminal-rule1" "[term-one]" . #t) '("terminal-rule2" "[term-two]" . #t)))))
      (displayln (object-to-string rule-merged))))
   
;   (test-case
;    "lex-rule-merge"
;    (let* ((get-rule (lambda (rule-name) (get-rule-for-merge-test rule-name)))
;           (get-rule-list (lambda (rule-names) (map get-rule rule-names)))
;           (rule-merge (lambda (parent children) (lex-rule-merge (get-rule parent) (map get-rule-list children)))))
;      (map (lambda (t) (displayln (object-to-string t))) (rule-merge "parent" (list (list "child1" "child3") (list "child2" "child4"))))))


   ; (lex-rule-merge "parent" (list (get-rule-for-merge-test "child1" "child3") (list "child2" "child4")))
   
;   (test-case
;    "lex-rule-merge"
;    (let* ((lex-rule-fraction (CREATE-LexRule-By-Fetcher "fraction" +fraction-number-rule-fetcher+))
;           (lex-rule-integer (CREATE-LexRule-By-Fetcher "integer" +fraction-number-rule-fetcher+))
;           (lex-rule-uinteger (CREATE-LexRule-By-Fetcher "uinteger" +fraction-number-rule-fetcher+))
;           (lex-rule-merged (lex-rule-merge-one lex-rule-fraction (list lex-rule-integer lex-rule-uinteger))))
;      (check-equal? (lex-rule-regex lex-rule-merged) "<11>\\/<21>" "Wrong lex-rule-merge-one.")
;      (check-equal? (lex-rule-target lex-rule-merged) "(make-fraction %11% %21%)" "Wrong lex-rule-merge-one.")
;      (check-equal? (lex-rule-child-rule-names lex-rule-merged) '((11 . "an-integer") (21 . "an-uinteger")) "Wrong lex-rule-merge-one.")))

   
   ))

(run-tests LexRule-test)

#|


(define +R-token+           #px"\\<[^\\>]*\\>")
(define +R-angle-brackets+  #px"\\\\<|\\\\>")

(define (slice text)
  (define id-gen (make-counter))
  (multimatch
   (list (cons text +nil+))
   (list +R-angle-brackets+
         +R-token+)
   identity
   (list token-escape
         (lambda (t)
           (if (equal? t "") ""
               (list->string (list #\< (integer->char (+ (id-gen) 48)) #\>)))))
   (lambda (t) #t)))

(slice "<real>\\+<ureal>i")  ;'("" "<1>" "\\+" "<2>" "i")

(slice "<integer>")  ;'("" "<1>" "")

(slice "<uinteger>\\/<uinteger>") ;'("" "<1>" "\\/" "<2>" "")

(define (replace-with-token-id text)
  (string-append* (slice text)))

(replace-with-token-id "<real>\\+<ureal>i")

(define (fetch-string-from-brackets text)
  (map (lambda (t) (substring t 1 (- (string-length t) 1)))
       (regexp-match* +R-token+ (regexp-replace +R-angle-brackets+ text ""))))


(fetch-string-from-brackets "<real>\\+<ureal>i")



;(object-to-string (lex-rule-upgrade *lex-rule-integer* 1))
;(object-to-string (lex-rule-upgrade *lex-rule-integer* 3))

(object-to-string (lex-rule-merge-one *lex-rule-fraction* (list *lex-rule-integer* *lex-rule-uinteger*)))
; "name: fraction, type: 2, regex: <11>\\/<21>, target: (make-fraction (lex \"Integer\" \"%11%\") (lex \"Integer\" \"%21%\")), child-rule-names:  (11 (<sign><digit>+))  (21 (<digit>+)) "

(object-to-string (lex-rule-merge-one *lex-rule-fraction-number* (list *lex-rule-fraction*)))
; "name: fraction-number, type: 1, regex: <11>\\/<12>, target: (make-fraction %11% %12%), child-rule-names:  (11 integer)  (12 uinteger) "


(object-to-string (lex-rule-merge-one (lex-rule-merge-one *lex-rule-fraction-number* (list *lex-rule-fraction*)) (list *lex-rule-integer* *lex-rule-uinteger*)))

;(lex-rule-children *lex-rule-fraction*)

;(object-to-string (lex-rule-merge *lex-rule-fraction* (lex-rule-children *lex-rule-fraction*)))


(map object-to-string (lex-rule-merge *lex-rule-fraction* (list (list *lex-rule-integer*) (list *lex-rule-uinteger*))))



(define (lex-rule-terminal? rule)
  (display (lex-rule-name rule))
  (or (equal? (lex-rule-name rule) "an-integer") (equal? (lex-rule-name rule) "an-uinteger")))

(define (rule-walker rule rule-children)
  (if (lex-rule-terminal? rule)
      (list rule)
      (lex-rule-merge rule (map (lambda (r) (if (lex-rule-terminal? r) (list r) (rule-walker r (lex-rule-children r)))) rule-children))))

(define (cache-patterns)
  (let ((init-rule *lex-rule-fraction-number*))
    (rule-walker init-rule (lex-rule-children init-rule))))


(displayln "======================================")

(map object-to-string (cache-patterns))


|#

