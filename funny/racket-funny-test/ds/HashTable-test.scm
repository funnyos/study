#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/HashTable.scm")

(define HashTable-test
  (test-suite
   "Tests for HashTable-test.scm"
   (test-case
    "hashtable-put and hashtable-get"
    (let ((ht (CREATE-HashTable))
          (hm (CREATE-HashMap)))
      (hashtable-put! ht "123" "abc")
      (hashtable-put! ht "123" "def")
      (hashtable-put! ht "456" "aaa")
      (hashtable-put! ht "456" "aaa")
      (check-equal? (hashtable-get ht "123") '("def" "abc") "Wrong hashtable-get.")
      (check-equal? (hashtable-get ht "456") '("aaa") "Wrong hashtable-get.")
      (check-exn exn:fail? (lambda () (hashtable-get ht "ccc")))

      (hashtable-put! hm "123" "abc")
      (hashtable-put! hm "123" "def")
      (hashtable-put! hm "456" "aaa")
      (hashtable-put! hm "456" "aaa")
      (check-equal? (hashtable-get hm "123") '("def") "Wrong hashtable-get.")
      (check-equal? (hashtable-get hm "456") '("aaa") "Wrong hashtable-get.")
      (check-exn exn:fail? (lambda () (hashtable-get hm "ccc")))))

   (test-case
    "hashtable-contains-key"
    (let ((ht (CREATE-HashTable))
          (hm (CREATE-HashMap)))
      (hashtable-put! ht "123" "abc")
      (hashtable-put! ht "123" "def")
      (hashtable-put! ht "456" "aaa")
      (hashtable-put! ht "456" "aaa")
      (check-equal? (hashtable-contains-key? ht "123") #t "Wrong hashtable-contains-key.")
      (check-equal? (hashtable-contains-key? ht "ccc") #f "Wrong hashtable-contains-key.")
      (hashtable-put! hm "123" "abc")
      (hashtable-put! hm "123" "def")
      (hashtable-put! hm "456" "aaa")
      (hashtable-put! hm "456" "aaa")
      (check-equal? (hashtable-contains-key? hm "123") #t "Wrong hashtable-contains-key.")
      (check-equal? (hashtable-contains-key? hm "ccc") #f "Wrong hashtable-contains-key.")))
   ))

(run-tests HashTable-test)

