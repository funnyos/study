#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Queue.scm")

(define Queue-test
  (test-suite
   "Tests for Queue-test.scm"
   (test-case
    "queue-is-empty"
    (let ((queue (CREATE-EmptyQueue)))
      (check-equal? (queue-is-empty? queue) #t "Wrong queue-is-empty.")
      (enqueue queue 1)
      (check-equal? (queue-is-empty? queue) #f "Wrong queue-is-empty.")
      (dequeue queue)
      (check-equal? (queue-is-empty? queue) #t "Wrong queue-is-empty.")
      (dequeue queue)
      (check-equal? (queue-is-empty? queue) #t "Wrong queue-is-empty.")))
   (test-case
    "enqueue and dequeue"
    (let ((queue (CREATE-EmptyQueue)))
      (enqueue queue 1)
      (enqueue queue 2)
      (check-equal? (dequeue queue) 1 "Wrong enqueue and dequeue.")
      (check-equal? (dequeue queue) 2 "Wrong enqueue and dequeue.")
      (check-equal? (dequeue queue) +nil+ "Wrong enqueue and dequeue.")))
   (test-case
    "queue-insert-head"
    (let ((queue (CREATE-EmptyQueue)))
      (queue-insert-head queue 1)
      (queue-insert-head queue 2)
      (queue-insert-head queue 3)
      (check-equal? (dequeue queue) 3 "Wrong queue-insert-head.")
      (check-equal? (dequeue queue) 2 "Wrong queue-insert-head.")
      (check-equal? (dequeue queue) 1 "Wrong queue-insert-head.")
      (check-equal? (dequeue queue) +nil+ "Wrong queue-insert-head.")))
   (test-case
    "queue-append"
    (let ((queue (CREATE-EmptyQueue)))
      (queue-append queue '(1 2 3))
      (check-equal? (queue-elements queue) '(1 2 3) "Wrong queue-append.")
      (queue-append queue '(4 5 6))
      (check-equal? (queue-elements queue) '(1 2 3 4 5 6) "Wrong queue-append.")))
   (test-case
    "queue-head"
    (let ((queue (CREATE-EmptyQueue)))
      (queue-append queue '(1 2 3))
      (check-equal? (queue-head queue) 1 "Wrong queue-head.")
      (queue-append queue '(4 5 6))
      (check-equal? (queue-head queue) 1 "Wrong queue-head.")))
   (test-case
    "queue-tail"
    (let ((queue (CREATE-EmptyQueue)))
      (queue-append queue '(1 2 3))
      (check-equal? (queue-tail queue) 3 "Wrong queue-tail.")
      (queue-append queue '(4 5 6))
      (check-equal? (queue-tail queue) 6 "Wrong queue-tail.")))))

(run-tests Queue-test)

