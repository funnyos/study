#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../prepare/statements.scm")
(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/Statement.scm")

(define Statement-test
  (test-suite
   "Tests for Statement-test.scm"
   (test-case
    "new-states"
    (begin
      (check-equal? (new-states -1) '() "Wrong new-states.")
      (check-equal? (new-states 0) '() "Wrong new-states.")
      (check-equal? (length (new-states 1)) 1 "Wrong new-states.")
      (check-equal? (length (new-states 2)) 2 "Wrong new-states.")
      (check-equal? (length (new-states 2)) 2 "Wrong new-states.")
      (check-equal? (length (new-states 3)) 3 "Wrong new-states.")))

   (test-case
    "transit-table"
    (let ((t (CREATE-TransitTable))
          (starter1 (new-state))
          (starter2 (new-state))
          (state1 (new-state))
          (state2 (new-state))
          (state3 (new-state))
          (state4 (new-state))
          (state5 (new-state))
          (state6 (new-state))
          (st1 (CREATE-SymbolStatement "abc"))
          (st2 (CREATE-SymbolStatement "abc"))
          (st3 (CREATE-SymbolStatement "123"))
          (st4 (CREATE-SymbolStatement "456"))
          (st5 (CREATE-SymbolStatement "789"))
          (st6 (CREATE-SymbolStatement "qwe"))
          (st7 (CREATE-SymbolStatement "rty")))
      (transit-table-put! t starter1 (list starter1 st1 state1))
      (transit-table-put! t state1 (list starter1 st2 state2))
      (transit-table-put! t starter2 (list starter2 st3 state3))
      (transit-table-put! t state3 (list starter2 st4 state4))
      (transit-table-put! t state4 (list starter2 st5 state5))
      (transit-table-put! t state5 (list starter2 st6 state3))
      (transit-table-put! t state3 (list starter2 st7 state6))
      
      (check-equal? (transit-table-get t starter1 starter1 st1) (list starter1 st1 state1) "Wrong transit-table.")
      (check-equal? (transit-table-get t state1 starter1 st2) (list starter1 st2 state2) "Wrong transit-table.")
      (check-equal? (transit-table-get t starter2 starter2 st3) (list starter2 st3 state3) "Wrong transit-table.")
      (check-equal? (transit-table-get t state3 starter2 st4) (list starter2 st4 state4) "Wrong transit-table.")
      (check-equal? (transit-table-get t state4 starter2 st5) (list starter2 st5 state5) "Wrong transit-table.")
      (check-equal? (transit-table-get t state5 starter2 st6) (list starter2 st6 state3) "Wrong transit-table.")
      (check-equal? (transit-table-get t state3 starter2 st7) (list starter2 st7 state6) "Wrong transit-table.")

      (check-equal? (transit-table-get t state5 starter1 st6) '() "Wrong transit-table.")
      (check-equal? (transit-table-get t state1 starter2 st2) '() "Wrong transit-table.")))

   (test-case
    "statement-to-string"
    (begin
      (check-equal? (statement-to-string test-statement1) "{a b {c} {d} e {f}}" "Wrong statement-to-string.")
      (check-equal? (statement-to-string test-statement2) "{a b {{1} + {2}} {3} e {4}}" "Wrong statement-to-string.")
      (check-equal? (statement-to-string test-statement3) "{a {b {c} ..} d}" "Wrong statement-to-string.")
      (check-equal? (statement-to-string test-statement4) "{a {b {c} ...} d}" "Wrong statement-to-string.")))

   (test-case
    "to-dfa"
    (let ((t (CREATE-TransitTable)))
      (check-equal? (mpair? (statement-to-dfa test-statement1 t)) #t "Wrong to-dfa.")
      (check-equal? (mpair? (statement-to-dfa test-statement2 t)) #t "Wrong to-dfa.")
      (check-equal? (mpair? (statement-to-dfa test-statement3 t)) #t "Wrong to-dfa.")
      (check-equal? (mpair? (statement-to-dfa test-statement4 t)) #t "Wrong to-dfa.")))

   (test-case
    "test-match"
    (let* ((t (CREATE-TransitTable))
           (test-starter1 (statement-to-dfa test-statement1 t))
           (test-starter2 (statement-to-dfa test-statement2 t))
           (test-starter3 (statement-to-dfa test-statement3 t))
           (test-starter4 (statement-to-dfa test-statement4 t))
           (test-match-result1 (statement-test-match test-statement2 t test-starter1)))
      (check-equal? (mcdr (car test-match-result1)) #t "Wrong test-match.")
      (check-equal? (statement-to-string (cdr (assoc "c" (cdr test-match-result1)))) "{{1} + {2}}" "Wrong test-match.")
      (check-equal? (statement-to-string (cdr (assoc "d" (cdr test-match-result1)))) "{3}" "Wrong test-match.")
      (check-equal? (statement-to-string (cdr (assoc "f" (cdr test-match-result1)))) "{4}" "Wrong test-match.")))

   (test-case
    "meta-context"
    (let* ((script-replacements (list (list "orders" 1)))
           (meta-context (statement-meta-context test-script-statement script-replacements)))
      (check-equal? (car meta-context) "(meta:run-script {1} {2})" "Wrong meta-context.")
      (check-equal? (statement-to-string (cadr (assoc 1 (cdr meta-context)))) "{\"(cache-lex {orders})\"}" "Wrong meta-context.")
      (check-equal? (cadr (assoc 2 (cdr meta-context))) (list "orders" 1) "Wrong meta-context.")))

   (test-case
    "statement-extract"
    (let ((t1 (CREATE-OneArgument "abc"))
          (t2 (CREATE-OneStatement
               (list (CREATE-SymbolStatement "cache")
                     (CREATE-SymbolStatement "lex")
                     (CREATE-OneArgument "orders")))))
      (check-equal? (statement-extract t1) "abc" "Wrong statement-extract.")
      (check-equal? (statement-extract t2) '() "Wrong statement-extract.")))

   (test-case
    "trie-prefix-of-statement"
    (begin
      (check-equal? (trie-prefix-of-statement test-statement1) "ab`" "Wrong trie-prefix-of-statement.")
      (check-equal? (trie-prefix-of-statement test-statement2) "ab`" "Wrong trie-prefix-of-statement.")
      (check-equal? (trie-prefix-of-statement test-statement3) "a`d" "Wrong trie-prefix-of-statement.")
      (check-equal? (trie-prefix-of-statement test-statement4) "a`d" "Wrong trie-prefix-of-statement.")
      (check-equal? (trie-prefix-of-statement (CREATE-OneStatement
                                               (list (CREATE-SymbolStatement "cache")
                                                     (CREATE-SymbolStatement "lex")
                                                     (CREATE-OneArgument "orders")))) "cac" "Wrong trie-prefix-of-statement.")
      (check-equal? (trie-prefix-of-statement (CREATE-OneStatement
                                               (list (CREATE-SymbolStatement "a")
                                                     (CREATE-OneArgument "b")))) "a`" "Wrong trie-prefix-of-statement.")))

   (test-case
    "statement-equals?"
    (let ((t (CREATE-OneStatement
              (list (CREATE-SymbolStatement "a")
                    (CREATE-SymbolStatement "b")
                    (CREATE-OneArgument "c")
                    (CREATE-OneArgument "d")
                    (CREATE-SymbolStatement "e")
                    (CREATE-OneArgument "f")))))
      (check-equal? (equal? test-statement1 t) #f "Wrong statement-equals.")
      (check-equal? (statement-equals? test-statement1 t) #t "Wrong statement-equals.")))))

(run-tests Statement-test)

