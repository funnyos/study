#lang racket
(require rackunit)
(require rackunit/text-ui)

(require "../../racket-funny/common/constants.scm")
(require "../../racket-funny/ds/Object.scm")
(require "../../racket-funny/ds/LcrsTree.scm")


;(define root (CREATE-TreeNode +nil+ +nil+ +nil+ +nil+ +nil+ 0))
;
;(tree-node-add! root '("1: abc" "2: 123" "3: tyu"))
;(tree-node-add! root '("1: abc" "2: 123" "3: xyz"))
;(tree-node-add! root '("1: abc" "2: 456" "3: tyu"))
;(tree-node-add! root '("1: abc" "2: 456" "3: qqq"))
;(tree-node-add! root '("1: yyy" "2: 123" "3: tyu"))
;(tree-node-add! root '("1: yyy" "2: 123" "3: zzz"))
;(tree-node-add! root '("1: zzz" "2: 123" "3: tyu"))
;
;(tree-travel root (lambda (t) (displayln (tree-node-key t))))


;(define root (CREATE-TreeNode 'compute-domains +nil+ +nil+ +nil+ +nil+ 0))
;
;(tree-node-add! root '(("1: abc" . "L1") ("2: 123" . "L2") ("3: tyu" . "L3")))


(define LcrsTree-test
  (test-suite
   "Tests for LcrsTree-test.scm"
   (test-case
    "tree-node-add!"
    (let ((root (CREATE-TreeNode 'compute-domains +nil+ +nil+ +nil+ +nil+ 0)))
      (tree-node-add! root '(("1: abc" . "L1") ("2: 123" . "L2") ("3: tyu" . "L3")))
      (tree-node-add! root '(("1: abc" . "L1") ("2: 123" . "L2") ("3: xyz" . "L3")))
      (tree-node-add! root '(("1: abc" . "L1") ("2: 456" . "L2") ("3: tyu" . "L3")))
      (tree-node-add! root '(("1: abc" . "L1") ("2: 456" . "L2") ("3: qqq" . "L3")))))
   ))

(run-tests LcrsTree-test)


