test for detect circular dependency between components:
funny-componentA-0.1.0 -> funny-componentB-0.1.0
funny-componentB-0.1.0 -> funny-componentC-0.1.0
funny-componentC-0.1.0 -> funny-componentA-0.1.0

test for detect circular dependency between modules of component funny-component-circle-0.1.0:
moduleA -> moduleB
moduleB -> moduleC
moduleC -> moduleA