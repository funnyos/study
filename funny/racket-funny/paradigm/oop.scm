#lang racket

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/HashTable.scm")
(require "../ds/Statement.scm")
(require "parser.scm")

(define +loop-token+          "%%")
(define +loop-start+          "%%loop-start%%")
(define +loop-separator+      "%%loop-separator%%")
(define +loop-end+            "%%loop-end%%")
;(define +loop-regex+          #rx"%%.*%%")
(define +param-regex+         #rx"%[^%]*%")

(define *oop-template-cache* (CREATE-HashMap))

(define (slice t) (intermatch t +param-regex+ identity identity))

(define (slice-with-loop txt)
  (flatmap (lambda (t) (if (pair? t) (map slice t) (slice t)))
           (map (lambda (t)
                  (if (string-contains? t +loop-separator+) (string-split t +loop-separator+) t))
                (string-split-triple txt +loop-start+ +loop-end+))))

(define (build-oop-template-cache template-def)
  (if (string-contains? template-def +loop-token+)
      (slice-with-loop template-def)
      (slice template-def)))

(define (oop-template template definition)
  (let ((template-name (statement-extract template))
        (template-def (statement-extract definition)))
    (hashtable-put! *oop-template-cache* template-name (build-oop-template-cache template-def))))

(define (generate-oop-code template params)
  '())

(define (generate-class-def-code template params properties methods)
  '())

(define (def-class class-statement)
  '())

(define (def-method method-name-statement method-def-statement)
  '())

(define (new-instance class-statement assign-statement)
  '())

(define (send-message message-statement message-detail-statement object-statement)
  '())

(define (get-property property-statement object-statement)
  '())

(define (set-property property-statement object-statement value-statement)
  '())

