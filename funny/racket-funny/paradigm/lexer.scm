#lang racket

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/Object.scm")
(require "../ds/Queue.scm")
(require "../ds/HashTable.scm")
(require "../ds/LexRule.scm")
(require "global.scm")

(provide (all-defined-out))

(define +plain-rule-delimiter+  "::")
(define +group-rule-delimiter+  ":::")

(define (clear-rules)
  (hashmap-clear *lex-rules*))

(define (register-rule token lst)
  (define rule-list
    (map (lambda (r)
           (let ((rule (list-padding-tail r 3)))
             (CREATE-LexRule-Normalized token (first rule) (second rule) (third rule)))) lst))
  (hashmap-put! *lex-rules* token rule-list))

(define *lex-terminals* (list))

(define (register-terminal token regex)
  (set! *lex-terminals* (append *lex-terminals* (list (cons token (cons regex #t))))))

(define (lex-rule-children rule-names)
  (map (lambda (t) (if (hashmap-contains-key? *lex-rules* t) (hashmap-get *lex-rules* t) '())) rule-names))

; (list (list r1 r2 r3) (list r4 r5))

(define (rule-walker rule-set)
  (cond ((andmap lex-terminal? rule-set) rule-set)
        (else (rule-walker (flatmap (lambda (r) (lex-rule-merge r (lex-rule-children (map cadr (lex-rule-child-rule-names r))) *lex-terminals*)) rule-set)))))

;(define (rule-expand rule-list)
;  (if (andmap lex-terminal? rule-list) rule-list
;      (flatmap (lambda (r) (lex-rule-merge r (lex-rule-children (map cdr (lex-rule-child-rule-names r))))) rule-list)))

; rule-walker  (list r1 r2)   (list (list r3) (list r4 r5))
;
; lex-rule-merge r1 (list (list r3) (list r4 r5))

; (lex-rule-merge rule (map (lambda (r) (rule-walker r (lex-rule-children (map cdr (lex-rule-child-rule-names r))))) rule-children))

(define (cache-patterns1)
  (map
   (lambda (t)
     (let* ((init-rule (first (hashmap-get *lex-rules* t)))
            (init-rule-children (lex-rule-children (map cdr (lex-rule-child-rule-names init-rule)))))
       (hashmap-put! *lex-pattern-cache* t (rule-walker init-rule init-rule-children))))
   (queue-elements *lex-orders*)))


(define (cache-patterns)
  (map
   (lambda (t)
     (let* ((rules (rule-walker (hashmap-get *lex-rules* t)))
            (x (map displayln (map object-to-string rules))))
       (hashmap-put! *lex-pattern-cache* t rules)))
   (queue-elements *lex-orders*)))

;step 6:
;regex  => <1111>\\/<1121>\\+<121>i
;target => (make-complex (make-fraction $1111 $1121) $121)
;map    => 1111 : ("(<sign><digit>+)" . "(lex \"Integer\" \"$1\")"), 1121 : ("(<digit>+)" . "(lex \"Integer\" \"$1\")"), 121 : ("(<digit>+)" . "(lex \"Integer\" \"$1\")")
;
(define (pattern-to-function pattern text)
  (let* ((regex (lex-rule-regex pattern))
         (target (lex-rule-target pattern))
         (child-rule-names (lex-rule-child-rule-names pattern))
         (regex-replaced (string-replace-by-alist regex (map (lambda (t) (cons (format "<~a>" (car t)) (cadr t))) child-rule-names)))
         (x (displayln regex-replaced))
         (y (displayln target))
         ; (rule-map (third pattern))
         (token-ids (fetch-string-from-brackets regex))
         (regex-to-match (pregexp regex-replaced))
         (match-result (regexp-match regex-to-match text))
         (z (displayln match-result)))
    (if match-result
        (string-replace-by-alist target (map (lambda (id value) (cons (string-append "%" id "%") value)) token-ids (cdr match-result)))
        '())))

(define (lex-to-function token text)
  (let ((lst (hashmap-get *lex-pattern-cache* token)))
    (displayln lst)
    (safe car (filter not-null? (map (lambda (p) (pattern-to-function p text)) lst)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; cache pattern
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|

(define (lex-rule-terminal? rule)
  (let ((child-rules (fetch-string-from-brackets (lex-rule-regex rule))))
    (= (length child-rules) (length (filter (lambda (t) (hashmap-contains-key? *lex-terminals* t)) child-rules)))))

(define (rule-walker rule rule-children)
  ;(displayln "==============")
  ;(displayln (object-to-string rule))
  ;(displayln "--------------")
  ;(map (lambda (t) (displayln (object-to-string t))) rule-children)
  ;(displayln "==============")
  (cond ((lex-rule-terminal? rule) (list rule))
        ((andmap null? rule-children) (list rule))
        ((andmap lex-rule-terminal? rule-children) rule-children)
        (else (lex-rule-merge rule (map (lambda (r) (rule-walker r (lex-rule-children r))) rule-children)))))

(define (init-lex-rule rule-name)
  (CREATE-LexRule rule-name +E-direct-rule+ "<1>" "%1%" (list (cons 1 rule-name)) rule-fetcher))

(define (cache-patterns)
  (map
   (lambda (t)
     (let ((init-rule (init-lex-rule t)))
       (hashmap-put! *lex-pattern-cache* t (rule-walker init-rule (lex-rule-children init-rule)))))
   (queue-elements *lex-orders*)))

|#







