#lang racket

(require racket/set)

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "../ds/Stack.scm")
(require "../ds/HashTable.scm")
(require "../ds/SortedSet.scm")

(define token-equal? equal?)
(define (token-less-then? t1 t2)
  (cond
    ((and (symbol? t1) (string? t2)) #t)
    ((and (symbol? t1) (symbol? t2)) (symbol<? t1 t2))
    ((and (string? t1) (string? t2)) (string<? t1 t2))
    (else #f)))

(define (token-set)
  (CREATE-SortedSet token-equal? token-less-then?))
(define (token-set-of-value v)
  (let ((s (token-set)))
    (sorted-set-add! s v)
    s))
(define (token-set-of-values values)
  (let ((s (token-set)))
    (sorted-set-add-all! s values)
    s))

(define (token-sets-union! hashmap key value)
  (if (hashmap-contains-key? hashmap key)
      (let ((old-set (hashmap-get hashmap key))
            (new-set (token-set)))
        (sorted-set-union! new-set old-set)
        (sorted-set-union! new-set value)
        (hashmap-put! hashmap key new-set))
      (hashmap-put! hashmap key value))
  hashmap)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; fixed point algorithm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (fixed-point point-value equal-func next-func)
  (let ((next-point-value (next-func point-value)))
    (if (equal-func point-value next-point-value)
        point-value
        (fixed-point next-point-value equal-func next-func))))



(define (LL-1-Parser grammar)
  (define production-list (hashmap-get grammar 'production-list))
  (define terminals (hashmap-get grammar 'terminals-set))
  (define non-terminals (hashmap-get grammar 'non-terminals-set))
  (define start-symbol (hashmap-get grammar 'start-symbol))

  (define productions
    (append* (map (lambda (t) (map (lambda (p) (list (first t) p)) (second t))) production-list)))
  (define productions-map
    (let ((hm (CREATE-HashMap)))
      (map (lambda (t) (hashmap-put! hm (first t) (second t))) production-list)
      hm))

  (define (empty? token) (equal? token 'EMPTY))
  (define (end? token) (equal? token 'END))
  (define (non-terminal? token) (set-member? non-terminals token))
  ; 'EMPTY and 'END are included
  (define (terminal? token) (not (non-terminal? token)))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; compute nullable:
  ;
  ; NULLABLE = {};
  ; while (nullable is still changing)
  ;     foreach (production p: X -> β)
  ;         if (β == ε)
  ;             NULLABLE U= {X}
  ;         if (β == Y1 ... Yn)
  ;             if (Y1 ∈ NULLABLE && ... && Yn ∈ NULLABLE)
  ;                 NULLABLE U= {X}
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (define (compute-nullable)
    (define (nullable-prod-body nullable-set prod-head prod-body)
      (cond
        ((null? prod-body) nullable-set)
        ((empty? (car prod-body)) (token-set-of-value prod-head))
        ((andmap (lambda (t) (and (non-terminal? t) (sorted-set-contains? nullable-set t))) prod-body) (token-set-of-value prod-head))
        (else nullable-set)))
    (define (next-nullable-set nullable-set productions)
      (if (null? productions) nullable-set
          (sorted-set-union!
           (nullable-prod-body nullable-set (caar productions) (cadar productions))
           (next-nullable-set nullable-set (cdr productions)))))
    (fixed-point (token-set) sorted-set-equal? (lambda (t) (next-nullable-set t productions))))

  (define nullable-set (compute-nullable))
  (define (not-nullable? token) (not (sorted-set-contains? nullable-set token)))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; compute first sets:
  ;
  ; foreach (nonterminal N)
  ;     FIRST(N) = {}
  ;
  ; while (some set is changing)
  ;     foreach (production p: N -> β1 ... βn)
  ;         foreach (βi from β1 upto βn) 
  ;             if (β1 == a ...)
  ;                 FIRST(N) U= {a}
  ;                 break
  ;             if (β1 == M ...)
  ;                 FIRST(N) U= FIRST(M)
  ;                 if(M is not in NULLABLE)
  ;                     break
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (define (init-first-sets)
    (let ((hashmap (CREATE-HashMap)))
      (map (lambda (t) (hashmap-put! hashmap t (token-set))) non-terminals)
      hashmap))
  (define (first-sets-equal? m1 m2)
    (hashmap-equal? m1 m2 sorted-set-equal?))
  
  (define (compute-first-sets)
    (define (recur-firstset-prod-body first-set-hash prod-head prod-body)
      (if (null? prod-body) first-set-hash
          (let ((token (car prod-body))
                (rest-tokens (cdr prod-body)))
            (if (terminal? token)
                (token-sets-union! first-set-hash prod-head (token-set-of-value token))
                (if (not-nullable? token)
                    (token-sets-union! first-set-hash prod-head (hashmap-get first-set-hash token))
                    (recur-firstset-prod-body first-set-hash prod-head rest-tokens))))))
    (define (next-first-set first-set-hash productions)
      (if (null? productions) first-set-hash
          (next-first-set
           (recur-firstset-prod-body first-set-hash
                                     (caar productions)
                                     (cadar productions))
           (cdr productions))))
    (fixed-point (init-first-sets) first-sets-equal? (lambda (t) (next-first-set (hashmap-copy t) productions))))

  (define first-sets (compute-first-sets))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; compute follow sets:
  ;
  ; foreach (nonterminal N)
  ;     FOLLOW(N) = {}
  ; while (some set is changing)
  ;     foreach (production p: N -> β1 ... βn)
  ;         temp = FOLLOW(N)
  ;         foreach (βi from βn downto β1)
  ;             if(βi == a...)
  ;                 temp = {a}
  ;             if (βi == M ...)
  ;                 FOLLOW(M) U= temp
  ;                 if(M is not NULLABLE)
  ;                     temp = FIRST(M)
  ;                 else temp U= FIRST(M)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (define (init-follow-sets)
    (let ((hm (CREATE-HashMap)))
      (map (lambda (t) (hashmap-put! hm t (token-set))) non-terminals)
      (hashmap-put! hm start-symbol (token-set-of-value 'END))
      hm))
  (define (follow-sets-equal? m1 m2)
    (hashmap-equal? m1 m2 sorted-set-equal?))
  
  (define (compute-follow-sets)
    (define (recur-follow-prod-body follow-set-hash prod-head prod-body temp)            
      (if (null? prod-body) follow-set-hash
          (let ((token (car prod-body))
                (rest-tokens (cdr prod-body))
                (next-temp '()))
            (if (terminal? token) (set! next-temp (token-set-of-value token))
                (let ((token-first-set (hashmap-get first-sets token)))
                  (token-sets-union! follow-set-hash token temp)
                  (set! next-temp (if (not-nullable? token)
                                      token-first-set
                                      (sorted-set-union! (sorted-set-union! (token-set) temp) token-first-set)))))
            (recur-follow-prod-body follow-set-hash prod-head rest-tokens next-temp))))
    (define (next-follow-set follow-set-hash productions)
      (if (null? productions) follow-set-hash
          (next-follow-set
           (recur-follow-prod-body follow-set-hash
                                   (caar productions)
                                   (reverse (cadar productions))
                                   (hashmap-get follow-set-hash (caar productions)))
           (cdr productions))))
    (define (remove-empty follow-set-hash)
      (hashmap-remove-in-value follow-set-hash 'EMPTY sorted-set-remove!)
      follow-set-hash)
    (remove-empty (fixed-point (init-follow-sets) follow-sets-equal? (lambda (t) (next-follow-set (hashmap-copy t) productions)))))

  (define follow-sets (compute-follow-sets))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; compute select sets:
  ;
  ; foreach(production p)
  ;   FIRST_S(p) = {}
  ; calculte_FIRST_S(production p: N->β1 ... βn)
  ;   foreach (βi from β1 to βn)
  ;     if (βi== a ...)
  ;       FIRST_S(p) U= {a}
  ;       return;
  ;     if (βi== M ...)
  ;       FIRST_S(p) U= FIRST(M)
  ;       if (M is not NULLABLE)
  ;         return;
  ;     FIRST_S(p) U= FOLLOW(N)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (define (compute-select-sets)
    (define (recur-select-prod-body select-set prod-head prod-body)
      (if (null? prod-body) select-set
          (let ((token (car prod-body))
                (rest-tokens (cdr prod-body)))
            (if (empty? token) (sorted-set-union! select-set (hashmap-get follow-sets prod-head))
                (if (terminal? token)
                    (sorted-set-union! select-set (token-set-of-value token))
                    (let ((token-first-set (hashmap-get first-sets token)))
                      (sorted-set-union! select-set token-first-set)
                      (if (not-nullable? token) select-set
                          (begin
                            (sorted-set-union! select-set (hashmap-get follow-sets prod-head))
                            (recur-select-prod-body select-set prod-head rest-tokens)))))))))
    (define (next-select-set productions)
      (if (null? productions) '()
          (cons (cons (car productions)
                      (recur-select-prod-body
                       (token-set)
                       (caar productions)
                       (cadar productions)))
                (next-select-set (cdr productions)))))
    (next-select-set productions))

  (define select-sets (compute-select-sets))


  (define (M X a)
    (let* ((select-set (filter (lambda (s) (token-equal? (caar s) X)) select-sets))
           (token-select-set (filter (lambda (s) (sorted-set-contains? (cdr s) a)) select-set)))
      (if (null? token-select-set) '()
          (caar token-select-set))))

  (define (error-entry? t) #f)
  
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; LL-1 Parser
  ;
  ; let a be the first symbol of w ;
  ; let X be the top stack symbol;
  ; while ( X≠$ ) { /* stack is not empty */
  ;   if ( X = a ) pop the stack and let a be the next symbol of w ;
  ;   else if ( X is a terminal ) error();
  ;   else if ( M [X, a] is an error entry ) error();
  ;   else if ( M [X, a] = X -> Y1 Y2 ... Yk ) {
  ;     output the production X -> Y1 Y2 ... Yk;
  ;     pop the stack;
  ;     push Yk, Yk-1, ... Y1 onto the stack, with Y1 on top;
  ;   }
  ;   let X be the top stack symbol;
  ; }
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (define (recur-parse tokens stack)
    (if (stack-is-empty? stack) '()
        (let ((X (stack-peek stack))
              (next-tokens tokens)
              (a (car tokens)))
          (cond
            ((empty? X)
             (stack-pop stack))
            ((token-equal? X a)
             (stack-pop stack)
             (set! next-tokens (cdr tokens)))
            ((and (terminal? X) (not (empty? X)))
             (error "terminal?"))
            (else
             (let ((p (M X a)))
               (cond
                 ((error-entry? p) (error "error entry"))
                 ((null? p) '())
                 (else
                  (displayln p)
                  (stack-pop stack)
                  (map (lambda (t) (stack-push stack t)) (reverse (second p))))))))
          (recur-parse next-tokens stack))))

  (lambda (tokens)
    (let ((stack (CREATE-Stack)))
      (stack-push stack start-symbol)
      (recur-parse tokens stack))))

(define (LR-0-Parser grammar)
  '())

(define (LR-1-Parser grammar)
  '())

(define (SLR-1-Parser grammar)
  '())

(define (LALR-1-Parser grammar)
  '())



(define (test-grammar)
  (define production-list
    '(("S"  (("E" END)))
      ("E"  (("T" "E'")))
      ("E'" (("+" "T" "E'")
             (EMPTY)))
      ("T"  (("F" "T'")))
      ("T'" (("*" "F" "T'")
             (EMPTY)))
      ("F"  (("(" "E" ")")
             ("id")))))
  (define terminals '("+" "*" "(" ")" "id" END))
  (define non-terminals '("S" "E" "T" "E'" "F" "T'"))
  (define grammar (CREATE-HashMap))
  (hashmap-put! grammar 'production-list production-list)
  (hashmap-put! grammar 'terminals-set terminals)
  (hashmap-put! grammar 'non-terminals-set non-terminals)
  (hashmap-put! grammar 'start-symbol "S")
  grammar)

(define tst (test-grammar))

((LL-1-Parser tst) '("id" "+" "id" "*" "id" END))

