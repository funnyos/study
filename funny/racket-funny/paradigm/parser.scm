#lang racket


; production is like
;
; expr : expr PLUS term
;
; name     - Name of the production.  For example 'expr'
; prod     - A list of symbols on the right side ['expr','PLUS','term']
; prec     - Production precedence level
; number   - Production number.
; func     - Function that executes on reduce

; Return the nth lr_item from the production (or None if at the end)

; ActionPart:  (label code-string)
; ActionProduction?
; emit?
; lalr_ ?
; LalrItemCore: (the-production dot-pos core-hash-cache symbol-after-dot)


(define (Production number name prod precedence func file line)
  (list number name prod precedence func file line))















