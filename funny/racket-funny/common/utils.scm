#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Utilities
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide (all-defined-out))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: make counter
; Input:
; Output: counter function
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (make-counter)
  (define counter 0)
  (lambda ()
    (set! counter (+ counter 1))
    counter))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Message generator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define msg-gen (make-counter))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; State generator
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define state-gen (make-counter))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: make empty list
; Input:
;   size -> size of empties
; Output: empty list
; Example:
;   (make-empties 0) => '()
;   (make-empties 1) => '(())
;   (make-empties 2) => '(() ())
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (make-empties size)
  (if (= size 0) (list)
      (append (list '()) (make-empties (- size 1)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: make enumeration
; Input:
;   start -> the start number
;   end   -> the end number
; Output: enumeration list
; Example:
;   (make-enumeration 1 1) => '(1)
;   (make-enumeration 1 3) => '(1 2 3)
;   (make-enumeration 1 0) => '()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (make-enumeration start end)
  (if (> start end) '()
      (if (= start end) (list start)
          (append (list start) (make-enumeration (+ start 1) end)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: make association list
; Input:
;   klist -> key list
;   vlist -> value list
; Output: association list made by klist and vlist
; Example:
;   (make-alist '(1 2 3) '(a v x)) => '((1 . a) (2 . v) (3 . x))
;   (make-alist '(1 2) '(a v x))   => '((1 . a) (2 . v))
;   (make-alist '(1 2 3) '(a v))   => '((1 . a) (2 . v))
;   (make-alist '() '())           => '()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (make-alist klist vlist)
  (if (or (null? klist) (null? vlist)) '()
      (append (list (cons (car klist) (car vlist))) (make-alist (cdr klist) (cdr vlist)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: make associated enumeration list
; Input:
;   vlist -> value list
; Output: association enumeration list made by vlist
; Example:
;   (make-enumeration-list '(a v x)) => '((1 . a) (2 . v) (3 . x))
;   (make-enumeration-list '())      => '()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (make-enumeration-list vlist)
  (make-alist (make-enumeration 1 (length vlist)) vlist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: split list by given element
; Input:
;   lst -> list to be splitted
;   x   -> the element
; Output: list of splitted list
; Example:
;   (split-by '(a b c x d x e x f) 'x) => '((a b c) (d) (e) (f))
;   (split-by '(x) 'x)                 => '(() ())
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (split-by lst x)
  (foldr (lambda (element next)
           (if (eqv? element x)
               (cons empty next)
               (cons (cons element (car next)) (cdr next))))
         (list empty) lst))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: produce list where inserted list b to list a one by one
; Input:
;   a -> list a
;   b -> list b
; Output: list produced
; Example:
;   (interleave '(1 2 3 4 5) '(a b c)) => '(1 a 2 b 3 c 4 5)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (interleave a b)
  (if (null? a)
      b
      (cons (car a)
            (interleave b (cdr a)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: produce list where inserted element to each gap of nodes
; Input:
;   element -> element
;   nodes   -> nodes
; Output: list produced
; Example:
;   (insert-between '(a b c) 'x) => '(a x b x c)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (insert-between nodes element)
  (if (empty? (cdr nodes))
      (list (car nodes))
      (cons (car nodes)
            (cons element (insert-between (cdr nodes) element)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: read lines from port
; Input:
;   port -> port
; Output: list of strings separated by newline
; Example:
;   (read-lines (open-input-string "a\nbb\ncd\ne")) => '("a" "bb" "cd" "e")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (read-lines port)
  (define (next-line-it port)
    (let ((line (read-line port 'any)))
      (if (eof-object? line)
          '()
          (append (list line)
                  (next-line-it port)))))
  (let ((lines (next-line-it port)))
    (close-input-port port)
    lines))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: not null predicate
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (not-null? t) (not (null? t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: not equal predicate
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (not-equal? a b) (not (equal? a b)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: non blank string predicate
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (non-blank-string? t) (non-empty-string? (string-trim t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: flatmap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (flatmap f lst)
  (apply append (map f lst)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: safe function call
; Example:
;   (safe car (cons 'a 'b)) => 'a
;   (safe car '()) => '()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (safe f t)
  (if (null? t) t
      (f t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: cartesian product of lists
; Example:
;   (cartesian-product-list '((4 5) (d e f) (#t))) => '((4 d #t) (4 e #t) (4 f #t) (5 d #t) (5 e #t) (5 f #t))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (cartesian-product-list lists)
  (foldr (lambda (xs ys)
           (append-map (lambda (x)
                         (map (lambda (y)
                                (cons x y))
                              ys))
                       xs))
         '(())
         lists))

;(cartesian-product-list '((("11" "integer-rule1") ("11" "integer-rule2")) (("12" "uinteger-rule1") ("12" "uinteger-rule2"))))

; '( (a . 123) (b . 456)) '((123 . 789)) => '( (a . 789) (b . 456))
(define (link-assoc alist1 alist2)
  (map (lambda (t)
         (let* ((link-key (cdr t))
                (pair (assoc link-key alist2)))
           (cons (car t) (if pair (cdr pair) link-key))))
       alist1))

(define (transformed-link-assoc alist1 alist2 linked-op unlinked-op)
  (map (lambda (t)
         (let* ((link-key (cdr t))
                (pair (assoc link-key alist2)))
           (if pair
               (cons (linked-op (car t)) (cdr pair))
               (cons (unlinked-op (car t)) link-key))))
       alist1))

; (link-assoc '( (a . 123) (b . 456)) '((123 . 789) (455 . 789)))

; (transformed-link-assoc '( ("1" . 123) ("2" . 456)) '((123 . 789) (455 . 789)) string->number (lambda (t) t))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: get value of alist
; Example:
;   (assoc-value '((a . 123) (b . 456)) 'a)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (assoc-value alist key)
  (cdr (assoc key alist)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: get replaced string by alist
; Example:
;   (string-replace-by-alist "aebea" '(("a" . "123") ("b" . "456")))
; Warning: one should not include another
;   (string-replace-by-alist "<integer>\\/<uinteger>" '(("integer" . "1") ("uinteger" . "2"))) => "<1>\\/<u1>"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (string-replace-by-alist str alist)
  (if (null? alist) str
      (string-replace-by-alist (string-replace str (caar alist) (cdar alist)) (cdr alist))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: every element in collection feeds the predicate
; Input:
;   predicate -> predicate
;   collection -> collection
; Output: feeds or not feeds
; Example:
;   (every? char? (list #\a #\b #\c)) => #t
;   (every? char? (list "a" #\b #\c)) => #f
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (every? predicate collection)
  (foldr (lambda (x y) (and x y)) #t (map predicate collection)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: all elements in collection feeds the predicate
; Input:
;   predicate -> predicate
;   collection -> collection
; Output: feeds or not feeds
; Example:
;   (all? char? (list #\a #\b #\c)) => #t
;   (all? char? (list "a" #\b #\c)) => #f
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define all? every?)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: some elements in collection feeds the predicate
; Input:
;   predicate -> predicate
;   collection -> collection
; Output: feeds or not feeds
; Example:
;   (some? char? (list #\a #\b #\c)) => #f
;   (some? char? (list "a" #\b #\c)) => #t
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (some? predicate collection)
  (not (every? predicate collection)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: any element in collection feeds the predicate
; Input:
;   predicate -> predicate
;   collection -> collection
; Output: feeds or not feeds
; Example:
;   (any? char? (list "a" #\b #\c)) => #t
;   (any? char? (list "a" "b" "c")) => #f
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (any? predicate collection)
  (foldr (lambda (x y) (or x y)) #f (map predicate collection)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: none of elements in collection feeds the predicate
; Input:
;   predicate -> predicate
;   collection -> collection
; Output: feeds or not feeds
; Example:
;   (none? char? (list "a" #\b #\c)) => #f
;   (none? char? (list "a" "b" "c")) => #t
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (none? predicate collection)
  (not (any? predicate collection)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: concatenate collections
; Input:
;   collections -> collections
; Output: collection concatenated
; Example:
;   (concat '(1 2 3) '(4 5 6) '(7 8 9)) => '(1 2 3 4 5 6 7 8 9)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (concat . collections)
  (foldr append '() collections))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: mutable list to immutable list
; Input:
;   lst -> the list mutable
; Output: immutable list transformed
; Example:
;   (mlist->list (mcons 1 (mcons 2 (mcons 3 '())))) => '(1 2 3)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (mlist->list lst)
  (if (null? lst) '()
      (cons (mcar lst) (mlist->list (mcdr lst)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: immutable list to mutable list
; Input:
;   lst -> the list immutable
; Output: mutable list transformed
; Example:
;   (list->mlist '(1 2 3)) => (mcons 1 (mcons 2 (mcons 3 '())))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (list->mlist lst)
  (if (null? lst) '()
      (mcons (car lst) (list->mlist (cdr lst)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: mutable map
; Input:
;   lst -> the list mutable
; Output: mutable map results
; Example:
;   (mmap - (mcons 1 (mcons 2 (mcons 3 '())))) => '(-1 -2 -3)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (mmap f lst)
  (if (null? lst) '()
      (mcons (f (mcar lst)) (mmap f (mcdr lst)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: mutable filter
; Input:
;   pred -> the predicate
;   lst -> the list mutable
; Output: mutable filter results
; Example:
;   (mfilter even? (mcons 1 (mcons 2 (mcons 3 '())))) => (mcons 2 '())
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (mfilter pred lst)
  (cond
    ((null? lst)
     null)
    ((pred (mcar lst))
     (mcons (mcar lst) (mfilter pred (mcdr lst))))
    (else
     (mfilter pred (mcdr lst)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: Split text where matched by regex, and then transform each item by function(split or match) to form a list
; Input:
;   text -> text to process
;   re -> regex
;   split-f -> split function
;   match-f -> match function
; Output: list produced
; Example:
;   (intermatch "MAKE-%className% self %properties%" #rx"%[^%]*%" identity identity) => '("MAKE-" "%className%" " self " "%properties%")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (intermatch text re split-f match-f)
  (interleave
   (map split-f (regexp-split re text))
   (map match-f (regexp-match* re text))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: Split text into alist and expand alist where matched by regexes, and then transform each item by function(split or match)
; Input:
;   text-list -> text list to process
;   re-list -> regex list to split
;   split-f -> split function
;   match-f-list -> match function list
;   expand-p -> expand predicate
; Output: list produced
; Example:
;
;(define (slice text)
;  (multimatch
;   (list (cons text "text-node"))
;   (list #px"[A-Z]+\\[[^\\]]*\\]"
;         #px"\\{[a-z][a-z|0-9|-]+\\}"
;         #px"\\\\\\{|\\\\\\}")
;   (lambda (t) (cons t "text-node"))
;   (list (lambda (t) (cons (token-escape t) "macro-node"))
;         (lambda (t) (cons (token-escape t) "argument-node"))
;         (lambda (t) (cons (token-escape t) "text-node")))
;   (lambda (t) (eq? (cdr t) "text-node"))
;   ))
;(slice "{function} > \\{(EXTRACT[{name}] TARGET[{function}])\\}")
;
;=>
;
;'(("" . "text-node")
;  ("{function}" . "argument-node")
;  (" > " . "text-node")
;  ("{" . "text-node")
;  ("(" . "text-node")
;  ("EXTRACT[{name}]" . "macro-node")
;  (" " . "text-node")
;  ("TARGET[{function}]" . "macro-node")
;  (")" . "text-node")
;  ("}" . "text-node")
;  ("" . "text-node"))
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (multimatch text-list re-list split-f match-f-list expand-p)
  (if (null? re-list) text-list
      (multimatch
       (flatmap
        (lambda (t)
          (if (expand-p t)
              (intermatch (if (pair? t) (car t) t) (car re-list) split-f (car match-f-list))
              (cons t '())))
        text-list)
       (cdr re-list)
       split-f
       (cdr match-f-list)
       expand-p)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: char followed by backslash to produce escape char
; Input:
;   c -> char
; Output: the escape char
; Example:
;   (escape #\n) => #\newline
;   (escape #\a) => #f
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (escape c)
  (let ((t (assoc c '((#\\ . #\\) (#\t . #\tab) (#\n . #\newline) (#\r . #\return) (#\" . #\")))))
    (if t (cdr t) #f)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: escape token string
; Input:
;   t -> the token
; Output: the escaped token string
; Example:
;   (token-escape "\\\\abc") => "\\abc"
;   (token-escape "\\\"") => "\""
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (token-escape t)
  (if (string-prefix? t "\\")
      (substring t 1)
      t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: strip and extract string between left and right
; Input:
;   str -> the string to strip
;   left -> left side
;   right -> right side
; Output: the string extracted
; Example:
;   (string-strip "( a b c )" "(" ")") => "a b c"
;   (string-strip "[abc]" "[" "]") => "abc"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (string-strip str left right)
  (string-trim (string-trim (string-trim str left #:left? #t) right #:right? #t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: defined for map-chain
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (map-chain-inner args)
  (cond ((null? args) '())
        ((= (length args) 1) (first args))
        (else (map (first args) (map-chain-inner (rest args))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: chain map functions
; Example:
;   (map-chain (lambda (t) (string-append t "abc")) (lambda (t) (format "~a" t)) '(1 2 3)) => '("1abc" "2abc" "3abc")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (map-chain . args)
  (map-chain-inner args))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: padding tail of list, make it easier to process sometimes
; Example:
;   (list-padding-tail (list 1 2 3) 4) => '(1 2 3 ())
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (list-padding-tail lst size)
  (define len (length lst))
  (if (>= len size) lst
      (append lst (make-empties (- size len)))))

; filter-by-car
(define (filter-by-car alist carlist)
  (filter (lambda (node)
            (any? (lambda (t) (equal? (car node) t)) carlist))
          alist))

; filter-by-cdr
(define (filter-by-cdr alist cdrlist)
  (filter (lambda (node)
            (any? (lambda (t) (equal? (cdr node) t)) cdrlist))
          alist))

; (construct-path "aa" "bb" "cc") => "aa\\bb\\cc"
(define (construct-path base . path)
  (string-join (append (list base) path) "\\"))

(define (scan-files base ext)
  (find-files (lambda (p) (path-has-extension? p ext)) base))

(define (read-config file-path head-predicate)
  (let ((lst (file->lines file-path))
        (r (list))
        (current-block-car '())
        (current-block-cdr ""))
    (for-each (lambda (t)
                (if (head-predicate t)
                    (begin (set! r (append r (list (cons current-block-car (string-trim current-block-cdr)))))
                           (set! current-block-car t)
                           (set! current-block-cdr ""))
                    (set! current-block-cdr (string-append current-block-cdr "\n" t))))
              lst)
    (set! r (append r (list (cons current-block-car (string-trim current-block-cdr)))))
    (safe cdr r)))

(define (parse-assignments assignment-string)
  (map (lambda (t)
         (let ((assignment (string-split t "=")))
           (cons (first assignment) (second assignment))))
       (string-split assignment-string)))


(define (get-clean-code code)
  (define (find-first-newline string index)
    (if (char=? (string-ref string index) #\newline) (+ index 1)
        (find-first-newline string (+ index 1))))
  (let ((trim-code (string-trim code)))
    (if (string-prefix? trim-code "#lang")
        (substring trim-code (find-first-newline trim-code 0))
        trim-code)))

(define (debug-info-old f . args)
  (begin (apply f args)
         (map display args)))

(define (debug-element tag element)
  (begin
    (display "\n\n[")
    (display tag)
    (display "]\n")
    (display element)
    element))

(define (debug-function tag f . args)
  (begin
    (display "\n\n[")
    (display tag)
    (display "]\ninputs:")
    (map display args)
    (display #\newline)
    (display "output:")
    (let ((r (apply f args)))
      (display r)
      r)))

;(intermatch "(define (CREATE-%className%) (create-instance MAKE-%%className%% %properties%))" #rx"%%.*%%" identity identity)

; (string-split-triple "aaa%%start%%bbb%%end%%ccc" "%%start%%" "%%end%%") => '("aaa" "bbb" "ccc")
(define (string-split-triple text start end)
  (flatmap (lambda (t) (string-split t end)) (string-split text start)))



;(display (read-config "../funny-example/component.meta" (lambda (s) (and (string-prefix? s "[") (string-suffix? s "]")))))


#|
(define txt
  "(define (CREATE-%className%) (create-instance MAKE-%className% %properties%))

(define (MAKE-%className% self %properties%)
  (let ((P-%parentClassName% (MAKE-%parentClassName% self %parentProperties%)))
    (lambda (message)
      (case message
%%loop-start%%
          ((G-%property%) (lambda () %property%))
          ((S-%property%) (lambda (NEW-%property%) (set! %property% NEW-%property%)))
%%loop-separator%%
          ((M-%methodName%) (lambda (%methodArgs%) %methodDef% ))
%%loop-end%%
        ((class) (lambda () '%className%))
        ((parent-class) (lambda () '%parentClassName%))
        ((is-a) (lambda (type) (case type (('Object) #f) (('%className%) #t) (else (get-method is-a '%parentClassName%)))))
        ((properties) (lambda () (vector %properties%)))
        ((methods) (lambda () (vector %methods%)))
        (else (get-method message P-%parentClassName%))))))")


;(string-split-triple txt "%%loop-start%%" "%%loop-end%%")


;(map (lambda (t) (string-split t "%%loop-separator%%")) (string-split-triple txt "%%loop-start%%" "%%loop-end%%"))

(define (f t) (intermatch t #rx"%[^%]*%" identity identity))

(flatmap (lambda (t) (if (pair? t) (map f t) (f t)))
         (map (lambda (t)
                (if (string-contains? t "%%loop-separator%%") (string-split t "%%loop-separator%%") t))
              (string-split-triple txt "%%loop-start%%" "%%loop-end%%")))

|#

