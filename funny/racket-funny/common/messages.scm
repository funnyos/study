#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Global messages definition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide (all-defined-out))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Object class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-Object-equals+      1001)
(define +M-Object-hash-code+   1002)
(define +M-Object-to-string+   1003)
(define +M-Object-type+        1004)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Queue class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-Queue-is-empty+     2001)
(define +M-Queue-enqueue+      2002)
(define +M-Queue-insert-head+  2003)
(define +M-Queue-dequeue+      2004)
(define +M-Queue-append+       2005)
(define +M-Queue-elements+     2006)
(define +G-Queue-head+         2007)
(define +G-Queue-tail+         2008)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Stack class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-Stack-is-empty+     3001)
(define +M-Stack-push+         3002)
(define +M-Stack-push-all+     3003)
(define +M-Stack-pop+          3004)
(define +M-Stack-peek+         3005)
(define +M-Stack-elements+     3006)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for HashTable class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-HashTable-put+          4001)
(define +M-HashTable-get+          4002)
(define +M-HashTable-contains-key+ 4003)
(define +M-HashTable-merge+        4004)
(define +M-HashTable-elements+     4005)
(define +M-HashTable-clear+        4006)
(define +M-HashTable-table+        4007)
(define +M-HashTable-equal+        4008)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Mapping class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +G-Mapping-type+                   5001)
(define +G-Mapping-source-statement+       5002)
(define +G-Mapping-target-statement+       5003)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Graph class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-Graph-add-vertex+           6001)
(define +M-Graph-add-edge+             6002)
(define +M-Graph-find-vertex+          6003)
(define +M-Graph-vertices+             6004)
(define +M-Graph-topological-sort+     6005)
(define +M-Graph-dag+                  6006)
(define +M-Graph-path+                 6007)
(define +M-Graph-update-vertex+        6008)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Trie class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +G-TrieNode-list+        7001)
(define +M-TrieNode-append+      7002)
(define +M-TrieNode-get-next+    7003)
(define +M-TrieNode-set-next+    7004)
(define +M-Trie-add+             7005)
(define +M-Trie-find+            7006)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Statement class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-TransitTable-put+         8001)
(define +M-TransitTable-get+         8002)
(define +M-Statement-test-match+     8003)
(define +M-Statement-extract+        8004)
(define +M-Statement-to-dfa+         8005)
(define +M-Statement-times+          8006)
(define +M-Statement-trie-prefix+    8007)
(define +M-Statement-meta-context+   8008)
(define +G-Statement-symbol+         8009)
(define +G-Statement-type+           8010)
(define +G-Statement-children+       8011)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for LcrsTree class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +G-TreeNode-key+               9001)
(define +G-TreeNode-value+             9002)
(define +G-TreeNode-parent+            9003)
(define +G-TreeNode-left-child+        9004)
(define +G-TreeNode-right-sibling+     9005)
(define +G-TreeNode-level+             9006)
(define +S-TreeNode-key+               9007)
(define +S-TreeNode-value+             9008)
(define +S-TreeNode-parent+            9009)
(define +S-TreeNode-left-child+        9010)
(define +S-TreeNode-right-sibling+     9011)
(define +S-TreeNode-level+             9012)
(define +M-Tree-travel+                9013)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for Component class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-Component-group+           10001)
(define +M-Component-artifact+        10002)
(define +M-Component-version+         10003)
(define +M-Component-dag+             10004)
(define +M-Component-valid-links+     10005)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for LexRule class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +G-LexRule-name+              11001)
(define +G-LexRule-regex+             11002)
(define +G-LexRule-target+            11003)
(define +G-LexRule-child-rule-names+  11004)
(define +G-LexRule-is-rule+           11005)
(define +G-LexRule-is-group+          11006)
(define +G-LexRule-is-terminal+       11007)
(define +G-LexRule-terminate+         11009)
(define +G-LexRule-register+          11010)
(define +M-LexRule-children+          11011)
(define +M-LexRule-merge+             11012)
(define +M-LexRule-upgrade+           11013)
(define +M-LexRule-merge-one+         11014)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for ComputeDomain class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-ComputeDomain-type+        12001)
(define +M-ComputeDomain-path+        12002)
(define +M-ComputeDomain-context+     12003)
(define +M-ComputeDomain-type-match+  12004)
(define +M-ComputeDomain-path-match+  12005)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for SortedSet class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-SortedSet-elements+        13001)
(define +M-SortedSet-vector+          13002)
(define +M-SortedSet-add+             13003)
(define +M-SortedSet-remove+          13004)
(define +M-SortedSet-remove-at+       13005)
(define +M-SortedSet-equal+           13006)
(define +M-SortedSet-add-all+         13007)
(define +M-SortedSet-union+           13008)
(define +M-SortedSet-contains+        13009)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Messages for SortedSet class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +M-StatementTrie-add+         14001)
(define +M-StatementTrie-find+        14002)



