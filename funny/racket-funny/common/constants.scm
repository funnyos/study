#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Global constants definition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide (all-defined-out))

(define +nil+ '())
(define +true+  #t)
(define +false+ #f)
(define +indentity+ (lambda (t) t))

(define +paren-left+     "(")
(define +paren-right+    ")")
(define +bracket-left+   "[")
(define +bracket-right+  "]")
(define +brace-left+     "{")
(define +brace-right+    "}")
(define +escape-string+  "\\")
(define +comment-string+ ";")
(define +space-string+   " ")

(define +paren-left-char+     #\()
(define +paren-right-char+    #\))
(define +bracket-left-char+   #\[)
(define +bracket-right-char+  #\])
(define +brace-left-char+     #\{)
(define +brace-right-char+    #\})
(define +escape-char+         #\\)
(define +comment-char+        #\;)
(define +space-char+          #\space)
(define +newline-char+        #\newline)
(define +delimiter-char+      #\,)
(define +double-quote-char+   #\")

(define +nil-macro+            "NIL")
(define +meta-scripts-prefix+  "system-macro:")
(define +funny-home+           "FUNNY_HOME")
(define +funny-scheme+         "FUNNY_SCHEME")
(define +config-path+          "/config/")
(define +system-macro-file+    "system-macro.fsdl")

(define +project-file-suffix+  ".fpdl")

(define +language-lib-path+    "lang")
(define +component-file+       "component.fn")
(define +dependencies-file+    "dependencies.fn")


(define +project-info-file-name+    "project.info")
(define +component-info-file-name+  "component.info")

(define +project-info-section-project-info+   "[project-info]")
(define +project-info-section-configuration+  "[configuration]")
(define +project-info-section-plugins+        "[plugins]")
(define +project-info-section-dependencies+   "[dependencies]")
(define +project-info-section-build-action+   "[:build-action]")

(define +component-info-section-component-info+   "[component-info]")
(define +component-info-section-configuration+    "[configuration]")
(define +component-info-section-plugins+          "[plugins]")

(define +key-group+               "group")
(define +key-name+                "name")
(define +key-version+             "version")
(define +key-entrance+            "entrance")
(define +key-source-location+     "source-location")
(define +key-build-location+      "build-location")
(define +key-plugin-location+     "plugin-location")
;(define +key-:match+              ":match")
;(define +key-:clean+              ":clean")
;(define +key-:build+              ":build")

(define +plugin-collector+        "plugin-collector")
(define +plugin-name-meta-parser+ ":parse-meta")









