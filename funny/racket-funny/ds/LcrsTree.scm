#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Tree class: left child right sibling tree
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")

(provide (all-defined-out))

; MAKE-TreeNode
(define (MAKE-TreeNode self key value parent left-child right-sibling level)
  (define (recur-travel-tree t f)
    (if (null? t) t
        (begin (f t)
               (recur-travel-tree (tree-node-lc t) f)
               (recur-travel-tree (tree-node-rs t) f))))
  (lambda (message)
    (cond
      ((= message +G-TreeNode-key+)
       (lambda () key))
      ((= message +G-TreeNode-value+)
       (lambda () value))
      ((= message +G-TreeNode-parent+)
       (lambda () parent))
      ((= message +G-TreeNode-left-child+)
       (lambda () left-child))
      ((= message +G-TreeNode-right-sibling+)
       (lambda () right-sibling))
      ((= message +S-TreeNode-key+)
       (lambda (NEW-key) (set! key NEW-key)))
      ((= message +S-TreeNode-value+)
       (lambda (NEW-value) (set! value NEW-value)))
      ((= message +S-TreeNode-parent+)
       (lambda (NEW-parent) (set! parent NEW-parent)))
      ((= message +S-TreeNode-left-child+)
       (lambda (NEW-left-child) (set! left-child NEW-left-child)))
      ((= message +S-TreeNode-right-sibling+)
       (lambda (NEW-right-sibling) (set! right-sibling NEW-right-sibling)))
      ((= message +M-Tree-travel+)
       (lambda (f)
         (recur-travel-tree self f)))
      (else +nil+))))

(define (CREATE-TreeNode k v p lc rs level)
  (create-instance MAKE-TreeNode k v p lc rs level))

(define (tree-node-key node)
  (ask node +G-TreeNode-key+))
(define (tree-node-value node)
  (ask node +G-TreeNode-value+))
(define (tree-node-parent node)
  (ask node +G-TreeNode-parent+))
(define (tree-node-lc node)
  (ask node +G-TreeNode-left-child+))
(define (tree-node-rs node)
  (ask node +G-TreeNode-right-sibling+))

(define (tree-node-key! node key)
  (ask node +S-TreeNode-key+ key))
(define (tree-node-value! node value)
  (ask node +S-TreeNode-value+ value))
(define (tree-node-lc! node lc)
  (ask node +S-TreeNode-left-child+ lc))
(define (tree-node-rs! node rs)
  (ask node +S-TreeNode-right-sibling+ rs))
(define (tree-node-parent! node parent)
  (ask node +S-TreeNode-parent+ parent))

(define (tree-travel t f)
  (ask t +M-Tree-travel+ f))

; e.g. (tree-node-add! root '(("1: abc" . "L1") ("2: 123" . "L2") ("3: tyu" . "L3")))
; todo add level
(define (tree-node-add! root-node node-chain)
  
  (define (find-slot-in-children children key)
    (if (null? children) '()
        (let ((slot (car children)))
          (if (equal? (tree-node-key slot) key)
              slot
              (find-slot-in-children (cdr children) key)))))
  
  (define (get-children sibling-node)
    (if (null? sibling-node) '()
        (cons sibling-node (get-children (tree-node-rs sibling-node)))))
  
  (define (find-slot-to-add ancestor-node descendants)
    (if (null? descendants)
        (cons ancestor-node descendants)
        (let* ((key (caar descendants))
               (lc (tree-node-lc ancestor-node))
               (siblings (get-children lc))
               (slot (find-slot-in-children siblings key)))
          (if (null? slot)
              (cons ancestor-node descendants)
              (find-slot-to-add slot (cdr descendants))))))
  
  (define (find-right-sibling node)
    (if (null? (tree-node-rs node))
        node
        (find-right-sibling (tree-node-rs node))))
  
  (define (add-descendants ancestor-node descendants)
    (if (null? descendants) '()
        (let* ((key (caar descendants))
               (value (cdar descendants))
               (node (CREATE-TreeNode key value ancestor-node '() '() 0)))
          (if (null? (tree-node-lc ancestor-node))
              (tree-node-lc! ancestor-node node)
              (tree-node-rs! (find-right-sibling (tree-node-lc ancestor-node)) node))
          (add-descendants node (cdr descendants)))))
  
  (let ((slot (find-slot-to-add root-node node-chain)))
    ;(displayln (cadr slot))
    (add-descendants (car slot) (cdr slot))))


