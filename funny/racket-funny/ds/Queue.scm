#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Queue class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")

(provide (all-defined-out))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make Queue class
;   -head: the head of queue
;   -tail: the tail of queue
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-Queue self head tail)
  (define is-empty? (lambda () (null? head)))
  (lambda (message)
    (cond
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Determine whether queue is empty
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Queue-is-empty+) is-empty?)

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Add data into tail of a queue
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Queue-enqueue+)
       (lambda (data)
         (if (is-empty?)
             (begin
               (set! head (mcons data +nil+)) 
               (set! tail head)) 
             (begin
               (set-mcdr! tail (mcons data +nil+)) 
               (set! tail (mcdr tail))))))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Insert data into head of a queue
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Queue-insert-head+)
       (lambda (data)
         (if (is-empty?)
             (begin
               (set! head (mcons data +nil+)) 
               (set! tail head)) 
             (set! head (mcons data head)))))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Remove data from a queue
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Queue-dequeue+)
       (lambda ()
         (if (is-empty?) 
             +nil+
             (let ((data (mcar head))) 
               (set! head (mcdr head))
               (if (null? head) (set! tail +nil+) +nil+)
               data))))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Append a list to a queue
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Queue-append+)
       (lambda (lst)
         (map (lambda (data) (ask self +M-Queue-enqueue+ data))
              lst)))
      ((= message +M-Queue-elements+)
       (lambda () (mlist->list head)))
      ((= message +G-Queue-head+);(mlist->list lst)
       (lambda () (mcar head)))
      ((= message +G-Queue-tail+)
       (lambda () (mcar tail)))
      (else +nil+))))

(define (CREATE-EmptyQueue) (create-instance MAKE-Queue +nil+ +nil+))
(define (CREATE-Queue lst)
  (let ((q (CREATE-EmptyQueue)))
    (ask q +M-Queue-append+ lst)
    q))

(define (queue-is-empty? q) (ask q +M-Queue-is-empty+))
(define (enqueue q data) (ask q +M-Queue-enqueue+ data))
(define (queue-insert-head q data) (ask q +M-Queue-insert-head+ data))
(define (dequeue q) (ask q +M-Queue-dequeue+))
(define (queue-append q lst) (ask q +M-Queue-append+ lst))
(define (queue-elements q) (ask q +M-Queue-elements+))
(define (queue-head q) (ask q +G-Queue-head+))
(define (queue-tail q) (ask q +G-Queue-tail+))


;(define (qlist q) (ask q +G-Queue-head+))
;(define (qinit lst)
;  (let ((q (CREATE-Queue)))
;    (ask q +M-Queue-append+ lst)
;    q))

