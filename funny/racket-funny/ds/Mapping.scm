#lang racket

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")
(require "Queue.scm")
(require "Trie.scm")
(require "Statement.scm")

(provide (all-defined-out))



#|
(define +system-macro-def-macro+ "defmacro")

(define +M-SystemMacro-eval+       (msg-gen))
(define +S-SystemMacro-name+       (msg-gen))
(define +S-SystemMacro-function+   (msg-gen))
(define +S-SystemMacro-mapping+    (msg-gen))
(define +S-SystemMacro-run+        (msg-gen))
(define +S-SystemMacro-eval+       (msg-gen))

(define (MAKE-SystemMacro self name function mapping-node run-node eval-node)
  (define (valid-macro? string) (not (eq? string +nil-macro+)))
  (define (parse-macro node)
    '())
  
  (lambda (message)
    (cond
      ((= message +M-SystemMacro-eval+)
       (lambda (replacements)
         (cond ((valid-macro? mapping-node)
                (let* ((m +nil+)  ;split-mapping mapping-node
                       (source (car m))
                       (target (cdr m))
                       (source-statement +nil+) ;(merge-statement source replacements)
                       (target-statement +nil+)) ;(merge-statement target replacements)
                  +nil+))
               ((valid-macro? run-node) (+ 1 2))
               ((valid-macro? eval-node) (+ 1 2))
               (else +nil+))))

      ((= message +S-SystemMacro-name+)
       (lambda (NEW-name)
         (set! name NEW-name)))
      ((= message +S-SystemMacro-function+)
       (lambda (NEW-function)
         (set! function NEW-function)))
      ((= message +S-SystemMacro-mapping+)
       (lambda (NEW-mapping-node)
         (set! mapping-node NEW-mapping-node)))
      ((= message +S-SystemMacro-run+)
       (lambda (NEW-run-node)
         (set! run-node NEW-run-node)))
      ((= message +S-SystemMacro-eval+)
       (lambda (NEW-eval-node)
         (set! eval-node NEW-eval-node)))
      (else (+ 1 2)))))

(define (CREATE-SystemMacro)
  (create-instance MAKE-SystemMacro +nil+ +nil+ +nil+ +nil+ +nil+))

(define (system-macro-name-set! m name)
  (ask m +S-SystemMacro-name+ name))
(define (system-macro-function-set! m function)
  (ask m +S-SystemMacro-function+ function))
(define (system-macro-mapping-set! m mapping-node)
  (ask m +S-SystemMacro-mapping+ mapping-node))
(define (system-macro-run-set! m run-node)
  (ask m +S-SystemMacro-run+ run-node))
(define (system-macro-eval-set! m eval-node)
  (ask m +S-SystemMacro-eval+ eval-node))

(define (system-macro-eval m replacements)
  (ask m +M-SystemMacro-eval+ replacements))


(define +G-Mapping-source-statement+       (msg-gen))
(define +S-Mapping-source-statement+       (msg-gen))
(define +S-Mapping-SystemMacro-name+       (msg-gen))
(define +S-Mapping-SystemMacro-function+   (msg-gen))
(define +S-Mapping-SystemMacro-mapping+    (msg-gen))
(define +S-Mapping-SystemMacro-run+        (msg-gen))
(define +S-Mapping-SystemMacro-eval+       (msg-gen))

(define (MAKE-Mapping self type source-statement target-statement starter)
  (lambda (message)
    (cond
      ((= message +G-Mapping-source-statement+)
       (lambda () source-statement))
      ((= message +S-Mapping-source-statement+)
       (lambda (text p)
         (begin (set! expandable #f)
                (set! source-statement (car (p text)))
                (set! starter (statement-to-dfa source-statement)))))
      (else +nil+))))

(define (CREATE-Mapping system-macro expandable source-statement target-statement starter)
  (create-instance MAKE-Mapping system-macro expandable source-statement target-statement starter))

(define (CREATE-System-Mapping)
  (create-instance MAKE-Mapping (CREATE-SystemMacro) +false+ +nil+ +nil+ +nil+))

(define (mapping-source-statement-set! m source-statement)
  (ask m +S-Mapping-source-statement+ source-statement parse-statements))

(define (mapping-system-macro-name-set! m name)
  (ask m +S-Mapping-SystemMacro-name+ name))
(define (mapping-system-macro-function-set! m funciton)
  (ask m +S-Mapping-SystemMacro-name+ funciton))
(define (mapping-system-macro-mapping-set! m mapping-node)
  (ask m +S-Mapping-SystemMacro-mapping+ mapping-node))
(define (mapping-system-macro-run-set! m run-node)
  (ask m +S-Mapping-SystemMacro-run+ run-node))
(define (mapping-system-macro-eval-set! m eval-node)
  (ask m +S-Mapping-SystemMacro-eval+ eval-node))

|#

(define +E-MappingType-system+       1)
(define +E-MappingType-expandable+   2)
(define +E-MappingType-normal+       3)

(define (MAKE-Mapping self type source-statement target-statement starter)
  (lambda (message)
    (cond
      ((= message +G-Mapping-type+)
       (lambda () type))
      ((= message +G-Mapping-source-statement+)
       (lambda () source-statement))
      ((= message +G-Mapping-target-statement+)
       (lambda () target-statement))
      (else +nil+))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: create system mapping
; Input:
;   statement-to-match     -> "{define {var} as {val}}"
;   system-macro-statement -> 
; Output: system mapping
; Example:
;   
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (CREATE-System-Mapping statement-to-match system-macro-statement transit-table)
  (create-instance MAKE-Mapping +E-MappingType-system+ statement-to-match system-macro-statement
                   (statement-to-dfa statement-to-match transit-table)))

(define (CREATE-Mapping expandable source-statement target-statement transit-table)
  (create-instance MAKE-Mapping (if expandable +E-MappingType-expandable+ +E-MappingType-normal+)
                   source-statement target-statement
                   (statement-to-dfa source-statement transit-table)))

(define (trie-prefix-of-mapping m)
  (trie-prefix-of-statement (ask m +G-Mapping-source-statement+)))

(define (cache-system-macro trie transit-table statement)
  (let* ((arguments (statement-arguments statement))
         (statement-to-match (last arguments))
         (prefix (trie-prefix-of-statement statement-to-match))
         (mapping (CREATE-System-Mapping statement-to-match statement transit-table)))
    (trie-add trie prefix mapping)))

(define (mapping-system? m)
  (= (ask m +G-Mapping-type+) +E-MappingType-system+))

(define (mapping-expandable? m)
  (= (ask m +G-Mapping-type+) +E-MappingType-expandable+))

(define (mapping-source-get m)
  (ask m +G-Mapping-source-statement+))

(define (mapping-target-get m)
  (ask m +G-Mapping-target-statement+))



