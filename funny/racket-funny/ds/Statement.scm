#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Statement class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")
(require "Queue.scm")
(require "HashTable.scm")
(require "Trie.scm")

(provide (all-defined-out))

(define (new-state) (mcons (state-gen) +false+))
(define (new-states count)
  (if (<= count 0) '()
      (append (list (new-state)) (new-states (- count 1)))))

; TransitNode: starter input next
; MAKE-TransitTable
(define (MAKE-TransitTable self hash-table)
  (define hash-code-state (lambda (state) (mcar state)))
  (define (hashtable-value-iter v starter input)
    (let ((e (safe car v)))
      (cond
        ((null? e) '())
        ((and (equal? (mcar starter) (mcar (first e))) (match-statement input (second e))) e)
        (else (hashtable-value-iter (cdr v) starter input)))))
  (lambda (message)
    (cond
      ((= message +M-TransitTable-put+)
       (lambda (current-state node)
         (hashtable-put! hash-table (hash-code-state current-state) node)))
      ((= message +M-TransitTable-get+)
       (lambda (current-state starter input)
         (let ((p (hashtable-contains-key? hash-table (hash-code-state current-state))))
           (if (not p) +nil+
               (let ((v (hashtable-get hash-table (hash-code-state current-state))))
                 (hashtable-value-iter v starter input))))))
      (else +nil+))))

(define (transit-table-put! table state node)
  (ask table +M-TransitTable-put+ state node))

(define (transit-table-get table state starter input)
  (ask table +M-TransitTable-get+ state starter input))

(define (CREATE-TransitTable) (create-instance MAKE-TransitTable (CREATE-HashTable)))

; input table starter state input assoc-list
; output (cons state assoc-list)
(define (transit table starter state input assoc-list)
  (cond ((or (null? state) (null? input)) +nil+)
        ((statement-symbol? input)
         (cons (third (transit-table-get table state starter input)) assoc-list))
        ((statement-argument? input)
         (let ((node (transit-table-get table state starter input)))
           (cons (third node) (append assoc-list (list (cons (statement-argument-name (second node)) input))))))
        (else +nil+)))

; input starter state statement-q assoc-list
; ouput (cons state assoc-list)
(define (recur-test-match transit-table starter state statement-q assoc-list)
  (let ((next-state (transit transit-table starter state (car statement-q) assoc-list))
        (next-statement (cdr statement-q)))
    (if (null? next-state) next-state
        (if (null? next-statement) next-state
            (recur-test-match transit-table starter (car next-state) next-statement (cdr next-state))))))

; enums
(define +E-StatementType-symbol+     1)
(define +E-StatementType-argument+   2)
(define +E-StatementType-loop+       3)
(define +E-StatementType-optional+   4)

(define (recur-transition transit-table starter state statement-list state-list)
  (if (null? statement-list) state
      (let ((statement (car statement-list))
            (next-state (car state-list)))
        (transit-table-put! transit-table state (list starter statement next-state))
        (recur-transition transit-table starter next-state (cdr statement-list) (cdr state-list)))))

(define (repeat-transition transit-table starter repeat-state repeat-statement)
  (let* ((statements (queue-elements (statement-children repeat-statement)))
         (count (length statements)))
    (recur-transition transit-table starter repeat-state statements (append (new-states (- count 1)) (list repeat-state)))))

(define (step-transition transit-table starter state statement)
  (let ((next (new-state)))
    (transit-table-put! transit-table state (list starter statement next))
    next))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make Statement class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-Statement self symbol type children)
  (define (trie-length statement)
    (if (statement-symbol? statement) (string-length (statement-symbol statement)) 1))
  (define (get-trie-elements lst count r)
    (if (or (>= count +max-trie-height+) (null? lst)) r
        (append r (list (car lst)) (get-trie-elements (cdr lst) (+ count (trie-length (car lst))) r))))
  (define (substring-in-range s max-length) (substring s 0 (min max-length (string-length s))))
  (lambda (message)
    (cond
      ((= message +M-Statement-test-match+)
       (lambda (transit-table starter)
         (recur-test-match transit-table starter starter (queue-elements children) +nil+)))
      ((= message +M-Statement-meta-context+)
       (lambda (replacements)
         (let ((count (statement-argument-count self))
               (arguments (statement-arguments self)))
           (cons (string-append "(meta:" (statement-symbol (queue-head children)) " "
                                (string-join (map (lambda (e) (format "{~v}" e)) (make-enumeration 1 count)))
                                ")")
                 (make-enumeration-list (list (take arguments (- count 1)) replacements))))))
      ((= message +M-Statement-extract+)
       (lambda ()
         (if (> (statement-argument-count self) 0) '()
             (string-join
              (map statement-symbol (queue-elements children))))))
      ((= message +M-Statement-to-dfa+)
       (lambda (transit-table)
         (let* ((current (new-state)) (starter current) (lst (queue-elements children)))
           (if (null? lst) +nil+
               (begin
                 (map
                  (lambda (st)
                    (if (statement-repeatable? st)
                        (repeat-transition transit-table starter current st)
                        (set! current (step-transition transit-table starter current st))))
                  lst)
                 (set-mcdr! current +true+)))
           starter)))
      ((= message +M-Statement-times+) ;todo
       (lambda (replacements)
         '()))
      ((= message +M-Object-to-string+)
       (lambda ()
         (if (= type +E-StatementType-symbol+) symbol
             (string-append
              +brace-left+
              (string-join
               (map (lambda (st) (ask st +M-Object-to-string+))
                    (filter (lambda (x) (not (null? x))) (queue-elements children)))
               +space-string+)
              +brace-right+))))
      ((= message +M-Statement-trie-prefix+)
       (lambda ()
         (substring-in-range
          (string-append*
           (map (lambda (st) (if (null? st) "" (if (statement-symbol? st) (statement-symbol st) +trie-base-string+)))
                (get-trie-elements (queue-elements children) 0 +nil+)))
          +max-trie-height+)))
      ((= message +G-Statement-symbol+)
       (lambda () symbol))
      ((= message +G-Statement-type+)
       (lambda () type))
      ((= message +G-Statement-children+)
       (lambda () children))
      ; todo
      (else +nil+))))

(define (statement-symbol? s) (= (ask s +G-Statement-type+) +E-StatementType-symbol+))
(define (statement-argument? s) (= (ask s +G-Statement-type+) +E-StatementType-argument+))
(define (statement-repeatable? s)
  (let ((type (ask s +G-Statement-type+)))
    (or (eq? type +E-StatementType-loop+) (eq? type +E-StatementType-optional+))))

; match-statement
(define (match-statement st1 st2)
  (let ((s1 (ask st1 +G-Statement-symbol+)) (s2 (ask st2 +G-Statement-symbol+)))
    (cond
      ((and (statement-symbol? st1) (statement-symbol? st2) (equal? s1 s2)) +true+)
      ((and (statement-argument? st1) (statement-argument? st2)) +true+)
      (else +false+))))

(define (trie-prefix-of-statement s)
  (ask s +M-Statement-trie-prefix+))

(define (CREATE-SymbolStatement symbol)
  (create-instance MAKE-Statement symbol +E-StatementType-symbol+ +nil+))

(define (CREATE-OneStatement children)
  (let* ((q (CREATE-Queue children))
         (qhead (queue-head q))
         (qtail (queue-tail q)))
    (if (statement-symbol-equals? qhead "comment")
        +nil+
        (cond
          ((statement-symbol-equals? qtail "..")
           (create-instance MAKE-Statement +nil+ +E-StatementType-optional+ q))
          ((statement-symbol-equals? qtail "...")
           (create-instance MAKE-Statement +nil+ +E-StatementType-loop+ q))
          (else
           (create-instance MAKE-Statement +nil+ +E-StatementType-argument+ q))))))

(define (statement-extract s)
  (ask s +M-Statement-extract+))

(define (get-mappings trie s)
  (trie-find trie (trie-prefix-of-statement s)))

(define (statement-to-dfa s transit-table)
  (ask s +M-Statement-to-dfa+ transit-table))

(define (statement-children s)
  (ask s +G-Statement-children+))

(define (statement-type s)
  (ask s +G-Statement-type+))

(define (statement-meta-context s replacements)
  (ask s +M-Statement-meta-context+ replacements))

(define (statement-arguments s)
  (filter statement-argument? (queue-elements (ask s +G-Statement-children+))))

(define (statement-first s)
  (first (queue-elements (statement-children s))))

(define (statement-second s)
  (second (queue-elements (statement-children s))))

(define (statement-third s)
  (third (queue-elements (statement-children s))))

(define (statement-tail s)
  (queue-tail (statement-children s)))

(define (statement-to-string s)
  (ask s +M-Object-to-string+))

(define (statement-symbol-equals? st symbol)
  (if (null? st) #f
      (equal? (ask st +G-Statement-symbol+) symbol)))

(define (statement-argument-name s)
  (statement-extract s))

(define (statement-symbol-count s)
  (length (filter statement-symbol? (queue-elements (ask s +G-Statement-children+)))))

(define (statement-argument-count s)
  (length (filter statement-argument? (queue-elements (ask s +G-Statement-children+)))))

(define (statement-test-match s transit-table starter)
  (ask s +M-Statement-test-match+ transit-table starter))

(define (statement-symbol s)
  (ask s +G-Statement-symbol+))

(define (statement-equals? s1 s2)
  (cond
    ((and (null? s1) (null? s2)) #t)
    ((or (null? s1) (null? s2)) #f)
    ((not (= (statement-type s1) (statement-type s2))) #f)
    ((and (statement-symbol? s1) (equal? (statement-symbol s1) (statement-symbol s2))) #t)
    (else (let ((c1 (queue-elements (statement-children s1))) (c2 (queue-elements (statement-children s2))))
            (andmap (lambda (st1 st2) (statement-equals? st1 st2)) c1 c2)))))

; e.g. {code module classes {funny-example} {Duck}}
(define (statement-to-compute-domains s leaf-info)
  (append
   ;(drop-right (map (lambda (t) (cons (statement-symbol t) (statement-symbol? t))) (queue-elements (statement-children s))) 1)
   (map (lambda (t)
          (if (statement-symbol? t)
              (cons (statement-symbol t) #t)
              (cons (statement-argument-name t) #f)))
        (queue-elements (statement-children s)))
   (list (cons 'leaf leaf-info))))


#|

(define t (CREATE-TransitTable))

(define (CREATE-OneArgument symbol)
  (CREATE-OneStatement (list (CREATE-SymbolStatement symbol))))

; statement: {a b {c} {d} e {f}}
(define statement1
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-SymbolStatement "b")
         (CREATE-OneArgument "c")
         (CREATE-OneArgument "d")
         (CREATE-SymbolStatement "e")
         (CREATE-OneArgument "f"))))

; statement: {a b {{1} + {2}} {3} e {4}}
(define statement2
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-SymbolStatement "b")
         (CREATE-OneStatement
          (list (CREATE-OneArgument "1")
                (CREATE-SymbolStatement "+")
                (CREATE-OneArgument "2")))
         (CREATE-OneArgument "3")
         (CREATE-SymbolStatement "e")
         (CREATE-OneArgument "4"))))

; statement: {a {b {c} ..} d}
(define statement3
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-OneStatement
          (list (CREATE-SymbolStatement "b")
                (CREATE-OneArgument "c")
                (CREATE-SymbolStatement "..")))
         (CREATE-SymbolStatement "d"))))

; statement: {a {b {c} ...} d}
(define statement4
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "a")
         (CREATE-OneStatement
          (list (CREATE-SymbolStatement "b")
                (CREATE-OneArgument "c")
                (CREATE-SymbolStatement "...")))
         (CREATE-SymbolStatement "d"))))

(statement-to-string statement1)
(statement-to-string statement2)
(statement-to-string statement3)
(statement-to-string statement4)

(statement-to-dfa statement1 t)
(statement-to-dfa statement2 t)
(statement-to-dfa statement3 t)
(statement-to-dfa statement4 t)

(define starter (statement-to-dfa statement1 t))

;(statement-to-dfa statement1 t)
;(statement-to-dfa statement2 t)
;(statement-to-dfa statement3 t)
;(statement-to-dfa statement4 t)

(define match-result (statement-test-match statement2 t starter))

(statement-to-string (cdr (assoc "c" (cdr match-result))))
(statement-to-string (cdr (assoc "d" (cdr match-result))))
(statement-to-string (cdr (assoc "f" (cdr match-result))))


(statement-test-match statement2 t starter)

;(define script1 "{run-script {\"(cache-lex {orders})\"} when match statement {cache lex {orders}}}")

; statement: {a {b {c} ..} d}
(define script-statement
  (CREATE-OneStatement
   (list (CREATE-SymbolStatement "run-script")
         (CREATE-OneArgument "\"(cache-lex {orders})\"")
         (CREATE-SymbolStatement "when")
         (CREATE-SymbolStatement "match")
         (CREATE-SymbolStatement "statement")
         (CREATE-OneStatement
          (list (CREATE-SymbolStatement "cache")
                (CREATE-SymbolStatement "lex")
                (CREATE-OneArgument "orders")))
         )))

(define script-replacements
  (list (list "orders" 1)))

(define sm (statement-meta-context script-statement script-replacements))

(display sm)
;(cadr (assoc 2 (cdr sm)))

(statement-to-string (cadr (assoc 1 (cdr sm))))


(statement-extract (CREATE-OneArgument "abc"))

(statement-extract (CREATE-OneStatement
                    (list (CREATE-SymbolStatement "cache")
                          (CREATE-SymbolStatement "lex")
                          (CREATE-OneArgument "orders"))))


(trie-prefix-of-statement statement1)
(trie-prefix-of-statement statement2)
(trie-prefix-of-statement statement3)
(trie-prefix-of-statement statement4)

(statement-equals? statement1 (CREATE-OneStatement
                    (list (CREATE-SymbolStatement "a")
                          (CREATE-SymbolStatement "b")
                          (CREATE-OneArgument "c")
                          (CREATE-OneArgument "d")
                          (CREATE-SymbolStatement "e")
                          (CREATE-OneArgument "f"))))


|#
