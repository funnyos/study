#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Graph class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require graph)

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")

(provide (all-defined-out))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make Graph class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-Graph self g directed)
  (define (find-vertex id)
    (safe car (filter (lambda (v) (equal? id (safe car v))) (get-vertices g))))
  (lambda (message)
    (cond
      ((= message +M-Graph-add-vertex+)
       (lambda (v)
         (add-vertex! g v)))
      ((= message +M-Graph-add-edge+)
       (lambda (src dest)
         (if directed
             (add-directed-edge! g (find-vertex src) (find-vertex dest))
             (add-edge! g (find-vertex src) (find-vertex dest)))))
      ((= message +M-Graph-update-vertex+)
       (lambda (id content)
         (define old (find-vertex id))
         (define new (cons (car old) content))
         (rename-vertex! g old new)))
      ((= message +M-Graph-find-vertex+) find-vertex)
      ((= message +M-Graph-vertices+)
       (lambda ()
         (get-vertices g)))
      ((= message +M-Graph-topological-sort+)
       (lambda ()
         (tsort g)))
      ((= message +M-Graph-dag+)
       (lambda ()
         (dag? g)))
      ((= message +M-Graph-path+)
       (lambda (src dest)
         (fewest-vertices-path g (find-vertex src) (find-vertex dest))))
      (else +nil+))))

(define (CREATE-DAG)
  (create-instance MAKE-Graph (unweighted-graph/directed '()) #t))

(define (graph-add-vertex! g v)
  (ask g +M-Graph-add-vertex+ v))

(define (graph-add-edge! g src dest)
  (ask g +M-Graph-add-edge+ src dest))

(define (graph-find-vertex g id)
  (ask g +M-Graph-find-vertex+ id))

(define (graph-update-vertex! g id content)
  (ask g +M-Graph-update-vertex+ id content))

(define (graph-topological-sort g)
  (ask g +M-Graph-topological-sort+))

(define (graph-dag? g)
  (ask g +M-Graph-dag+))

(define (graph-vertices g)
  (ask g +M-Graph-vertices+))

(define (graph-path g src dest)
  (ask g +M-Graph-path+ src dest))

#|
(define g (unweighted-graph/directed '()))
(add-vertex! g 1)
(add-vertex! g 2)
(add-vertex! g 2)
(add-vertex! g 3)
(add-vertex! g 4)
(add-vertex! g 5)
(add-vertex! g 6)

(add-directed-edge! g 1 2)
(add-directed-edge! g 2 3)
(add-directed-edge! g 1 3)
(add-directed-edge! g 2 4)
(add-directed-edge! g 3 4)
(add-directed-edge! g 4 5)
(add-directed-edge! g 5 6)
(add-directed-edge! g 3 6)

(fewest-vertices-path g 1 5)

|#
