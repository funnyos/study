#lang racket

(require graph)

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")
(require "Queue.scm")
(require "HashTable.scm")
(require "Graph.scm")
(require "LcrsTree.scm")
(require "Statement.scm")

(provide (all-defined-out))

(define +coordinates-delimiter+ "::")

; messages
;(define +M-Component-group+           1101)
;(define +M-Component-artifact+        1102)
;(define +M-Component-version+         1103)
;(define +M-Component-dag+             1104)
;(define +M-Component-valid-links+     1105)

; name: Component name
; modules: HashTable with key module-name and value module-path
; classes: Inheritance trees, part of modules except root
; dependencies: Graph, DAG
; outer-dependencies: list of list
(define (MAKE-Component self name modules classes dependencies outer-dependencies)
  (define coordinates (string-split name +coordinates-delimiter+))
  (lambda (message)
    (cond
      ((= message +M-Component-group+)
       (lambda () (car coordinates)))
      ((= message +M-Component-artifact+)
       (lambda () (cadr coordinates)))
      ((= message +M-Component-version+)
       (lambda () (caddr coordinates)))
      
      ((= message +M-Component-dag+)
       (lambda ()
         (let ((g dependencies))
           ;(map (lambda (t) (tree-add-to-dag t g)) classes)
           (graph-dag? g))))
      ((= message +M-Component-valid-links+)
       (lambda (g)
         (every? (lambda (k) (not (equal? (graph-path g name k) #f))) outer-dependencies)))
      (else +nil+))))

;path extension classes generics inner-deps outer-deps
(define (CREATE-Component component statements)
  (let* ((path (car statements))
         (extension (cadr statements))
         (classes (caddr statements))
         (generics (cadddr statements))
         (inner-deps (cadddr (cdr statements)))
         (outer-deps (cadddr (cddr statements)))
         (modules (CREATE-HashTable))
         (dependencies (CREATE-DAG))
         (name '()))
    (let-values (((dir file b) (split-path path)))
      (set! name file))
    (map (lambda (p)
         (let-values (((dir file b) (split-path p)))
           (hashtable-put! modules file dir)))
       (scan-files component extension))
    (map (lambda (d)
           (let* ((args (statement-arguments d))
                  (src (first args))
                  (dest (second args)))
             (graph-add-vertex! dependencies src)
             (graph-add-vertex! dependencies dest)
             (graph-add-edge! dependencies src dest))) inner-deps)
    (map (lambda (d) '()) outer-deps)
    (create-instance MAKE-Component
                     name
                     modules
                     (build-inheritance-trees classes inner-deps outer-deps)
                     dependencies
                     (build-outer-dependencies outer-deps))))

(define (CREATE-FullComponent name modules classes dependencies outer-dependencies)
  (create-instance MAKE-Component name modules classes dependencies outer-dependencies))

(define (component-dag? c)
  (ask c +M-Component-dag+))

(define (component-valid-links? c g)
  (ask c +M-Component-valid-links+ g))

(define (component-valid? comp component-graph)
  (cond
    ((not (component-dag? comp)) "a")
    ((not (component-valid-links? comp component-graph)) "b")
    (else #t)))

(define (component-module-path m)
  '())

(define (component-modules comp)
  '())

; (sub-sts (queue-elements (statement-children statements)))
(define (build-inheritance-trees classes inner-deps outer-deps)
  (let ((sub-sts (queue-elements (statement-children classes))))
    (build-inheritance-trees-recur sub-sts)))

(define (build-inheritance-tree base sub)
  (let ((t (CREATE-TreeNode))
        (base-class (statement-extract base))
        (sub-classes (queue-elements (statement-children sub))))
    (if (null? sub-classes) +nil+
        (map (lambda (c)
               (let* ((args (statement-arguments sub-classes))
                      (class (first args))
                      (class-name (statement-extract class))
                      (properties (second args))
                      (methods (third args))
                      (subs (fourth args)))
                 ;(tree-add t class-name base-class (cons properties methods))
                 (build-inheritance-tree class subs)))
             sub-classes))
    t))

(define (build-inheritance-trees-recur statements)
  (if (null? statements) +nil+
      (append (list (build-inheritance-tree (car statements) (cadr statements)))
              (build-inheritance-trees-recur (cddr statements)))))

; (tree-add t +nil+ "Object" "Object-class")

(define (build-outer-dependencies outer-deps)
  '())

;(define (scan-modules statement)
;  (map (lambda (p)
;         (let-values (((dir file b) (split-path p)))
;           (hashtable-put! *module-paths* file dir)))
;       (scan-files statement #".fn")))

(define (add-inheritance-tree base-class sub-classes)
  '())

(define (class-module statement)
  (let ((args (statement-arguments statement)))
    (if (null? (cddr args)) '()
        (add-inheritance-tree (car args) (cadr args)))))


(define (generic-module statement)
  '())

;(define (inner-dependencies statement)
;  (let ((args (statement-arguments statement)))
;    (map (lambda (dep)
;           (let ((dep-args (statement-arguments dep)))
;             (graph-add-edge! *module-graph* (cadr dep-args) (car dep-args))))
;         args)))
;
;(define (outer-dependencies statement)
;  (let ((args (statement-arguments statement)))
;    (map (lambda (dep)
;           (let ((dep-args (statement-arguments dep)))
;             (graph-add-edge! *module-graph* (cdr dep-args) (car dep-args)))) ;;
;         args)))

(define (hierachy statement)
  '())


#|

(define modules-A (CREATE-HashMap))
(hashtable-put! modules-A "A1" "path:A1")
(hashtable-put! modules-A "A2" "path:A2")
(hashtable-put! modules-A "A3" "path:A3")
(hashtable-put! modules-A "A4" "path:A4")
(hashtable-put! modules-A "A5" "path:A5")
(hashtable-put! modules-A "A6" "path:A6")
(hashtable-put! modules-A "A7" "path:A7")
(hashtable-put! modules-A "A8" "path:A8")
(hashtable-put! modules-A "A9" "path:A9")
(hashtable-put! modules-A "a1" "path:a1")
(hashtable-put! modules-A "a2" "path:a2")
(hashtable-put! modules-A "a3" "path:a2")

(define sub-class-A1 (CREATE-TreeNode))
(tree-add sub-class-A1 +nil+ "Object" "Object-class")
(tree-add sub-class-A1 "Object" "A1" "A1-class")
(tree-add sub-class-A1 "Object" "A2" "A2-class")
(tree-add sub-class-A1 "A2" "A3" "A3-class")
(tree-add sub-class-A1 "A2" "A4" "A4-class")
(define sub-class-A2 (CREATE-TreeNode))
(tree-add sub-class-A1 +nil+ "OtherRoot" "OtherRoot-class")
(tree-add sub-class-A1 "OtherRoot" "A5" "A5-class")
(tree-add sub-class-A1 "OtherRoot" "A6" "A6-class")
(tree-add sub-class-A1 "A5" "A7" "A7-class")
(tree-add sub-class-A1 "A5" "A8" "A8-class")
(tree-add sub-class-A1 "A8" "A9" "A9-class")

(define classes-A (list sub-class-A1 sub-class-A2))

(define dependencies-A (CREATE-DAG))
(graph-add-vertex! dependencies-A (cons "a1" "a1-module"))
(graph-add-vertex! dependencies-A (cons "a2" "a2-module"))
(graph-add-vertex! dependencies-A (cons "a3" "a3-module"))
(graph-add-vertex! dependencies-A (cons "A1" "A1-module"))
(graph-add-vertex! dependencies-A (cons "A5" "A5-module"))
(graph-add-vertex! dependencies-A (cons "A9" "A9-module"))
(graph-add-edge! dependencies-A "a1" "A1")
(graph-add-edge! dependencies-A "a2" "A5")
(graph-add-edge! dependencies-A "a3" "A9")
(graph-add-edge! dependencies-A "a1" "a2")
(graph-add-edge! dependencies-A "a2" "a3")
(graph-add-edge! dependencies-A "a1" "a3")

(define outer-dependencies-A (list "Object" "OtherRoot"))

(define component1
  (CREATE-FullComponent "A" modules-A classes-A dependencies-A outer-dependencies-A))

(define component2
  (CREATE-FullComponent "Object" (CREATE-HashMap) +nil+ (CREATE-DAG) +nil+))

(define component3
  (CREATE-FullComponent "OtherRoot" (CREATE-HashMap) +nil+ (CREATE-DAG) +nil+))

(define components (CREATE-DAG))
(graph-add-vertex! components (cons "A" component1))
(graph-add-vertex! components (cons "Object" component2))
(graph-add-vertex! components (cons "OtherRoot" component3))
(graph-add-edge! components "A" "Object")
(graph-add-edge! components "A" "OtherRoot")

(component-valid? component1 components)

|#

