#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; LexRule class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")

(provide (all-defined-out))

; enums
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Direct rule, e.g:
;   atom -> <boolean>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +E-direct-rule+      1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Plain rule, e.g:
;   complex -> <real> :: (make-complex %1% 0)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +E-plain-rule+       2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Group rule, e.g:
;   integer -> (<sign><digit>+) ::: (lex "Integer" %1%)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +E-group-rule+       3)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Terminal rule, e.g:
;   bool -> #[tf]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define +E-terminal-rule+    4)

; Normalized rule type enums
(define +E-rule+             1)
(define +E-group+            2)
(define +E-terminal+         3)

; regexes
(define +R-token+           #px"\\<[^\\>]*\\>")
(define +R-angle-brackets+  #px"\\\\<|\\\\>")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Slice text into list, and transform with id
; Example:
;   (slice "<real>\\+<ureal>i")  =>  '("" "<1>" "\\+" "<2>" "i")
;   (slice "<real>\\+<ureal>i-<real>")  =>  '("" "<1>" "\\+" "<2>" "i-" "<3>" "")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (slice text)
  (define id-gen (make-counter))
  (multimatch
   (list (cons text +nil+))
   (list +R-angle-brackets+
         +R-token+)
   identity
   (list token-escape
         (lambda (t)
           (if (equal? t "") ""
               (list->string (list #\< (integer->char (+ (id-gen) 48)) #\>)))))
   (lambda (t) #t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Replace text in brackets with token id
; Example:
;   (replace-with-token-id "<real>\\+<ureal>i")  =>  "<1>\\+<2>i"
;   (replace-with-token-id "<real>-<ureal>i+<real>")  =>  <1>-<2>i+<3>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (replace-with-token-id text)
  (string-append* (slice text)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Fetch strings from brackets into list
; Example:
;   (fetch-string-from-brackets "<real>\\+<ureal>i")  =>  '("real" "ureal")
;   (fetch-string-from-brackets "<real>-<ureal>i+<real>")  =>  '("real" "ureal" "real")
; Note:
;   (regexp-match #px"([0-9]+.[0-9]*e[0-9]+)-([0-9]*)/([0-9]*)i" "3.1e2-5/6i") => '("3.1e2-5/6i" "3.1e2" "5" "6")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (fetch-string-from-brackets text)
  (map (lambda (t) (substring t 1 (- (string-length t) 1)))
       (regexp-match* +R-token+ (regexp-replace +R-angle-brackets+ text ""))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Upgrade function for regex part of action
; Example:
;   ((upgrade-regex 3) "1") => '("<1>" . "<31>")
;   ((upgrade-regex 5) "2") => '("<2>" . "<52>")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (upgrade-regex parent-id)
  (lambda (t) (cons (format "<~a>" t) (format "<~a~a>" parent-id t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Upgrade function for target part of action
; Example:
;   ((upgrade-target 3) "1") => '("%1%" . "%31%")
;   ((upgrade-target 5) "2") => '("%2%" . "%52%")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (upgrade-target parent-id)
  (lambda (t) (cons (format "%~a%" t) (format "%~a~a%" parent-id t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Upgrade function for child rule names
; Example:
;   ((upgrade-child-rule-names 3) '(1 . something)) => '(31 . something)
;   ((upgrade-child-rule-names 5) '(2 . other)) => '(52 . other)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (upgrade-child-rule-names parent-id)
  (lambda (t) (cons (string->number (format "~a~a" parent-id (car t))) (cdr t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Link rules if it's not terminal
; Example:
;   (link-rule '((1 . ("rule-1" . #f)) (2 . ("rule-2" . #f))) '(("rule-1" . ("[term-1]" . #t)))) => '((1 "[term-1]" . #t) (2 "rule-2" . #f))
;   (link-rule '((1 . ("[term-1]" . #t)) (2 . ("rule-1" . #f))) '(("rule-1" . ("[term-2]" . #t)))) => '((1 "[term-1]" . #t) (2 "[term-2]" . #t))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (link-rule alist1 alist2)
  (map (lambda (t)
         (let* ((link-node (cdr t))
                (link-key (car link-node))
                (terminal (cdr link-node)))
           (if terminal t
               (let ((pair (assoc link-key alist2)))
                 (cons (car t) (if pair (cdr pair) link-node))))))
       alist1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make LexRule class
;   -name: e.g. "complex"
;   -type: e.g. +E-rule+
;   -regex: e.g. "<1>\\+<2>i"
;   -target: e.g. "(make-complex %1% %2%)"
;   -child-rule-names: e.g. '((1 . "real") (2 . "ureal"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-LexRule self name type regex target child-rule-names)
  (define (terminate-regex regex terminals)
    (let* ((tokens (fetch-string-from-brackets regex))
           (regex-replacements (map (lambda (t) (cons (format "<~a>" t) (car (assoc-value terminals t)))) tokens)))
      (string-replace-by-alist regex regex-replacements)))
  (define terminated? cddr)
  
  (lambda (message)
    (cond
      ((= message +G-LexRule-name+)
       (lambda () name))
      ((= message +G-LexRule-regex+)
       (lambda () regex))
      ((= message +G-LexRule-target+)
       (lambda () target))
      ((= message +G-LexRule-child-rule-names+)
       (lambda () child-rule-names))
      ((= message +G-LexRule-is-rule+)
       (lambda () (= type +E-rule+)))
      ((= message +G-LexRule-is-group+)
       (lambda () (= type +E-group+)))
      ((= message +G-LexRule-is-terminal+)
       (lambda () (= type +E-terminal+)))

      ((= message +G-LexRule-terminate+)
       (lambda (terminals)
         (cond ((= type +E-rule+)
                (let* ((linked (link-rule child-rule-names terminals))
                       (r-type (if (andmap terminated? linked) +E-terminal+ +E-rule+)))
                  (CREATE-LexRule name r-type regex target linked)))
               ((= type +E-group+)
                (let ((t-regex (cadr (first child-rule-names))))
                  (CREATE-LexRule name +E-terminal+ "<1>" target
                                  (list (cons 1 (cons (terminate-regex t-regex terminals) #t))))))
               (else self))))

      ((= message +M-LexRule-merge+)
       (lambda (children terminals)
         (let ((cartesion-rules (cartesian-product-list children)))
           (map
            (lambda (t)
              (lex-rule-merge-one self t terminals))
            cartesion-rules))))

      ((= message +M-LexRule-merge-one+)
       (lambda (rules terminals)
         (let* ((terminated-rules (map (lambda (t) (lex-rule-terminate t terminals)) rules))
                (ids (map car child-rule-names))
                (rules-upgrade (map (lambda (id rule) (lex-rule-upgrade rule id)) ids terminated-rules))
                (regex-replacements (map (lambda (id rule) (cons (format "<~a>" id) (lex-rule-regex rule))) ids rules-upgrade))
                (target-replacements (map (lambda (id rule) (cons (format "%~a%" id) (lex-rule-target rule))) ids rules-upgrade)))
           (CREATE-LexRule
            name
            (if (andmap lex-terminal? terminated-rules) +E-terminal+ +E-rule+)
            (string-replace-by-alist regex regex-replacements)
            (string-replace-by-alist target target-replacements)
            (append* (map lex-rule-child-rule-names rules-upgrade))))))

      ((= message +M-LexRule-upgrade+)
       (lambda (parent-rule-id)
         (let ((upgrade (lambda (op which) (map-chain (op parent-rule-id) which child-rule-names))))
           (CREATE-LexRule
            name
            type
            (string-replace-by-alist regex (upgrade upgrade-regex car))
            (string-replace-by-alist target (upgrade upgrade-target car))
            (upgrade upgrade-child-rule-names +indentity+)))))

      ((= message +M-Object-to-string+)
       (lambda ()
         (string-append*
          (list "name: " name ", "
                "type: " (format "~a" type) ", "
                "regex: " regex ", "
                "target: " target ", "
                "child-rule-names: " (string-append* (map (lambda (t) (format " (~a ~a) " (car t) (cdr t))) child-rule-names)) "\n"))))
      
      (else +nil+))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; e.g. LexRule Objects to refer:
;   [direct-rule] fraction-number -> fraction
;   [plain-rule] fraction -> <integer>\\/<uinteger> :: (make-fraction %1% %2%)
;   [group-rule] integer -> (<sign><digit>+) :: (lex \"Integer\" \"%1%\")
;   [group-rule] uinteger -> (<digit>+) :: (lex \"Integer\" \"%1%\")
;   [terminal] sign -> [+-]?
;   [terminal] digit -> [[:digit:]]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (lex-rule? lex-rule)
  (ask lex-rule +G-LexRule-is-rule+))

(define (lex-group? lex-rule)
  (ask lex-rule +G-LexRule-is-group+))

(define (lex-terminal? lex-rule)
  (ask lex-rule +G-LexRule-is-terminal+))

(define (lex-rule-name lex-rule)
  (ask lex-rule +G-LexRule-name+))

(define (lex-rule-regex lex-rule)
  (ask lex-rule +G-LexRule-regex+))

(define (lex-rule-target lex-rule)
  (ask lex-rule +G-LexRule-target+))

(define (lex-rule-child-rule-names lex-rule)
  (ask lex-rule +G-LexRule-child-rule-names+))

(define (lex-rule-terminate lex-rule terminals)
  (ask lex-rule +G-LexRule-terminate+ terminals))

(define (lex-rule-register lex-rule name type regex target rule-saver)
  (ask lex-rule +G-LexRule-register+ name type regex target rule-saver))

(define (lex-rule-merge lex-rule children terminals)
  (ask lex-rule +M-LexRule-merge+ children terminals))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; e.g.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (lex-rule-merge-one lex-rule rules terminals)
  (ask lex-rule +M-LexRule-merge-one+ rules terminals))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; e.g.
; (object-to-string (lex-rule-upgrade *lex-rule-fraction* 1))
; => "name: fraction, type: 2, regex: <11>\\/<12>, target: (make-fraction %11% %12%), child-rule-names:  (11 integer)  (12 uinteger) "
; (object-to-string (lex-rule-upgrade *lex-rule-fraction* 3))
; => "name: fraction, type: 2, regex: <31>\\/<32>, target: (make-fraction %31% %32%), child-rule-names:  (31 integer)  (32 uinteger) "
; (object-to-string (lex-rule-upgrade *lex-rule-fraction-number* 3))
; => "name: fraction-number, type: 1, regex: <31>, target: %31%, child-rule-names:  (31 fraction) "
; (object-to-string (lex-rule-upgrade *lex-rule-integer* 1))
; => "name: integer, type: 3, regex: <11>, target: (lex \"Integer\" \"%11%\"), child-rule-names:  (11 (<sign><digit>+)) "
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (lex-rule-upgrade lex-rule parent-rule-id)
  (ask lex-rule +M-LexRule-upgrade+ parent-rule-id))

(define (CREATE-LexRule name type regex target child-rule-names)
  (create-instance MAKE-LexRule name type regex target child-rule-names))

(define (CREATE-LexRule-Normalized name type regex target)
  (define (construct-single regex is-terminal)
    (list (cons 1 (cons regex is-terminal))))
  (define (construct-children regex)
    (make-enumeration-list (map (lambda (t) (cons t #f)) (fetch-string-from-brackets regex))))
  (cond
    ((= type +E-direct-rule+)
     (CREATE-LexRule name +E-rule+ "<1>" "%1%" (construct-children regex)))
    ((= type +E-plain-rule+)
     (CREATE-LexRule name +E-rule+ (replace-with-token-id regex) target (construct-children regex)))
    ((= type +E-group-rule+)
     (CREATE-LexRule name +E-group+ "<1>" target (construct-single regex #f)))
    ((= type +E-terminal-rule+)
     (CREATE-LexRule name +E-terminal+ "<1>" "%1%" (construct-single regex #t)))
    (else
     (error "invalid rule type"))))
