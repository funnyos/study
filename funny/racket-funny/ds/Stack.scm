#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Stack class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")

(provide (all-defined-out))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make Stack class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-Stack self stack)
  (define is-empty? (lambda () (null? stack)))
  (lambda (message)
    (cond
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Determine whether stack is empty
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Stack-is-empty+) is-empty?)

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Push data to stack
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Stack-push+)
       (lambda (data)
         (set! stack (append (list data) stack))))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Push all data to stack
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Stack-push-all+)
       (lambda (data)
         (set! stack (append (reverse data) stack))))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Pop data from stack
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Stack-pop+)
       (lambda ()
         (if (is-empty?) +nil+
             (let ((data (car stack)))
               (set! stack (cdr stack))
               data))))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Peek data from stack
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-Stack-peek+)
       (lambda ()
         (if (is-empty?) +nil+
             (car stack))))

      ((= message +M-Stack-elements+)
       (lambda ()
         stack))
      
      (else +nil+))))

(define (CREATE-Stack) (create-instance MAKE-Stack +nil+))

(define (stack-is-empty? stack)
  (ask stack +M-Stack-is-empty+))

(define (stack-push stack element)
  (ask stack +M-Stack-push+ element))

(define (stack-push-all stack elements)
  (ask stack +M-Stack-push-all+ elements))

(define (stack-pop stack)
  (ask stack +M-Stack-pop+))

(define (stack-peek stack)
  (ask stack +M-Stack-peek+))

(define (stack-elements stack)
  (ask stack +M-Stack-elements+))
