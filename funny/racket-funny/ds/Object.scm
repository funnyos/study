#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Object class: the root class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")

(provide create-instance
         ask
         CREATE-Object
         MAKE-Object
         get-method
         object-equals?
         object-hash-code
         object-to-string
         object-type)

(define +SET-HANDLER!+    0)

(define (make-instance)
  (let ((handler +nil+))
    (lambda (message)
      (cond
        ((= message +SET-HANDLER!+)
         (lambda (handler-proc)
           (set! handler handler-proc)))
        (else (get-method message handler))))))

(define (create-instance maker . args)
  (let* ((instance (make-instance))
         (handler (apply maker instance args)))
    (ask instance +SET-HANDLER!+ handler)
    instance))

(define (create-root-object) (create-instance make-root-object))

(define (make-root-object self) (lambda (message) (self message)))

(define (get-method message object) (object message))

(define (ask object message . args)
  (let ((method (get-method message object)))
    (if (not (null? method))
        (apply method args)
        (error "No method for message" message))))

(define (CREATE-Object) (create-instance MAKE-Object))

(define (MAKE-Object self)
  (let ((root-part (make-root-object self)))
    (lambda (message)
      (cond
        ((= message +M-Object-equals+) (lambda (other) (equal? (object-type other) 'Object)))
        ((= message +M-Object-hash-code+) (lambda () -1))
        ((= message +M-Object-to-string+) (lambda () +nil+))
        ((= message +M-Object-type+) (lambda () 'Object))
        (else (get-method message root-part))))))

(define (object-equals? obj other)
  (ask obj +M-Object-equals+ other))

(define (object-hash-code obj)
  (ask obj +M-Object-hash-code+))

(define (object-to-string obj)
  (ask obj +M-Object-to-string+))

(define (object-type obj)
  (ask obj +M-Object-type+))
