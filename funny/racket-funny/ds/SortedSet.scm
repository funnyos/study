#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SortedSet class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require data/gvector)
(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")

(provide (all-defined-out))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make SortedSet class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-SortedSet self gvec equal-p less-than-p)
  (define (len) (gvector-count gvec))
  (define (insert-pos v equal-p less-than-p low high)
    (define middle (quotient (+ low high) 2))
    (if (or (> low high) (= middle (len))) low
        (let ((value (gvector-ref gvec middle)))
          (cond
            ((equal-p v value) #f)
            ((less-than-p v value) (insert-pos v equal-p less-than-p low (- middle 1)))
            (else (insert-pos v equal-p less-than-p (+ middle 1) high))))))
  (define (remove-pos v equal-p less-than-p low high)
    (define middle (quotient (+ low high) 2))
    (if (> low high) #f
        (let ((value (gvector-ref gvec middle)))
          (cond
            ((equal-p v value) middle)
            ((less-than-p v value) (remove-pos v equal-p less-than-p low (- middle 1)))
            (else (remove-pos v equal-p less-than-p (+ middle 1) high))))))
  (lambda (message)
    (cond
      ((= message +M-SortedSet-elements+)
       (lambda () (gvector->list gvec)))

      ((= message +M-SortedSet-vector+)
       (lambda () gvec))

      ((= message +M-SortedSet-add+)
       (lambda (v)
         (let ((pos (insert-pos v equal-p less-than-p 0 (- (len) 1))))
           (cond
             ((equal? pos #f) '())
             ((equal? pos (len)) (gvector-add! gvec v))
             (else (gvector-insert! gvec pos v))))))

      ((= message +M-SortedSet-add-all+)
       (lambda (vs)
         (map (lambda (v) (sorted-set-add! self v)) vs)))

      ((= message +M-SortedSet-remove+)
       (lambda (v)
         (let ((pos (remove-pos v equal-p less-than-p 0 (- (len) 1))))
           (cond
             ((equal? pos #f) '())
             (else (gvector-remove! gvec pos))))))

      ((= message +M-SortedSet-remove-at+)
       (lambda (pos)
         (gvector-remove! gvec pos)))

      ((= message +M-SortedSet-equal+)
       (lambda (s)
         (equal? gvec (sorted-set-vector s))))

      ((= message +M-SortedSet-union+)
       (lambda (vs)
         (map (lambda (v) (sorted-set-add! self v)) (gvector->list (sorted-set-vector vs)))
         self))

      ((= message +M-SortedSet-contains+)
       (lambda (v)
         (let ((pos (insert-pos v equal-p less-than-p 0 (- (len) 1))))
           (if (equal? pos #f) #t #f))))

      (else +nil+))))

(define (CREATE-SortedSet equal-p less-than-p)
  (create-instance MAKE-SortedSet (make-gvector) equal-p less-than-p))

(define (sorted-set-elements s)
  (ask s +M-SortedSet-elements+))

(define (sorted-set-vector s)
  (ask s +M-SortedSet-vector+))

(define (sorted-set-add! s v)
  (ask s +M-SortedSet-add+ v))

(define (sorted-set-add-all! s vs)
  (ask s +M-SortedSet-add-all+ vs))

(define (sorted-set-remove! s v)
  (ask s +M-SortedSet-remove+ v))

(define (sorted-set-remove-at! s p)
  (ask s +M-SortedSet-remove-at+ p))

(define (sorted-set-equal? s1 s2)
  (ask s1 +M-SortedSet-equal+ s2))

(define (sorted-set-union! s vs)
  (ask s +M-SortedSet-union+ vs))

(define (sorted-set-contains? s v)
  (ask s +M-SortedSet-contains+ v))

#|

(define gvec (gvector 1 5))
(define (insert-pos v equal-p less-than-p low high)
  (define middle (quotient (+ low high) 2))
  (if (or (> low high) (= middle (gvector-count gvec))) low
      (let ((value (gvector-ref gvec middle)))
        (cond
          ((equal-p v value) #f)
          ((less-than-p v value) (insert-pos v equal-p less-than-p low (- middle 1)))
          (else (insert-pos v equal-p less-than-p (+ middle 1) high))))))

(set! gvec (gvector))
(insert-pos 4 = < 0 -1) ; 0

(set! gvec (gvector 5))
(insert-pos 4 = < 0 0) ; 0
(insert-pos 5 = < 0 0) ; #f
(insert-pos 6 = < 0 0) ; 1

(set! gvec (gvector 2 6))
(insert-pos 1 = < 0 1) ; 0
(insert-pos 3 = < 0 1) ; 1
(insert-pos 7 = < 0 1) ; 2
(insert-pos 2 = < 0 1) ; #f

(set! gvec (gvector 2 6 9))
(insert-pos 1 = < 0 2) ; 0
(insert-pos 3 = < 0 2) ; 1
(insert-pos 7 = < 0 2) ; 2
(insert-pos 10 = < 0 2) ; 3
(insert-pos 2 = < 0 2) ; #f

|#

