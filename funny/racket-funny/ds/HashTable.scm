#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; HashTable class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")

(provide (all-defined-out))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make HashTable class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-HashTable self table replace-mode)
  (define (hashtable-put-all! table1 table2)
    (map (lambda (kv) (hashtable-put! table1 (car kv) (cadr kv))) (hashtable-elements table2)))
  (lambda (message)
    (cond
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Get value from a hashtable
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-get+)
       (lambda (key) (hash-ref table key)))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Put value to a hashtable
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-put+)
       (lambda (key value)
         (if replace-mode
             (hash-set! table key (cons value +nil+))
             (let ((v (if (hash-has-key? table key) (hash-ref table key) +nil+)))
               (if (member value v) v
                   (hash-set! table key (cons value v)))))))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Determine whether hashtable contains key
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-contains-key+)
       (lambda (key) (hash-has-key? table key)))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Hashtable elements
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-elements+)
       (lambda () (hash->list table)))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Hashtable merge
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-merge+)
       (lambda (table-list)
         (map
          (lambda (t)
            (hashtable-put-all! self t))
          table-list)
         self))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Hashtable clear
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-clear+)
       (lambda ()
         (hash-clear! table)))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Hashtable table
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-table+)
       (lambda ()
         table))

      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ; Hashtable equal
      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ((= message +M-HashTable-equal+)
       (lambda (ht vequal-p)
         (let ((table2 (ask ht +M-HashTable-table+)))
           (cond
             ((not (= (hash-count table) (hash-count table2))) #f)
             ((not (andmap (lambda (t) (vequal-p (car (hash-ref table t)) (car (hash-ref table2 t)))) (hash-keys table))) #f)
             (else #t)))))

      ((= message 11111)
       (lambda ()
         (hash-map table (lambda (k v) (display k) (display " -> ") (display v)))))

      ((= message 22222)
       (lambda (table2)
         (andmap (lambda (t) (equal? (hashmap-get table t) (hashmap-get table2 t))) (hash-keys table))
         ; (equal? table (ask table2 33333))
         ))

      ((= message 33333)
       (lambda ()
         table))

      ((= message 44444)
       (lambda ()
         (create-instance MAKE-HashTable (hash-copy table) #t)))

      ((= message 55555)
       (lambda (rv f)
         (hash-map table (lambda (k v) (f (hashmap-get self k) rv)))))
      
      (else +nil+))))

(define (CREATE-HashTable)
  (create-instance MAKE-HashTable (make-hash) #f))

(define (CREATE-HashMap)
  (create-instance MAKE-HashTable (make-hash) #t))

(define (CREATE-HashMap-By-List kvlist)
  (let ((hm CREATE-HashMap))
    (map (lambda (t) (hashmap-put! hm (car t) (cdr t))) kvlist)
    hm))

(define (hashtable-put! ht key value)
  (ask ht +M-HashTable-put+ key value))

(define (hashtable-get ht key)
  (ask ht +M-HashTable-get+ key))

(define (hashtable-contains-key? ht key)
  (ask ht +M-HashTable-contains-key+ key))

(define (hashtable-elements ht)
  (ask ht +M-HashTable-elements+))

(define (hashtable-merge ht table-list)
  (ask ht +M-HashTable-merge+ table-list))

(define (hashtable-clear ht)
  (ask ht +M-HashTable-clear+))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Description: usage of hashmap
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (hashmap-grow! ht key value)
  (let ((m (if (null? ht) (CREATE-HashMap) ht)))
    (hashmap-put! m key value)
    m))

(define (hashmap-put! ht key value)
  (hashtable-put! ht key value))

(define (hashmap-get ht key)
  (car (hashtable-get ht key)))

(define (hashmap-contains-key? ht key)
  (hashtable-contains-key? ht key))

(define (hashmap-elements ht)
  (hashtable-elements ht))

(define (hashmap-merge ht table-list)
  (hashtable-merge ht table-list))

(define (hashmap-clear ht)
  (hashtable-clear ht))

(define (hashmap-string ht)
  (display "hashmap-string:")
  (ask ht 11111))

(define (hashmap-table ht)
  (ask ht 33333))

(define (hashmap-copy ht)
  (ask ht 44444))

(define (equalset? s1 s2)
;  (displayln s1)
;  (displayln s2)
;  (displayln (null? (set-symmetric-difference s1 s2)))
;  (displayln "==========================")
  (null? (set-symmetric-difference s1 s2)))

;(define (hashmap-equal? ht1 ht2)
;  (let* ((table1 (hashmap-table ht1))
;         (table2 (hashmap-table ht2))
;         (keyset1 (hash-keys table1))
;         (keyset2 (hash-keys table2)))
;    (cond
;      ((not (equalset? keyset1 keyset2)) #f)
;      ((not (andmap (lambda (t) (equalset? (hash-ref table1 t) (hash-ref table2 t))) keyset1)) #f)
;      (else #t))))

(define (hashmap-equal? ht1 ht2 vequal-p)
  (ask ht1 +M-HashTable-equal+ ht2 vequal-p))

(define (hashmap-remove-in-value ht value f)
  (ask ht 55555 value f))






