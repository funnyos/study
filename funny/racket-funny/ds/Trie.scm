#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Trie class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "Object.scm")
(require "Queue.scm")
(require "HashTable.scm")

(provide (all-defined-out))

(define +trie-base-char+   #\`)
(define +trie-base-string+ "`")
(define +trie-node-size+   27)
(define +max-trie-height+  3)

(define (MAKE-TrieNode self lst next)
  (lambda (message)
    (cond
      ((= message +G-TrieNode-list+)
       (lambda () lst))
      ((= message +M-TrieNode-append+)
       (lambda (data) (set! lst (cons data lst))))
      ((= message +M-TrieNode-get-next+)
       (lambda (index) (vector-ref next index)))
      ((= message +M-TrieNode-set-next+)
       (lambda (index) (vector-set! next index (CREATE-TrieNode))))
      (else +nil+))))

(define (CREATE-TrieNode)
  (create-instance MAKE-TrieNode '() (make-vector +trie-node-size+)))

(define (safe-next-node node index)
  (if (equal? node 0) 0
      (ask node +M-TrieNode-get-next+ index)))

(define (next node c)
  (let* ((index (- (char->integer c) (char->integer +trie-base-char+)))
         (next-node (safe-next-node node index)))
    (if (equal? next-node 0) (ask node +M-TrieNode-set-next+ index)
        +nil+)
    (safe-next-node node index)))

(define (insert-data node data char-list c)
  (let ((next-node (next node c)))
    (if (null? char-list)
      (ask next-node +M-TrieNode-append+ data)
      (insert-data next-node data (cdr char-list) (car char-list)))))

(define (find-data node char-list c)
  (let* ((index (- (char->integer c) (char->integer +trie-base-char+)))
         (next-node (safe-next-node node index)))
    (if (not (null? char-list))
        (find-data next-node (cdr char-list) (car char-list))
        (if (equal? next-node 0) +nil+
            (ask next-node +G-TrieNode-list+)))))

(define (prefix-out-of-range prefix)
  (not (null? (filter (lambda (c) (or (char<? c +trie-base-char+) (char>? c #\z))) (string->list prefix)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Make Trie class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (MAKE-Trie self root hash-table)
  (lambda (message)
    (cond
      ((= message +M-Trie-add+)
       (lambda (data prefix)
         (if (null? data) +nil+
             (let ((lst (string->list prefix)))
               (if (prefix-out-of-range prefix)
                   (hashtable-put! hash-table prefix data)
                   (insert-data root data (cdr lst) (car lst)))))))
      ((= message +M-Trie-find+)
       (lambda (prefix)
         (let ((lst (string->list prefix)))
           (if (prefix-out-of-range prefix)
               (if (hashtable-contains-key? hash-table prefix)
                   (hashtable-get hash-table prefix)
                   +nil+)
               (find-data root (cdr lst) (car lst))))))
      (else +nil+))))

(define (CREATE-Trie)
  (create-instance MAKE-Trie (CREATE-TrieNode) (CREATE-HashTable)))

(define (trie-add trie prefix data)
  (ask trie +M-Trie-add+ data prefix))

(define (trie-find trie prefix)
  (ask trie +M-Trie-find+ prefix))
