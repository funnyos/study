#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Funny launcher
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require racket/cmdline)
(require racket/file)

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/HashTable.scm")
(require "global.scm")
(require "parser.scm")
(require "system.scm")
(require "engine.scm")

(provide funny-ns)

(define funny-ns (make-base-namespace))

(define (run-funny)
  ; process arguments and register to context
  ;  (define (arguments-key start end)
  ;    (if (> start end) '()
  ;        (if (= start end) (list start)
  ;            (append (list (format "$~a" start)) (arguments-key (+ start 1) end)))))
  ; (define (arguments) (current-command-line-arguments))
  ; (define (arguments-alist) (make-alist (arguments-key 1 (length (arguments))) (arguments)))
  (define (register-arguments) (register-context 'arguments (current-command-line-arguments)))

  ; resolve path of FUNNY_HOME, system macro file and module translator file
  (define (get-funny-home) (getenv +funny-home+))
  (define (get-funny-scheme) (getenv +funny-scheme+))
  (define (get-system-macro-file-path) (string-append* (get-funny-home) +config-path+ +system-macro-file+))
  (define (get-module-translator-file-path) (string-append* (get-funny-home) +config-path+ (get-funny-scheme) "-translator.scm"))

  ; init system macro
  (define (init) (init-system-macro (open-input-file (get-system-macro-file-path))))

  ; init translator
  (define (init-translator) (eval (read (open-input-string (file->string (get-module-translator-file-path)))) funny-ns))
  (define (run-funny-script-interface) (eval 'run-funny-script funny-ns))
  ;; (define (translate-funny-script-interface) (eval 'translate-funny-script funny-ns)) ;(translate-funny-script funny-statements match-fn in out dest)

  ; find project file, must be single
  (define (get-project-file)
    (let ((files (find-files (lambda (p) (string-suffix? (path->string p) +project-file-suffix+)) (current-directory))))
      (if (= (length files) 1) (first files)
          (error "Zero or multiple project files!"))))
  ; match statement with namespace
  (define (match-statement-with-namespace ns-name input-file)
    (let* ((script (file->string input-file))
           (wraped-script (string-append* "{@@ {" ns-name "} {" script "}}")))
      (match-statement (parse-statements wraped-script))))
  ; run project
  (define (run-project)
    ((run-funny-script-interface) "project" (get-project-file) match-statement-with-namespace))

  (register-arguments)
  (init)
  (init-translator)
  (run-project))

; (run-funny)

