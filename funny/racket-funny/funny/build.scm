#lang racket

(require racket/cmdline)
(require racket/file)

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/HashTable.scm")

; (require (file "E:/gitee/funnyos/study/funny/racket-funny/ds/Trie.scm"))

(define build-ns (make-base-namespace))

(define +bindings-delimiter+   "=")
(define +space-string+         " ")
(define +default+              "default")
(define +requirements+         "
  (require racket/file)
  (require \"../funny/global.scm\")
  (require \"../funny/parser.scm\")
  (require \"../funny/lexer.scm\")
  (require \"../funny/matcher.scm\")
  (require \"../funny/oop.scm\")
  (require \"../funny/engine.scm\")")

(define (build binding-config function-binding-config build-script)
  (define *funny-home*
    (let ((v (getenv +funny-home+)))
      (if (null? *funny-home*)
          (error "Please define environment variable 'FUNNY_HOME' first!")
          v)))
  (define (default-plugin-path plugin-file-name)
    (if (string-suffix? *funny-home* "\\")
        (string-append *funny-home* "plugin\\" plugin-file-name)
        (string-append *funny-home* "\\plugin\\" plugin-file-name)))
  (define *default-function-bindings*
    (let ((t (CREATE-HashMap)))
      (map (lambda (k p) (hashtable-put! t k (default-plugin-path p)))
           (list ":match"
                 ":clean"
                 ":parse-meta"
                 ":build")
           (list "default-matcher.scm"
                 "default-cleaner.scm"
                 "default-meta-parser.scm"
                 "default-builder.scm"))
      t))
  
  (define (parse-bindings bindings)
    (let ((t (string-split bindings +bindings-delimiter+)))
      (string-append "(" (first t) +space-string+ (second t) ")")))
  (define (parse-function-bindings bindings)
    (let* ((t (string-split bindings +bindings-delimiter+))
           (k (first t))
           (v (second t)))
      (cons k (if (equal? v +default+) (hashtable-get *default-function-bindings* k) v))))

  (define binding-script (string-join (map (lambda (t) (parse-bindings t)) binding-config) +space-string+))
  (define function-bindings (map (lambda (t) (parse-function-bindings t)) function-binding-config))
  (define exp-list
    (append
     (read (open-input-string +requirements+))
     (map (lambda (t)
            (read (open-input-string (file->string (cdr t))))
            (string->symbol (car t)))
          function-bindings)
     (read (open-input-string
            (string-append "(let (" binding-script ")" build-script ")")))))
  (define (eval-expressions exp-list)
    (map (lambda (exp) (eval exp build-ns)) exp-list))
  (eval-expressions exp-list))


