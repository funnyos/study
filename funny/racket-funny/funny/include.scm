#lang racket

(require racket/file)
(require "../ds/Component.scm")
(require "../ds/Graph.scm")
(require "../ds/HashTable.scm")
(require "../ds/LcrsTree.scm")
(require "../ds/Mapping.scm")
(require "global.scm")
(require "parser.scm")
(require "lexer.scm")
(require "oop.scm")
(require "engine.scm")

(provide (all-defined-out))

(define %*plugin-collector* *plugin-collector*)

; Component
(define %create-component CREATE-Component)
(define %component-valid? component-valid?)

; Graph
(define %create-dag CREATE-DAG)
(define %graph-dag? graph-dag?)

; HashTable
(define %create-hashtable CREATE-HashTable)
(define %create-hashmap CREATE-HashMap)
(define %hashmap-get hashmap-get)
(define %hashmap-put! hashmap-put!)

; LcrsTree
(define %create-treenode CREATE-TreeNode)
(define %tree-node-add! tree-node-add!)
(define %tree-node-lc! tree-node-lc!)
(define %tree-node-rs! tree-node-rs!)

; LexRule

; Mapping
(define %create-system-mapping CREATE-System-Mapping)
(define %create-mapping CREATE-Mapping)

; Object

; Queue

; Stack

; Statement

; Trie



(define %match-statement match-statement)


