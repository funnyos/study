#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Funny engine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/HashTable.scm")
(require "../ds/Graph.scm")
(require "../ds/Statement.scm")
(require "global.scm")
(require "parser.scm")

(provide (all-defined-out))

(define engin-ns (make-base-namespace))

(define (match-statement s)
  (let ((symbols (statement-symbol-count s))
        (arguments (statement-argument-count s))
        (m (get-context 'mapping-trie)))
    (if (= arguments 0)
        (match-leaf s)
        (match-statement-foreach-mapping s (get-mappings m s)))))

(define (match-leaf s) '())
(define (match-statement-foreach-mapping s mappings) '())

;(define-argument {argument} {arg-index})
(define (get-cmd-argument argument-index)
  (let ((index (- (string->number (substring argument-index 1)) 1))
        (args (get-context 'arguments)))
    (vector-ref args index)))

; (arguments {arguments})

; (construct-path
; (plugin
; (explain {filepath} {filename})
; (run-compute-domain {compute-domain})
; (dependencies-resolver {dependencies})
; (get-compute-domain {domain-declare})

; to-implement

(define (CREATE-ComputeDomain s)
  '())

(define (compute-domain-id compute-domain)
  '())

(define (explain-funny fn)
  '())

(define (scan-compute-domains component-path repositories)
  (define components-map (CREATE-HashMap))
  (define compute-domains-map (CREATE-HashMap))
  
  (define (first-statement file-path)
    (car (parse-statements (call-with-input-file file-path (lambda (in) (read-line in))))))

  (define (get-compute-domain file-path)
    (CREATE-ComputeDomain (first-statement file-path)))

  (define (language-repo)
    (string-append (getenv +funny-home+) +language-lib-path+))

  (define (repo-list)
    (append (string-split repositories) (list (language-repo))))

  (define (cache-all-component-def)
    (map (lambda (t) (hashmap-put! components-map (get-compute-domain (string-append t +component-file+)) t))
     (repo-list)))

  (define (graph-node t) '())
  (define (graph-node-key t) '())

  (define (cache-all-component-compute-domains)
    (define dep (explain-funny (string-append component-path +dependencies-file+)))
    (define components-graph (CREATE-DAG))
    (define compute-domains-graph (CREATE-DAG))
    (define (recur-cache component-graph compute-domain-graph)
      (if (null? component-graph) '()
          (map (lambda (t)
                 (let ((d (explain-funny (string-append (graph-node-key t) +dependencies-file+))))
                   ;;; components-map
                   ;;; compute-domains-map
                   (recur-cache (car d) (cdr d))))
               (graph-node component-graph))))
    
    '())

  compute-domains-map)











