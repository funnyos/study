#lang racket

(require "../common/constants.scm")
(require "../common/messages.scm")
(require "../common/utils.scm")
(require "../ds/Object.scm")
;(require "../ds/Queue.scm")
;(require "../ds/Stack.scm")
(require "../ds/HashTable.scm")
;(require "../ds/Mapping.scm")
;(require "../ds/Graph.scm")
;(require "../ds/Trie.scm")
;(require "../ds/Statement.scm")
;(require "../ds/LcrsTree.scm")
;(require "../ds/Component.scm")

(provide register-context
         change-context
         get-context)

(define *global-context* (CREATE-HashMap))

(define (register-context key value)
  (hashmap-put! *global-context* key value))

(define (change-context key new-value)
  (hashmap-put! *global-context* key new-value))

(define (get-context key)
  (hashmap-get *global-context* key))


#|

; Tree: used for cache compute domains
(define *compute-domains* (CREATE-TreeNode 'compute-domains +nil+ +nil+ +nil+ +nil+ 0))

;(define *funny-home* '())
;(define *system-macros* (CREATE-HashTable))

; Trie: used for cache mappings
(define *mapping-trie* (CREATE-Trie))

; TransitTable: used for PDFA
(define *transit-table* (CREATE-TransitTable))

(define *plugin-collector* (CREATE-HashMap))



;(define *inheritance-tree* (CREATE-TreeNode))
(define *lex-pattern-cache* (CREATE-HashMap))
(define *oop-template-cache* '())

(define *lex-rules* (CREATE-HashMap))
;(define *lex-terminals* (list))
(define *lex-orders* (CREATE-EmptyQueue))

(define *my-component* '())
(define *repositories* '())
(define *component-paths* (CREATE-HashMap))
(define *component-meta* (CREATE-HashMap))
(define *components* (CREATE-DAG))
(define *modules* (CREATE-DAG))


(define (trie-add-m mapping) (ask *mapping-trie* +M-Trie-add+ mapping trie-prefix-of-mapping))

|#

