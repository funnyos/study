#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Funny system macro
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/Stack.scm")
(require "../ds/Trie.scm")
(require "../ds/Statement.scm")
(require "../ds/Mapping.scm")
(require "global.scm")
(require "parser.scm")

(provide (all-defined-out))

(define system-macro-ns (make-base-namespace))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; meta scripts are defined as follows:
; 1. (run-script script statement)
; 2. (cache-mapping mapping-source mapping-target function-name statement)
; 3. (eval-script eval-result statement)
; 4. (translate-statement eval-result function-name mapping-source mapping-target statement)
; 5. (define-macro mapping-source mapping-target statement)
; 6. (prepare-mapping mapping-source function-name statement)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (meta:run-script script replacements)
  (eval-script script replacements))

(define (meta:cache-mapping mapping-source mapping-target function-name replacements)
  (let ((s (eval-script mapping-source replacements))
        (t (eval-script mapping-target replacements))
        (n (eval-script function-name replacements))
        (m (get-context 'mapping-trie)))
    (trie-add m (trie-prefix-of-statement s) (CREATE-Mapping #f s t))))

(define (meta:eval-script eval-result replacements)
  (eval-script eval-result replacements))

(define (meta:translate-statement eval-result function-name mapping-source mapping-target replacements)
  (let ((s (eval-script mapping-source replacements))
        (t (eval-script mapping-target replacements))
        (n (eval-script function-name replacements))
        (m (get-context 'mapping-trie)))
    (trie-add m (trie-prefix-of-statement s) (CREATE-Mapping #f s t))
    (eval-script eval-result replacements)))

(define (meta:define-macro mapping-source mapping-target replacements)
  (let ((s (eval-script mapping-source replacements))
        (t (eval-script mapping-target replacements))
        (m (get-context 'mapping-trie)))
    (trie-add m (trie-prefix-of-statement s) (CREATE-Mapping #t s t))))

;; todo
(define (meta:prepare-mapping mapping-source function-name replacements)
  (let ((s (eval-script mapping-source replacements))
        (t '()) ;(CREATE-Statement)
        (n (eval-script function-name replacements))
        (m (get-context 'mapping-trie)))
    (trie-add m (trie-prefix-of-statement s) (CREATE-Mapping #f s t))))

(define (explain-system-macro mapping replacements)
  (let* ((statement (mapping-target-get mapping))
         (context (statement-meta-context statement replacements)))
    (eval-script (car context) (cdr context))))

; eval-script????

(define replacements-stack (CREATE-Stack))

(define (find-statement argument-name)
  (cdr (assoc argument-name (stack-peek replacements-stack))))

(define (replaced-script script replacements)
  (string-replace-by-alist
   script
   (map (lambda (t) (cons (format "{~a}" (car t)) (cdr t))) replacements)))

(define (eval-script script replacements)
  (begin
    (stack-push replacements-stack replacements)
    (let ((result (eval (read (open-input-string (replaced-script script replacements))) system-macro-ns)))
      (stack-pop replacements-stack)
      result)))

; init-system-macro

(define (init-system-macro input-port)
  (let ((system-macros (string-join (read-lines input-port) (~a #\newline)))
        (m (register-context 'mapping-trie (CREATE-Trie)))
        (t (register-context 'transit-table (CREATE-TransitTable))))
    (eval-statements
     (lambda (s)
       (cache-system-macro m t s))
     system-macros)))

