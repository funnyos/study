#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Funny parser, based on the paper "Monadic Parser Combinators"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/Object.scm")
(require "../ds/Queue.scm")
(require "../ds/Statement.scm")

(provide parse-statements
         eval-statements
         translate-statements)

(define (to-string lst)
  (CREATE-SymbolStatement (list->string (append (list #\") lst (list #\")))))

(define (to-symbol lst)
  (CREATE-SymbolStatement (list->string lst)))

(define (to-statement st)
  (if (list? st) (CREATE-OneStatement st) st))

(define zero
  (lambda (input) '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; primitives
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define result
  (lambda (v)
    (lambda (input) (list (cons v input)))))

(define item
  (lambda (input)
    (if (equal? input "")
        '()
        (let ((fst (string-ref input 0))
              (rst (substring input 1 (string-length input))))
          (list (cons fst rst))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; combinators
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (bind parser f)
  (lambda (input)
    (let ((vs (parser input)))
      (if (null? vs)
          vs
          ((f (caar vs)) (cdar vs))))))

(define (satisfy predicate)
  (bind item
        (lambda (x)
          (if (predicate x)
              (result x)
              zero))))

(define char (lambda (x) (satisfy (lambda(y) (equal? x y)))))

(define (plus p q)
  (lambda (input)
    (concat (p input) (q input))))

(define non-empty-many
  (lambda (p)
    (bind p
          (lambda (x)
            (bind (many p)
                  (lambda (xs)
                    (result (append (list x) xs))))))))

(define many (lambda (p) (plus (non-empty-many p) (result '()))))
(define many-1 (lambda (p) (non-empty-many p)))

(define first
  (lambda (p)
    (lambda (input)
      (let ((x (p input)))
        (if (null? x)
            x
            (list (car x)))))))

(define (plus-1 p q)
  (first (plus p q)))

(define space (satisfy char-whitespace?))

(define spaces (bind (many-1 space) (lambda(_) (result '()))))

(define non-newline (satisfy (lambda(x) (not (equal? x #\newline)))))

(define comment
  (bind (char #\;)
        (lambda (_)
          (bind (many non-newline)
                (lambda (_)
                  (bind (char #\newline)
                        (lambda (_)
                          (result '()))))))))

(define junk (bind (many (plus-1 spaces comment)) (lambda (_) (result '()))))

(define sep-by-1
  (lambda (sep)
    (lambda (p)
      (bind p
            (lambda (x) 
              (bind (many
                     (bind sep
                           (lambda (_)
                             (bind p
                                   (lambda (y)
                                     (result y))))))
                    (lambda (xs)
                      (result (cons x xs)))))))))

(define (single-sep p)
  (bind (many space)
        (lambda(_)
          (bind p
                (lambda(x)
                  (bind (many space)
                        (lambda(_)
                          (result x))))))))

(define brace-open (single-sep (char #\{)))
(define brace-close (single-sep (char #\})))

(define to-escape-chars
  (satisfy (lambda(y) (not (equal? (member y '(#\\ #\t #\n #\r #\")) #f)))))

(define escaped
  (bind (char #\\)
        (lambda (_)
          (bind to-escape-chars
                (lambda (y)
                  (result (escape y)))))))

(define non-quote
  (satisfy (lambda (ch) (not (char=? ch #\")))))

(define quoted
  (bind (char #\")
        (lambda (_)
          (bind (many (plus escaped non-quote))
                (lambda (x)
                  (bind (char #\")
                        (lambda (_)
                          (result (to-string x)))))))))

(define non-sym
  (satisfy (lambda (ch) (not (or (char=? ch #\")
                                 (char=? ch #\,)
                                 (char=? ch #\;)
                                 (char=? ch #\{)
                                 (char=? ch #\})
                                 (char-whitespace? ch))))))

(define sym
  (bind non-sym
        (lambda (s)
          (bind (many non-sym)
                (lambda (x)
                  (result (to-symbol (append (list s) x))))))))

(define statement
  (bind brace-open
        (lambda (_)
          (bind (plus ((sep-by-1 (many space)) value) (result '(())))
                (lambda (v)
                  (bind brace-close
                        (lambda(_)
                          (result (to-statement v)))))))))

(define clean-statement
  (bind junk
        (lambda (_)
          (bind statement
                (lambda (v)
                  (bind junk
                        (lambda (_)
                          (result v))))))))

(define value
  (foldr plus-1 zero (list quoted sym clean-statement)))

(define (parse-statements txt)
  (let ((t (clean-statement txt)))
    (if (null? t)
        t
        (cons (caar t) (parse-statements (cdar t))))))

(define (eval-statements f txt)
  (let ((t (clean-statement txt)))
    (if (null? t)
        t
        (begin (f (caar t))
               (eval-statements f (cdar t))))))

(define (translate-statements f txt)
  (let ((t (clean-statement txt)))
    (if (null? t)
        t
        (string-append (f (caar t))
                       (translate-statements f (cdar t))))))
