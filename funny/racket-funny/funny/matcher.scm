#lang racket

(define-namespace-anchor anc)
(define ns (namespace-anchor->namespace anc))

(require "../common/utils.scm")
(require "../common/messages.scm")
(require "../ds/Object.scm")
(require "../ds/Statement.scm")
(require "../ds/Mapping.scm")


(provide (all-defined-out))



(define (match-lex s)
  '())

(define (match-scheme s)
  '())

(define (expandable? m)
  #f)

(define (system-macro? m)
  #f)

(define (tranlate mapping replacements)
  '())

(define cache-lex (lambda (m) m))

(define global-replacements '())

(define (explain-system-macro mapping replacements)
  (set! global-replacements (list (cons 1 replacements)))
  (eval (read (open-input-string "(cache-lex (assoc \"orders\" (cdr (assoc 1 global-replacements))))")) ns))

(define (expand-statement mapping replacements)
  '())

(define (match-statement-foreach-mapping statement mappings)
  (let* ((matched (ormap
                   (lambda (m) (cons m (ask statement +M-Statement-test-match+ m)))
                   mappings))
         (m (safe car matched))
         (state (safe cadr matched))
         (replacements (safe cddr matched)))
    (if (or (null? state) (not (cdr state)))
        (error (append "Cannot find mapping for statement: " (ask statement +M-Object-to-string+)))
        (cond
          ((mapping-system? m)
           (explain-system-macro m replacements))
          ((mapping-expandable? m)
           (expand-statement m replacements))
          (else (tranlate m replacements))))))

;(define (match-statement s)
;  (let ((symbols (ask s +M-Statement-symbol-count+))
;        (arguments (ask s +M-Statement-argument-count+)))
;    (if (= arguments 0)
;        (if (= symbols 1)
;            (match-lex s)
;            (match-scheme s))
;        (match-statement-foreach-mapping s (get-mappings s)))))


(define assoc-list '(("orders" . "integer > fraction")))

;(tranlate-system-macro "mapping" assoc-list)

