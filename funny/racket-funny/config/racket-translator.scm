(define (run-funny-script namespace input-file eval-f)
  (eval-f namespace input-file))

(define (translate-funny funny-statements match-f in out dest)
  (define (translate)
    (string-append*
     "#lang racket"
     (~a #\newline)
     (map (lambda (t)
            (string-append* "(require \"" t "\")" (~a #\newline)))
          in)
     "(provide "
     out
     ")"
     (~a #\newline)
     (map match-f funny-statements)))
  (write-to-file (translate) dest))