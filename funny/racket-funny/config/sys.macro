;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Funny Language - a free style programming language.
;; Copyright (C) 2014 by fanguangping. Email: fanguangping@163.com
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: lex
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {cache lex {"integer > fraction > decimal > complex"}}
;;
statement: {cache lex {orders}}
name: cachelex
function: NIL
mapping: NIL
run: LEX[{orders}]
eval: NIL

DELIMITER;

;;
;; example: {define lex rule {boolean} -> {"<bool> :: ($)"}}
;;
statement: {define lex rule {token} -> {pattern}}
name: defrule
function: NIL
mapping: NIL
run: RULE[{token},{pattern}]
eval: NIL

DELIMITER;

;;
;; example: {define lex group {integer} -> {"(<sign><digit>+) :: (lex-integer $)"}}
;;
statement: {define lex group {token} -> {pattern}}
name: defgroup
function: NIL
mapping: NIL
run: GROUP[{token},{pattern}]
eval: NIL

DELIMITER;

;;
;; example: {define lex token {bool} -> {"#[tf]"}}
;;
statement: {define lex token {token} -> {pattern}}
name: deftoken
function: NIL
mapping: NIL
run: TOKEN[{token},{pattern}]
eval: NIL

DELIMITER;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: templates and functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {define template {or}: {{x} or {y}} as {(or {x} {y})}}
;;
statement: {define template {name}: {template} as {statement}}
name: deftpl
function: FUNCNAME[{statement}]
mapping: {template} > {statement}
run: NIL
eval: NIL

DELIMITER;

;;
;; example: {define {abc} as {123}}
;;
statement: {define {var} as {val}}
name: defvar
function: NIL
mapping: NIL
run: NIL
eval: (define EXTRACT[{var}] MATCH[{val}])

DELIMITER;

;;
;; example: {define function {qsum}: {the sum of squares {a} and {b}} as {{{a}*{a}}+{{b}*{b}}}}
;;
statement: {define function {name}: {function} as {statement}}
name: defun
function: EXTRACT[{name}]
mapping: {function} > \{(EXTRACT[{name}] TARGET[{function}])\}
run: NIL
eval: (define (EXTRACT[{name}] ARGLIST[{function}]) MATCH[{statement}])

DELIMITER;

;;
;; example: {lambda {a b c}: {{a}+{b}+{c}}}
;;
statement: {lambda {args}: {expression}}
name: lambda
function: NIL
mapping: NIL
run: NIL
eval: (lambda (EXTRACT[{args}]) MATCH[{expression}])

DELIMITER;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {define macro {set {var} to nil} as {set {var} to {*nil*}}}
;;
statement: {define macro {template} as {statement}}
name: defmacro
function: NIL
mapping: {template} > {statement}
run: NIL
eval: NIL

DELIMITER;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: resource loaders
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {load builtin resource {lexer}: {lexer.fn}}
;;
statement: {load builtin resource {module}: {file}}
name: loadbltn
function: NIL
mapping: NIL
run: LOADBLTN[{module},{file}]
eval: NIL

DELIMITER;

;;
;; example: {load template {"templates/core.fn"}}
;;
statement: {load template {template}}
name: loadtpl
function: NIL
mapping: NIL
run: LOADTPL[{template}]
eval: NIL

DELIMITER;

;;
;; example: {load file {"core/basis/Object.fn"}}
;;
statement: {load file {file}}
name: loadfile
function: NIL
mapping: NIL
run: LOADFILE[{file}]
eval: NIL

DELIMITER;

;;
;; example: {load package {"types.tar"}}
;;
statement: {load package {package}}
name: loadpkg
function: NIL
mapping: NIL
run: LOADPKG[{package}]
eval: NIL

DELIMITER;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: oop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {define oop template {new-instance} as {"(CREATE-%className% %propertyValues%)"}}
;;
statement: {define oop template {template} as {definition}}
name: defoop
function: NIL
mapping: NIL
run: OOP[{template},{definition}]
eval: NIL

DELIMITER;

;;
;; example: {define class {Circle}}
;;
statement: {define class {class}}
name: defclass
function: NIL
mapping: NIL
run: DEFCLASS[{class}]
eval: NIL

DELIMITER;

;;
;; example: {declare method {to-string}:{convert {self} to string}}
;;
statement: {declare method {method-name}:{declaration}}
name: decmethod
function: METHODNAME[{method-name}]
mapping: {declaration} > \{\}
run: NIL
eval: NIL

DELIMITER;

;;
;; example: {define method {store-area} as {set {area} as {calculate area}}}
;;
statement: {define method {method-name} as {definition}}
name: defmethod
function: NIL
mapping: NIL
run: DEFMETHOD[{method-name},{definition}]
eval: NIL

DELIMITER;

;;
;; example: {new instance of {Rectangle}, witch {{side}={5}}}
;;
statement: {new instance of {class}, witch {properties-assign}}
name: newinstance
function: NIL
mapping: NIL
run: NIL
eval: NEWINSTANCE[{class},{properties-assign}]

DELIMITER;

;;
;; example: {send message {how-to-eat}:{eat} to {animal}}
;;
statement: {send message {message}:{message-detail} to {object}}
name: sendmsg
function: NIL
mapping: NIL
run: NIL
eval: SENDMSG[{message},{message-detail},{object}]

DELIMITER;

;;
;; example: {get {area} of {shape}}
;;
statement: {get {property} of {object}}
name: getprop
function: NIL
mapping: NIL
run: NIL
eval: GETPROP[{property},{object}]

DELIMITER;

;;
;; example: {set {area} of {shape} as {10}}
;;
statement: {set {property} of {object} as {value}}
name: setprop
function: NIL
mapping: NIL
run: NIL
eval: SETPROP[{property},{object},{value}]

DELIMITER;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: pattern matcher
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {define struct {BinaryTreeNode}: {data left right} with pattern matcher {left<-[data]->right}}
;;
statement: {define struct {struct-name}: {struct-properties} with pattern matcher {pattern}}
name: defstructp
function: NIL
mapping: NIL
run: DEFSTRUCT[{struct-name},{struct-properties},{pattern}]
eval: (define-struct EXTRACT[{struct-name}] EXTRACT[{struct-properties}])

DELIMITER;

;;
;; example: {define pattern matcher {list-matcher1}:{@ [x|xs]} for {list:List} as {{x} => {first of {list}}, {xs} => {rest of {list}}}
;;
statement: {define pattern matcher {matcher-name}:{pattern} for {object} as {matcher}}
name: defpattern
function: NIL
mapping: NIL
run: DEFPATTERN[{matcher-name},{pattern},{object},{matcher}]
eval: NIL

DELIMITER;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: components
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {define component {funny-examples-0.3.0}}
;;
statement: {define component {component-name}}
name: defcomp
function: NIL
mapping: NIL
run: DEFCOMP[{component-name}]
eval: (define-component EXTRACT[{component-name}])

DELIMITER;

;;
;; example: {requires component {...}}
;;
statement: {requires component {component-name}}
name: require
function: NIL
mapping: NIL
run: REQUIRE[{component-name}]
eval: (require-component EXTRACT[{component-name}])

DELIMITER;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System macro defined for: comments
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; example: {comment {"this is a comment ......"}}
;;
statement: {comment {comments}}
name: comment
function: NIL
mapping: NIL
run: NIL
eval: NIL




