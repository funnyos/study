#lang racket

(require "../funny/include.scm")

(provide (all-defined-out))

(define (:clean loc)
  (delete-directory/files loc)
  (display loc))
