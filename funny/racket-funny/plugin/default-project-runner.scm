#lang racket

(require "plugin-common.scm")

(define +use-plugin-as+ ":=")

(define (get-plugin-name path) '())

(define (get-arguments arguments) '())

(define (build cache cache-get-f cache-put-f action)
  (define build-tools (cache-get-f cache +build-tools+))
  (define build-tool (string->symbol (string-append (cache-get-f build-tools action) ":execute")))
  (cond
    ((equal? action +clean+) '())) ;meta-parser use plugin?
  (eval (file->string (open-input-file plugin-path))) ;remove #lang
  (eval (append (list plugin-function) (get-arguments arguments))))

(define (default-project-runner:execute project-file-path cache cache-get-f cache-put-f)
  (define blocks (extract-config project-file-path))
  (define project-configs (cache-get-f cache +project-configs+))
  (for-each (lambda (block)
              (cache-put-f project-configs (car block) (cdr block)))
            blocks)
  (for-each (lambda (action)
              (build cache cache-get-f cache-put-f action))
            (cache-get-f project-configs +build-actions+)))


(define (get-file-name path) '())

(define (run-plugin plugin-path content-path)
  (eval (file->string (open-input-file plugin-path)))
  (eval (string-append* "(" (get-file-name plugin-path) ":execute \"" content-path "\")")))


#|
https://stackoverflow.com/questions/29460841/dynamic-function-call-in-racket-or-get-a-procedure-from-a-string

#lang racket
(define (add x y)
  (+ x y))

(define ns (variable-reference->namespace (#%variable-reference)))

(define (string->procedure s)
  (define sym (string->symbol s))
  (eval sym ns))

(string->procedure "add")

((string->procedure "add") 1 2)

https://stackoverflow.com/questions/50897767/racket-eval-namespace-attach-module-vs-namespace-require

|#