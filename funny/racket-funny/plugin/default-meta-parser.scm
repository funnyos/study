#lang racket

(require "../funny/include.scm")

(provide (all-defined-out))

(define (:parse-meta metadata)
  (define +class-prefix+            "::")
  (define +properties-prefix+       "properties:")
  (define +methods-prefix+          "methods:")
  (define +indent+                  "--")
  (define +method-arity+            "/")

  (define (non-blank-string? t) (non-empty-string? (string-trim t)))
  (define (safe f t)
    (if (null? t) t
        (f t)))

  ;(define metadata (read-config meta-file (lambda (s) (and (string-prefix? s "[") (string-suffix? s "]")))))

  (define (grouping-blocks lst predicate)
    (let ((r (list))
          (current-block (list)))
      (for-each (lambda (t)
                  (if (predicate t)
                      (begin (set! r (append r (list current-block)))
                             (set! current-block (list t)))
                      (set! current-block (append current-block (list (string-trim t))))))
                lst)
      (set! r (append r (list current-block)))
      (safe cdr r)))

  (define (grouping-classes lst)
    (map (lambda (block)
           (cons (list (car block))
                 (grouping-blocks (cdr block) (lambda (t) (string-prefix? t +indent+)))))
         (grouping-blocks lst (lambda (t) (string-prefix? t +class-prefix+)))))

  (define (count-level class-def)
    (/ (string-length (car (string-split class-def +class-prefix+))) (string-length +indent+)))

  (define (get-name class-def)
    (if (string-prefix? class-def +class-prefix+)
        (car (string-split class-def +class-prefix+))
        (cadr (string-split class-def +class-prefix+))))

  (define (get-class-name class-node) (get-name (car class-node)))

  (define (get-class-value class-node)
    (cdr class-node))

  (define (to-tree lst)
    (let ((root (%create-treenode (safe caar lst) '() '() '() '())))
      (%tree-node-lc! root (recur-build-tree (%create-treenode (safe caar (cdr lst)) '() '() '() '()) 1 (cdr lst)))
      root))

  (define (recur-build-tree node level branches)
    (define (get-index lst pred index)
      (cond ((null? lst) index)
            ((pred (car lst)) index)
            (else (get-index (cdr lst) pred (+ index 1)))))
    (define restlst (safe cdr branches))
    (define split-condition
      (lambda (t) (= level (count-level (car t)))))
    (if (or (null? node) (null? branches)) '()
        (let-values (((lclst rslst) (split-at restlst (get-index restlst split-condition 0))))
          (if (null? lclst) '()
              (%tree-node-lc! node (recur-build-tree (%create-treenode (safe caar lclst) '() (car lclst) '() '()) (+ level 1) lclst)))
          (if (null? rslst) '()
              (%tree-node-rs! node (recur-build-tree (%create-treenode (safe caar rslst) '() (car rslst) '() '()) level rslst)))
          node)))

  (define (parse-classes text)
    (let* ((text-lines (filter non-blank-string? (string-split text "\n")))
           (grp (grouping-classes text-lines)))
      (map to-tree grp)))
  
  (display metadata))

















#|

#lang racket

(require "../common/constants.scm")
(require "../common/utils.scm")
(require "../ds/LcrsTree.scm")

(provide parse-classes)

(define +class-prefix+            "::")
(define +properties-prefix+       "properties:")
(define +methods-prefix+          "methods:")
(define +indent+                  "--")
(define +method-arity+            "/")

(define (grouping-blocks lst predicate)
  (let ((r (list))
        (current-block (list)))
    (for-each (lambda (t)
                (if (predicate t)
                    (begin (set! r (append r (list current-block)))
                           (set! current-block (list t)))
                    (set! current-block (append current-block (list (string-trim t))))))
              lst)
    (set! r (append r (list current-block)))
    (safe cdr r)))

(define (grouping-classes lst)
  (map (lambda (block)
         (cons (list (car block))
               (grouping-blocks (cdr block) (lambda (t) (string-prefix? t +indent+)))))
       (grouping-blocks lst (lambda (t) (string-prefix? t +class-prefix+)))))

(define (count-level class-def)
  (/ (string-length (car (string-split class-def +class-prefix+))) (string-length +indent+)))

(define (get-name class-def)
  (if (string-prefix? class-def +class-prefix+)
      (car (string-split class-def +class-prefix+))
      (cadr (string-split class-def +class-prefix+))))

(define (get-class-name class-node) (get-name (car class-node)))

(define (get-class-value class-node)
  (cdr class-node))

(define (to-tree lst)
  (let ((root (CREATE-TreeNode (safe caar lst) +nil+ +nil+ +nil+ +nil+)))
    (tree-node-lc! root (recur-build-tree (CREATE-TreeNode (safe caar (cdr lst)) +nil+ +nil+ +nil+ +nil+) 1 (cdr lst)))
    root))

(define (recur-build-tree node level branches)
  (define (get-index lst pred index)
    (cond ((null? lst) index)
          ((pred (car lst)) index)
          (else (get-index (cdr lst) pred (+ index 1)))))
  (define restlst (safe cdr branches))
  (define split-condition
    (lambda (t) (= level (count-level (car t)))))
  (if (or (null? node) (null? branches)) +nil+
      (let-values (((lclst rslst) (split-at restlst (get-index restlst split-condition 0))))
        (if (null? lclst) '()
            (tree-node-lc! node (recur-build-tree (CREATE-TreeNode (safe caar lclst) +nil+ (car lclst) +nil+ +nil+) (+ level 1) lclst)))
        (if (null? rslst) '()
            (tree-node-rs! node (recur-build-tree (CREATE-TreeNode (safe caar rslst) +nil+ (car rslst) +nil+ +nil+) level rslst)))
        node)))

(define (parse-classes text)
  (let* ((text-lines (filter non-blank-string? (string-split text "\n")))
         (grp (grouping-classes text-lines)))
    (map to-tree grp)))


(define (default-meta-parser:execute meta-file)
  '())

|#

