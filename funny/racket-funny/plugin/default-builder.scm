#lang racket

(require "../funny/include.scm")

(provide (all-defined-out))

(define (:build components build-location match-function)
  (display components)
  (display build-location)
  (display match-function))

