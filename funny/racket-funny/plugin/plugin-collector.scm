#lang racket

(require "../funny/include.scm")

(provide (all-defined-out))

(define plugin-ns (make-base-namespace))

(define (collect-plugins)
  (define (find-first-newline string index)
    (if (char=? (string-ref string index) #\newline) (+ index 1)
        (find-first-newline string (+ index 1))))
  (define (get-clean-code code)
    (let ((trim-code (string-trim code)))
      (if (string-prefix? trim-code "#lang")
          (substring trim-code (find-first-newline trim-code 0))
          trim-code)))

  (define (remove-redundant-code code function)
    (let ((trim-code (string-trim code)))
      (if (and (string-prefix? trim-code "(") (string-prefix? (string-trim (substring trim-code 1)) function))
          (substring trim-code (find-first-newline trim-code 0))
          trim-code)))
  (define (recur-remove-redundant-code code function)
    (let ((new-code (remove-redundant-code code function)))
      (if (equal? code new-code) code
          (recur-remove-redundant-code new-code function))))

  ; there are some bugs left to fix
  ; (recur-process-code "\n  (require \"../funny/include.scm\") \n  (require \"../funny/include1.scm\") \n(provide (all-defined-out))" '("require" "provide"))
  ; (recur-process-code "\n  (require \"../funny/include.scm\") \n  (require \"../funny/include1.scm\") \n(provide (all-defined-out))" '("require" "provide"))
  (define (recur-process-code code function-list)
    (if (null? function-list) code
        (let* ((f (car function-list))
               (new-code (recur-remove-redundant-code code f)))
          (if (equal? code new-code) code
              (recur-process-code new-code (cdr function-list))))))
  (define (process-code code)
    (recur-process-code (get-clean-code code) '("require" "provide")))
  (define (collect-plugin plugin-name plugin-file)
    (eval (read (open-input-string (process-code (file->string plugin-file)))) plugin-ns)
    (%hashmap-put! %*plugin-collector* plugin-name (eval (string->symbol plugin-name) plugin-ns)))
  (collect-plugin ":match" "../plugin/default-matcher.scm")
  (collect-plugin ":clean" "../plugin/default-cleaner.scm")
  (collect-plugin ":parse-meta" "../plugin/default-meta-parser.scm")
  (collect-plugin ":build" "../plugin/default-builder.scm"))

