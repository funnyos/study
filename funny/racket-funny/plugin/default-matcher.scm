#lang racket

(require "../funny/include.scm")

(provide (all-defined-out))

(define (:match statement)
  (%match-statement statement))
